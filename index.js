import { registerRootComponent } from 'expo';
import * as firebase from "firebase";
import App from './App';
const firebaseConfig = {
    apiKey: "AIzaSyBCiYyOzHo5M6m6Nv4g0OS92XxmjvPv8VM",
    authDomain: "sanatorio-digital.firebaseapp.com",
    databaseURL: "https://sanatorio-digital.firebaseio.com",
    projectId: "sanatorio-digital",
    storageBucket: "sanatorio-digital.appspot.com",
    messagingSenderId: "921371634398",
    appId: "1:921371634398:web:0c87b491fb4e57a5ea704f",
    measurementId: "G-2FYPM779SY"
  };
  
  firebase.initializeApp(firebaseConfig);
// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in the Expo client or in a native build,
// the environment is set up appropriately
registerRootComponent(App);
