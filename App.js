import React, { useState, useEffect, useRef } from "react";
import { Drawcontent } from "./src/Screen/Paciente/DrowerContentPaciente/Drower";
import { DrawcontentMedico } from "./src/Screen/Medico/DrowerContentMedico/Drower";
import { DrawcontentTutor } from "./src/Screen/Tutor/DrowerTutor/Drower";
import { DrawcontentServicio } from "./src/Screen/Servicio/DrowerServicio/Drower";
import { Alert } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { SafeAreaProvider, SafeAreaView } from "react-native-safe-area-context";
import * as firebase from "firebase";
import {
  AsyncStorage,
  ActivityIndicator,
  View,
  Text,
  Button,
  Platform,
} from "react-native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { AuthContext } from "./Component/context";
import {
  PacienteStackScreen,
  MedicoStackScreen,
  TutorStackScreen,
  MainTab,
  SinLogin,
  ServicioStackScreen,
} from "./src/Stacks/Stacks";
import { useStateIfMounted } from "use-state-if-mounted";
import { stylesGral } from "./Asset/estilos/stylesGral";
import Constants from "expo-constants";
import * as Notifications from "expo-notifications";
import * as Permissions from "expo-permissions";
import * as SecureStore from "expo-secure-store";
import { isNull } from "lodash";

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

const Drawer = createDrawerNavigator();

function LoginAlert(msg) {
  Alert.alert(
    "Ha ocurrido un error!",
    msg,
    [{ text: "OK", onPress: () => console.log("Alerta cerrada") }],
    { cancelable: true }
  );
}

export default function App({ navigation }) {
  //setters y getters para notificaciones
  const [expoPushToken, setExpoPushToken] = useState("");
  const [VarExpoPushToken, setVarExpoPushToken] = useState("");
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();
  const [show, setshow] = useStateIfMounted(false);
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case "RESTORE_TOKEN":
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
            UserRoles: action.Roles,
          };
        case "SIGN_IN":
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
            UserRoles: action.Roles,
          };
        case "SIGN_UP":
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
            UserRoles: action.Roles,
          };
        case "SIGN_OUT":
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
            UserRoles: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
      UserRoles: null,
    }
  );
  //cada vez que va recargando la pagina vuelve el token a generarse  en memoria segura
  React.useEffect(() => {
    setshow(true);
    const getTokenStore = async () => {
      await SecureStore.getItemAsync("uidTokenSecure").then((value) => {
        console.log("Token almacenado en memoria segura: " + value);
        //  bootstrapAsync();
        if (value != null) {
          //El fetch en produccion tiene que cambiar al de produccion tenes
          console.log(value);
          var Consulta =
            "https://app.excelenciadigital.ml/SanatorioDigitalApiTest/usuarios/uid/" +
            value;
          //06-01-2020 usuario alexissa41@gmail.com no se encuentra en entorno de TEST.
          // +
          // "/idInstitucion/" +
          // 38;
          // console.log(Consulta)
          fetch(Consulta).then((response) =>
            response.status === 200
              ? response.json().then((data) => {
                  console.log("Codigo de respuesta", response.status);
                  console.log("Contenido de respuesta", data);
                  if (data == null) {
                    //Borramos el token erroneo.
                    SecureStore.deleteItemAsync("uidTokenSecure");
                    console.log("Token erroneo eliminado");
                    setshow(false);
                  } else {
                    // console.log(data);
                    var nom = data.Nombre.split(" ");
                    AsyncStorage.setItem("UserName", nom[0]);
                    AsyncStorage.setItem("UserSexo", data.Sexo);
                    AsyncStorage.setItem("uid", value);
                    AsyncStorage.setItem(
                      "IdObraSocial",
                      JSON.stringify(data.IdObraSocial)
                    );
                    AsyncStorage.setItem(
                      "IdPacIns",
                      data.IdPacientes_Instituciones
                    );
                    // console.log(data.datosRoles.length)
                    if (data.datosRoles.length != 0) {
                      if (data.datosRoles[0].tipoRol == "Prestador") {
                        // console.log("Entro Medico-------------------")
                        AsyncStorage.setItem(
                          "IdPrestador",
                          JSON.stringify(data.datosPrestador.idPrestador)
                        );
                        setshow(false);
                        dispatch({
                          type: "SIGN_IN",
                          token: value,
                          Roles: "PRESTADOR",
                        });
                      } else if (data.datosRoles[0].tipoRol == "Tutor") {
                        console.log("Entro TUTOR-------------------");
                        setshow(false);
                        dispatch({
                          type: "SIGN_IN",
                          token: value,
                          Roles: "TUTOR",
                        });
                      } else if (
                        data.datosRoles[0].tipoRol == "Administrador"
                      ) {
                        console.log("Entro AdminServicios-------------------");
                        setshow(false);
                        dispatch({
                          type: "SIGN_IN",
                          token: data.uid,
                          Roles: "ADMINSERV",
                        });
                      }
                    } else {
                      // console.log("Entro PACIENTE-------------------")
                      setshow(false);
                      dispatch({
                        type: "SIGN_IN",
                        token: value,
                        Roles: "PACIENTE",
                      });
                    }
                  }
                })
              : response
                  .text()
                  .then((response) =>
                    console.log(
                      "Tenemos problemas con firebase. Motivo => ",
                      response
                    )
                  )
                  .then(() => {
                    setshow(false);
                  })
          );
        } else {
          setshow(false);
        }
      });
    };
    getTokenStore();

    const bootstrapAsync = async () => {
      let userToken;
      let UserRoles;
      try {
        UserRoles = await AsyncStorage.getItem("UserRoles");
        userToken = await AsyncStorage.getItem("userToken");
      } catch (e) {}
      dispatch({
        type: "RESTORE_TOKEN",
        token: userToken,
        Roles: UserRoles,
      });
    };
    // notificaciones
    registerForPushNotificationsAsync().then((token) =>
      setExpoPushToken(token)
    );

    // This listener is fired whenever a notification is received while the app is foregrounded
    notificationListener.current = Notifications.addNotificationReceivedListener(
      (notification) => {
        setNotification(notification);
      }
    );

    // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
    responseListener.current = Notifications.addNotificationResponseReceivedListener(
      (response) => {
        console.log("Notificación recibida");
        console.log(response);
      }
    );
    return () => {
      Notifications.removeNotificationSubscription(notificationListener);
      Notifications.removeNotificationSubscription(responseListener);
    };
  }, []);

  async function sendExpoToken(message) {
    console.log("enviando token");
    console.log(message);
    await fetch(
      "https://app.excelenciadigital.ml/SanatorioDigitalApiTest/expo/registrar-token",
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Accept-encoding": "gzip, deflate",
          "Content-Type": "application/json",
        },
        body: message,
      }
    )
      .then((respuesta) => respuesta.json())
      .then((datos) => {
        console.log(datos);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  async function obtenerUID() {
    return await AsyncStorage.setItem("uid");
  }
  const authContext = React.useMemo(
    () => ({
      signIn: async (data) => {
        setshow(true);
        var user = data.Usuario.replace(/\s/g, "");
        var pass = data.Password.replace(/\s/g, "");
        firebase
          .auth()
          .signInWithEmailAndPassword(user, pass)
          .then((user) => {
            SecureStore.deleteItemAsync("uidTokenSecure");
            SecureStore.setItemAsync("uidTokenSecure", user.user.uid);
            fetch(
              "https://app.excelenciadigital.ml/SanatorioDigitalApiTest/usuarios/uid/" +
                user.user.uid +
                "/idInstitucion/" +
                38
            )
              .then((response) => response.json())
              .then((data) => {
                console.log(data);
                var nom = data.Nombre.split(" ");
                AsyncStorage.setItem("UserName", nom[0]);
                AsyncStorage.setItem("UserSexo", data.Sexo);
                AsyncStorage.setItem("uid", user.user.uid);
                AsyncStorage.setItem(
                  "IdObraSocial",
                  JSON.stringify(data.IdObraSocial)
                );
                AsyncStorage.setItem(
                  "IdPacIns",
                  data.IdPacientes_Instituciones
                );

                const message = {
                  uid: data.uid,
                  token: VarExpoPushToken,
                };
                sendExpoToken(JSON.stringify(message));
                if (data.datosRoles.length != 0) {
                  if (data.datosRoles[0].tipoRol == "Prestador") {
                    // console.log("Entro Medico-------------------")
                    AsyncStorage.setItem(
                      "IdPrestador",
                      JSON.stringify(data.datosPrestador.idPrestador)
                    );
                    setshow(false);
                    dispatch({
                      type: "SIGN_IN",
                      token: user.user.uid,
                      Roles: "PRESTADOR",
                    });
                  } else if (data.datosRoles[0].tipoRol == "Tutor") {
                    console.log("Entro TUTOR-------------------");
                    // AsyncStorage.setItem('IdTutor', JSON.stringify(data.datosTutor.idUsuariouid));
                    setshow(false);
                    dispatch({
                      type: "SIGN_IN",
                      token: user.user.uid,
                      Roles: "TUTOR",
                    });
                  } else if (data.datosRoles[0].tipoRol == "Administrador") {
                    console.log("Entro AdminServicios-------------------");
                    // AsyncStorage.setItem('IdTutor', JSON.stringify(data.datosTutor.idUsuariouid));
                    setshow(false);
                    dispatch({
                      type: "SIGN_IN",
                      token: data.uid,
                      Roles: "ADMINSERV",
                    });
                  }
                } else {
                  // console.log("Entro PACIENTE-------------------")
                  setshow(false);
                  dispatch({
                    type: "SIGN_IN",
                    token: user.user.uid,
                    Roles: "PACIENTE",
                  });
                }
              });
          })
          .catch((error) => {
            setshow(false);
            if (error.message == "The email address is badly formatted.") {
              LoginAlert("El correo electrónico esta mal escrito.");
            } else if (
              error.message ==
              "The password is invalid or the user does not have a password."
            ) {
              LoginAlert("Usuario o Contraseña incorrectos");
            } else if (
              error.message ==
              "There is no user record corresponding to this identifier. The user may have been deleted."
            ) {
              LoginAlert("No encontramos el usuario.");
            }
          });
      },
      signOut: () =>
        dispatch(
          { type: "SIGN_OUT" },
          SecureStore.deleteItemAsync("uidTokenSecure")
        ),
      signUp: async (data) => {
        // console.log(data)
        var nom = data.res.Nombre.split(" ");
        AsyncStorage.setItem("UserName", nom[0]);
        AsyncStorage.setItem("UserSexo", data.res.Sexo);
        AsyncStorage.setItem("uid", data.res.uid);
        AsyncStorage.setItem(
          "IdObraSocial",
          JSON.stringify(data.res.IdObraSocial)
        );
        AsyncStorage.setItem("IdPacIns", data.res.IdPacientes_Instituciones);
        SecureStore.deleteItemAsync("uidTokenSecure");
        SecureStore.setItemAsync("uidTokenSecure", data.res.uid);
        if (data.datosRoles) {
          if (data.datosRoles[0].tipoRol == "Prestador") {
            AsyncStorage.setItem(
              "IdPrestador",
              JSON.stringify(data.datosPrestador.idPrestador)
            );
            setshow(false);
            dispatch({
              type: "SIGN_IN",
              token: data.res.uid,
              Roles: "PRESTADOR",
            });
          } else if (data.datosRoles[0].tipoRol == "Tutor") {
            console.log("Entro TUTOR-------------------");
            // AsyncStorage.setItem('IdTutor', JSON.stringify(data.datosTutor.idUsuariouid));
            setshow(false);
            dispatch({ type: "SIGN_IN", token: data.res.uid, Roles: "TUTOR" });
          } else if (data.datosRoles[0].tipoRol == "Administrador") {
            console.log("Entro AdminServicios-------------------");
            // AsyncStorage.setItem('IdTutor', JSON.stringify(data.datosTutor.idUsuariouid));
            setshow(false);
            dispatch({ type: "SIGN_IN", token: data.uid, Roles: "ADMINSERV" });
          }
        } else {
          // console.log("Entro PACIENTE-------------------")
          setshow(false);
          dispatch({
            type: "SIGN_IN",
            token: data.res.uid,
            Roles: "PACIENTE",
          });
        }
      },
    }),
    []
  );

  return show ? (
    <View style={[stylesGral.containerLoading, stylesGral.horizontalLoading]}>
      <ActivityIndicator size="large" />
    </View>
  ) : (
    <AuthContext.Provider value={authContext}>
      <SafeAreaProvider>
        <NavigationContainer>
          {state.userToken == null ? (
            <SinLogin />
          ) : state.UserRoles == "PRESTADOR" ? (
            <Drawer.Navigator
              initialRouteName="Medico"
              drawerContent={(props) => <DrawcontentMedico {...props} />}
            >
              <Drawer.Screen name="Medico" component={MedicoStackScreen} />
            </Drawer.Navigator>
          ) : state.UserRoles == "PACIENTE" ? (
            <Drawer.Navigator
              initialRouteName="Paciente"
              drawerContent={(props) => <Drawcontent {...props} />}
            >
              <Drawer.Screen name="Paciente" component={PacienteStackScreen} />
            </Drawer.Navigator>
          ) : state.UserRoles == "TUTOR" ? (
            <Drawer.Navigator
              initialRouteName="TutorStack"
              drawerContent={(props) => <DrawcontentTutor {...props} />}
            >
              <Drawer.Screen name="TutorStack" component={TutorStackScreen} />
              <Drawer.Screen name="MainTab" component={MainTab} />
            </Drawer.Navigator>
          ) : state.UserRoles == "ADMINSERV" ? (
            <Drawer.Navigator
              initialRouteName="ServicesStack"
              drawerContent={(props) => <DrawcontentServicio {...props} />}
            >
              <Drawer.Screen
                name="ServicesStack"
                component={ServicioStackScreen}
              />
            </Drawer.Navigator>
          ) : null}
        </NavigationContainer>
      </SafeAreaProvider>
    </AuthContext.Provider>
  );

  // Can use this function below, OR use Expo's Push Notification Tool-> https://expo.io/dashboard/notifications
  async function sendPushNotification(expoPushToken) {
    const message = {
      to: expoPushToken,
      sound: "default",
      title: "Original Title",
      body: "And here is the body!",
      data: { data: "goes here" },
    };
    await fetch("https://exp.host/--/api/v2/push/send", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Accept-encoding": "gzip, deflate",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(message),
    });
  }
  //  Aca toma el token para notificaciones
  async function registerForPushNotificationsAsync() {
    let token;
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== "granted") {
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS
        );
        finalStatus = status;
      }
      if (finalStatus !== "granted") {
        alert(
          "Ha ocurrido un error obteniendo el token push para las notificaciones!"
        );
        return;
      }
      token = (await Notifications.getExpoPushTokenAsync()).data;

      console.log("el token es: " + token);
      setVarExpoPushToken(token);
      //SecureStore.setItemAsync('expoPushToken', token);
    } else {
      alert(
        "Se debe usar un dispositivo físico para recibir Notificaciones Push"
      );
    }

    if (Platform.OS === "android") {
      Notifications.setNotificationChannelAsync("default", {
        name: "default",
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: "#FF231F7C",
      });
    }

    return token;
  }
}
