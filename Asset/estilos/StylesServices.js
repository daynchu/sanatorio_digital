import {StyleSheet} from 'react-native'

export const StyleServices = StyleSheet.create({

    backgroundHeaderImage:{
        position: 'absolute',
        marginTop: '10%',
        height: '100%',
        width: '100%',
        resizeMode: 'stretch',
        zIndex: -10
      },

})