import {
  StyleSheet
} from "react-native";
import Constants from "expo-constants";
import {
  RFPercentage,
  RFValue
} from "react-native-responsive-fontsize";

export const stylesMedico = StyleSheet.create({
  backgroundHeaderImage: {
    position: 'absolute',
    marginTop: '10%',
    height: '100%',
    width: '100%',
    flex: 1,
    resizeMode: 'stretch',
    zIndex: -10
  },
  container: {
    // marginTop: '100%',
    height: '100%'
  },
  containerSplash: {
    marginTop: '100%',
  },
  header: {
    top: '2%'
  },
  body: {
    marginTop: '-10%',
    alignSelf: 'center',
    paddingLeft: '1%',
    paddingRight: '1%',
    width: '90%',
    fontSize: RFValue(24, 420),
  },
  card: {
    alignSelf: 'center',
    borderRadius: 12,
    backgroundColor: '#fff',
    marginTop: '10%',
    width: '60%',
    height: 80,
    // 
    // height:150,
    // width:"80%",
    backgroundColor: "white",
    borderRadius: 15,
    padding: 10,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    marginBottom: '5%'
  },
  cardImage: {
    // alignSelf: 'center',
  },
  cardImageCircle: {
    alignSelf: 'center',
    marginTop: '-20%',
    resizeMode: 'contain',
  },
  cardImageCircleA: {
    alignSelf: 'center',
    marginTop: 10,
    resizeMode: 'stretch',
  },
  cardText: {
    textAlign: 'center',
    // marginTop: '-50%',
  },
  cardTitle: {
    textAlign: 'center',
    fontSize: 22,
    color: 'black',
    textAlignVertical: 'center',
    marginTop: '-2%'
  },
  backgroundImageSplash: {
    position: 'absolute',
    top: '6%',
    maxHeight: '65%',
    maxWidth: '65%',
    flex: 1,
    resizeMode: 'stretch',
    alignSelf: 'center'
  },
  Title: {
    fontSize: 40,
    textAlign: 'center',
    color: '#000',
    fontWeight: 'bold'
  },
  subTitle: {
    fontSize: 30,
    textAlign: 'center',
    color: '#000',
    marginTop: '10%',
    paddingLeft: '6%',
    paddingRight: '6%',
  },
  TitleIni: {
    fontSize: RFPercentage(5),
    textAlign: 'center',
    color: '#92B913',
    fontWeight: 'bold',
    marginTop: '14%',
    marginLeft: '24%',
  },
  subTitleIni: {
    fontSize: RFPercentage(3),
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#92B913',
    marginLeft: '24%',
  },
  TitleIniAgen: {
    fontSize: RFPercentage(4),
    textAlign: 'center',
    color: '#92B913',
    fontWeight: 'bold',
    marginTop: '18%',
    marginLeft: '24%',
  },
  titleIni: {
    fontSize: RFPercentage(3.7),
    textAlign: 'center',
    color: 'black',
    marginTop: '5%',
    paddingLeft: '8%',
    paddingRight: '8%',
  },
  pIni: {
    fontSize: RFPercentage(3),
    // textAlign: 'center',
    textAlign: 'left',
    color: 'black',
    marginTop: '5%',
    paddingLeft: '8%',
    paddingRight: '8%',
  },
  draBot: {
    position: 'absolute',
    marginTop: '10%',
    top: 10,
    left: 5,
    height: 50,
    width: 75,
    flex: 1,
    resizeMode: 'stretch',
  },

  ContainerButton: {
    flexDirection: 'row',
    width: '50%'
  },

  cardTitleAgenda: {
    textAlign: 'center',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#92B913',
    textAlignVertical: 'center',
    marginTop: '7%'
  },

  cardAgenda: {
    flexDirection: 'row',
    alignSelf: 'center',
    borderRadius: 12,
    backgroundColor: '#fff',
    marginTop: '10%',
    marginLeft: '5%',
    width: '90%',
    height: 70,
    backgroundColor: "white",
    borderRadius: 15,
    padding: 10,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    marginBottom: '5%'
  },

  cardTitleAgenda: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#92B913',
    textAlignVertical: 'center',
    width: '5%',
    height: '5%',
  },

  cardImageAgenda: {
    width: 40,
    height: 40,
  },
  cardImageAgendaA: {
    width: 40,
    height: 40,
  },
  cardTitleAgenda: {
    margin: 5
  },

  cardNumeroAgenda: {
    marginTop: 5,
    fontWeight: 'bold',
    fontSize: 25
  },
  cardTitleListado: {
    fontSize: 20
  },
  TitleListad: {

    margin: 5,
    width: '80%'
  },
  icono: {
    marginLeft: '3%',
    width: '20%'
  },
  encabezado: {
    flexDirection: 'row',
    margin: 4
  },
  ContainerListado: {
    flexDirection: 'column',
    alignSelf: 'center',
    borderRadius: 12,
    backgroundColor: '#fff',
    marginTop: '2%',
    width: '90%',
    backgroundColor: "white",
    borderRadius: 15,
    padding: 10,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    marginBottom: '5%'
  },
  containerLoad: {
    flex: 1,
    justifyContent: "center"
  },
  horizontalLoad: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  cardContainerCCMedico: {
    alignSelf: "center",
    borderRadius: 12,
    backgroundColor: "#92B913",
    marginTop: "8%",
    width: "99%",
    height: "auto",
    borderRadius: 15,
    padding: 10,
    elevation: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
  },
  cardContainerCCMedicoFilter: {
    alignSelf: "center",
    borderRadius: 12,
    backgroundColor: "#fff",
    marginTop: "8%",
    width: "99%",
    height: "auto",
    borderRadius: 15,
    padding: 10,
    elevation: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
  },
  cardNoMarginTop: {
    alignSelf: "center",
    borderRadius: 12,
    backgroundColor: "#fff",
    marginTop: "1%",
    width: "99%",
    height: "auto",
    backgroundColor: "white",
    borderRadius: 15,
    padding: 10,
    elevation: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
  },
  menuButton: {
    backgroundColor: '#F5FAFD',
  },
  fab: {
    position: 'absolute',
    width: 56,
    height: 56,
    alignItems: 'center',
    justifyContent: 'center',
    right: 20,
    bottom: 20,
    backgroundColor: '#03A9F4',
    borderRadius: 30,
    elevation: 8
  },
  fabIcon: {
    fontSize: 40,
    color: 'white'
  }
})