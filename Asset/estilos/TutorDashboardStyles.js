import {
    StyleSheet
  } from 'react-native'

  export const TutorDashboardStyles = StyleSheet.create({
    boxData:{
      flexDirection: 'column',
      alignSelf: 'center',
      borderRadius: 12,
      backgroundColor: '#fff',
      marginTop: 0,
      width: '90%',
      backgroundColor:"white",
      borderRadius:15,
      padding:10,
      elevation:10,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 3 },
      shadowOpacity: 0.5,
      shadowRadius: 5,
      marginBottom: 10
    },
    boxDataTitle:{
      width:'100%',
      backgroundColor:'#C8E3D2',
      borderRadius:10,
      flexDirection:'row'
    },
    boxDataContentWidth:{
      width:'50%',
    },
    boxDataTitleAlignLeft:{
      padding:5,
      color:'black',
      fontSize:18,
      fontWeight:'bold',
      textAlign:'left',
      width: '50%'
    },
    boxDataTitleAlignRight:{
      padding:5,
      color:'black',
      fontSize:18,
      fontWeight:'bold',
      textAlign:'right',
      width: '50%'
    },
    flexDirectionRow:{
      flexDirection:'row',
      margin: 5
    },
    bold:{
      fontWeight:'bold',
      fontSize: 18
    },
    text:{
      fontSize: 18
    },
    
    /***Notificaciones***/
    flex1:{
      flex:1
    },
    row:{
      flexDirection:'row',
      width:'100%',
      backgroundColor:'white'
    },
    backgroundHeaderImage:{
      position: 'absolute',
      marginTop: '10%',
      height: '100%',
      width: '100%',
      resizeMode: 'stretch',
      zIndex: -10
    },
    BoxNotifs:{
      flexDirection: 'column',
      alignSelf: 'center',
      borderRadius: 12,
      backgroundColor: '#fff',
      marginTop: '2%',
      width: '90%',
      backgroundColor:"white",
      borderRadius:15,
      padding:10,
      elevation:10,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 3 },
      shadowOpacity: 0.5,
      shadowRadius: 5,
      marginBottom:'5%'
    },
    HeaderNotifsFont:{ 
      fontSize: 20
    },
    TitleNotifWidth:{
      width:'30%'
    },
    TitleNotifText:{
      fontWeight:'bold',
      fontSize: 16
    },
    ContentNotifWidth:{
      width:'60%'
    },
    ContentNotifText:{
      fontSize: 16
    },
    ContentIconWidth:{
      width: '10%'
    },

    /* Botones con estilo especial de imagen sobrepuesta */
    StyledButton: {
      alignSelf: 'center',
      borderRadius: 12,
      backgroundColor: '#fff',
      marginTop: '20%',
      width: '60%',
      height: 80,
      // 
      // height:150,
      // width:"80%",
      backgroundColor:"white",
      borderRadius:15,
      padding:10,
      elevation:10,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 3 },
      shadowOpacity: 0.5,
      shadowRadius: 5,
      marginBottom:'5%'
    },
    StyledButtonImage: {
      // alignSelf: 'center',
    },
    StyledButtonText: {
      textAlign: 'center',
      // marginTop: '-50%',
    },
    StyledButtonImageCircle: {
      alignSelf: 'center',
      marginTop: '-28%',
      /*marginVertical:'10%',*/
      top: 25,
      width: 80,
      height: 80,
    },
    StyledButtonTitle: {
      textAlign: 'center',
      fontSize: 22,
      fontWeight: 'bold',
      color: 'black',
      textAlignVertical: 'center',
      marginTop: '7%'
    },
    ScreenTitleText:{
      color:'white',
       fontSize:30,
       textAlign:'center',
       marginTop:30
    },
    ScreenTitleTextSub:{
      color:'white',
       fontSize:20,
       textAlign:'center',
       marginTop:30
    }
  });