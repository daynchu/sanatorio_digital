import { StyleSheet } from "react-native";
export const stylesMenu = StyleSheet.create({
  menuIconWidth: {
    width: "13%",
  },
  menuButton: {
    backgroundColor: "white",
  },
  row: {
    flexDirection: "row",
    width: "100%",
    backgroundColor: "white",
    marginTop: "-.7%",
  },
  ScreenTitleWidth: {
    width: "65%",
    marginVertical: 5,
  },
  ScreenTitleText: {
    marginTop: 10,
    fontSize: 20,
    color: "black",
    textAlign: "center",
  },
  selectInstitucionWidth: {
    marginVertical: 5,
    height: 45,
  },
});
