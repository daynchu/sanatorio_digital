import * as React from 'react'
import { View, StyleSheet, Text} from "react-native";
import {Picker} from '@react-native-community/picker';
import Icons from 'react-native-vector-icons/Ionicons'
import { stylesMenu } from '../../../Asset/estilos/stylesMenu'
import { useStateIfMounted } from "use-state-if-mounted";

function DefinirColor(color) {
  switch (color) {
    case "medico":
      return '#92B913';
    case "tutor":
      return '#68B184';
    case "paciente":
      return '#3FA9F5';
    default:
      'black';
  }
}

export const MenuGeneral = ({ cadenaTitulo,navigation,color }) => {
  const [Institucion, SetInstitucion] = useStateIfMounted('30')
  return (
    <View style={stylesMenu.row}>    
      <View style={stylesMenu.menuIconWidth}>
        <Icons.Button style={stylesMenu.menuButton} name='ios-menu' size={40} color="black" onPress={() => { navigation.openDrawer() }} />
      </View>
      <View style={stylesMenu.ScreenTitleWidth}>
        <Text style={[stylesMenu.ScreenTitleText, {color:DefinirColor(color)}]}>{cadenaTitulo}</Text>
      </View>
      <View style={stylesMenu.selectInstitucionWidth}>
        {(color == 'medico')&&
        <Picker
          mode='dialog'
          selectedValue={Institucion}
          style={{height: 50, width: 100}}
          onValueChange={(itemValue, itemIndex) => SetInstitucion(itemValue)}>
          <Picker.Item label="Seleccione Institucion" value="" color="#B2B1B1" />
          <Picker.Item label="Cimyn" value="30" color="black" />
          <Picker.Item label="Hospital Privado" value="40" color="black" />
        </Picker>
        }
      </View> 
    </View>
  )
}