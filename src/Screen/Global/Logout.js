import { AsyncStorage } from 'react-native';
import * as firebase from "firebase";
export const Logout = () => {
    firebase.auth().signOut();
    AsyncStorage.clear();
}








