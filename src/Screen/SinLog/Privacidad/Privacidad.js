import { Text, View, Image } from 'react-native'
import React, { Component } from 'react';
import { styles } from '../../../../Asset/estilos/Styles'
import { SafeAreaView } from 'react-native-safe-area-context';

const ImgDoctores = () => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.stretch}
        source={require('../../../../Asset/Images/Sinlog/Privacidad/doctores.png')}
      />
    </View>
  );
}

export default class verPrivacidad extends Component {

  render() {
    return (
      <SafeAreaView
        style={{ flex: 1, justifyContent: 'space-between' }}
      >
        <View>
          <View style={styles.col8}>
            <ImgDoctores></ImgDoctores>
          </View>
          <View style={styles.col8}>
            <Text style={styles.text}> Tu confidencialidad es muy importante para nosotros</Text>
          </View>
          <View style={styles.col8}>
            <Text style={styles.text}> Solo vos y tus doctores tendrán acceso a la información de tu salud</Text>
          </View>
        </View>
      </SafeAreaView>
    )
  }
}

