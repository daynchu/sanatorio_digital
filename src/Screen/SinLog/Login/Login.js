import { stylesGral } from "../../../../Asset/estilos/stylesGral";
import {
    TouchableOpacity,
    Text,
    View,
    StyleSheet,
    Alert,
    ActivityIndicator,
    TouchableHighlight,
    Image,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { Input, Button } from "react-native-elements";
import * as React from "react";
import InputScrollView from "react-native-input-scroll-view";
import { SafeAreaView } from "react-native-safe-area-context";
import { AuthContext } from "../../../../Component/context";
import { useStateIfMounted } from "use-state-if-mounted";
import { RG } from "../../../../Asset/estilos/Styles";
import * as firebase from "firebase";
import * as WebBrowser from "expo-web-browser";
import * as Google from "expo-google-app-auth";
import * as AppAuth from "expo-app-auth";
import * as SecureStore from "expo-secure-store";

const IOS_CLIENT_ID =
    "921371634398-ilqtn64r594h2ugh8mq9u9djfcs5q376.apps.googleusercontent.com";
const ANDROID_CLIENT_ID =
    "921371634398-dtv0nl6fjkk31lcartqvopfamm5l137i.apps.googleusercontent.com";

const ANDROID =
    "921371634398-p9up1obgqqalgphnc3f95ihfklou9nrm.apps.googleusercontent.com";

WebBrowser.maybeCompleteAuthSession();

export default function Login(props) {
    const { signIn } = React.useContext(AuthContext);
    const { signUp } = React.useContext(AuthContext);

    const Statelogin = {
        errorMessage: "",
        Usuario: "",
        Password: "",
        mostrarActividad: false,
    };

    const [Data, setData] = useStateIfMounted(Statelogin);
    const [mostrarActividad, SetMostrarActividad] = useStateIfMounted(false);
    const [Usuario, SetUsuario] = useStateIfMounted("");
    const [Password, SetPassword] = useStateIfMounted("");
    const [Loading, SetLoading] = useStateIfMounted(false);
    const [secure, SetSecure] = useStateIfMounted(true);

    const onSignIn = async (googleUser) => {
        // //console.log('Google Auth Response', googleUser);
        // We need to register an Observer on Firebase Auth to make sure auth is initialized.
        var unsubscribe = firebase.auth().onAuthStateChanged(
            function (firebaseUser) {
                unsubscribe();
                // Check if we are already signed-in Firebase with the correct user.
                // Build Firebase credential with the Google ID token.
                var credential = firebase.auth.GoogleAuthProvider.credential(
                    // googleUser.getAuthResponse().id_token);
                    googleUser.idToken,
                    googleUser.accessToken
                );
                // Sign in with credential from the Google user.
                firebase
                    .auth()
                    .signInWithCredential(credential)
                    .then((res) => {
                        SecureStore.deleteItemAsync("uidTokenSecure");
                        SecureStore.setItemAsync(
                            "uidTokenSecure",
                            firebase.auth().currentUser.uid
                        );
                        inicioSesion(firebase.auth().currentUser.uid);
                        //console.log(firebase.auth().currentUser.uid)
                    })
                    .catch(function (error) {
                        // Handle Errors here.
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        // The email of the user's account used.
                        var email = error.email;
                        // The firebase.auth.AuthCredential type that was used.
                        var credential = error.credential;
                        // ...
                    });
            }.bind(this)
        );
    };

    const signInWithGoogle = async () => {
        SetLoading(true);

        const result = await Google.logInAsync({
            iosClientId: IOS_CLIENT_ID,
            androidClientId: ANDROID_CLIENT_ID,
            androidStandaloneAppClientId: ANDROID,
            scopes: ["profile", "email"],
            // behavior: 'web',
            // redirectUrl: `com.excelencia.sanatorial:/oauthredirect`,
            redirectUrl: `${AppAuth.OAuthRedirect}:/oauthredirect`,
        });

        // console.log(AppAuth.OAuthRedirect)
        if (result.type === "success") {
            // Then you can use the Google REST API
            let userInfoResponse = await fetch(
                "https://www.googleapis.com/userinfo/v2/me",
                {
                    headers: { Authorization: `Bearer ${result.accessToken}` },
                }
            );

            onSignIn(result);
            // SetLoading(false)
            return result.accessToken;
        } else {
            SetLoading(false);
            return { cancelled: true };
        }
    };
    const checkLogin = async () => {
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                console.log("user is logged");
            }
        });
    };

    const inicioSesion = async (uid) => {
        try {
            fetch(
                "https://app.excelenciadigital.ml/SanatorioDigitalApi/usuarios/uid/" +
                    uid +
                    ""
            )
                .then((response) => {
                    return response.ok
                        ? response.json()
                        : Promise.reject("Error en la conexión");
                })
                .then((res) => {
                    if (res == null) {
                        SetLoading(false);
                        props.navigation.navigate("postgoogle", {
                            uidFire: uid,
                        });
                    } else {
                        SetLoading(false);
                        signUp({ res });
                    }
                });
        } catch (error) {
            //console.log(error)
        }
    };
    const ViewPass = () => {
        return (
            <TouchableOpacity
                onPress={() => {
                    SetSecure(!secure);
                }}
            >
                {secure ? (
                    <Icon name="eye-slash" size={26} color="black" />
                ) : (
                    <Icon name="eye" size={26} color="black" />
                )}
            </TouchableOpacity>
        );
    };
    return Loading ? (
        <View
            style={[stylesGral.containerLoading, stylesGral.horizontalLoading]}
        >
            <ActivityIndicator size="large" />
        </View>
    ) : (
        <SafeAreaView style={{ flex: 1, justifyContent: "space-between" }}>
            <View style={stylesGral.container}>
                <Image
                    source={require("../../../../Asset/Images/Medico/movilApp.png")}
                    style={stylesGral.backgroundImage}
                />
                <Image
                    source={require("../../../../Asset/Images/Medico/todos.png")}
                    style={stylesGral.backgroundPers}
                />
                <InputScrollView>
                    <View style={stylesGral.textContent}>
                        <Image
                            source={require("../../../../Asset/Images/Medico/logo.png")}
                            style={stylesGral.Logo}
                        />
                        <Text style={stylesGral.textTitle}>
                            Sanatorio Digital
                        </Text>
                        <Text style={stylesGral.textSubtitle}>
                            Cuidarte es nuestra prioridad
                        </Text>
                    </View>
                    <View style={stylesGral.Card}>
                        <Text style={stylesGral.textTitleCard}>
                            Iniciar Sesion
                        </Text>
                        <View style={[stylesGral.loadingContainer]}>
                            {mostrarActividad && (
                                <ActivityIndicator
                                    size="large"
                                    color="#00ff00"
                                />
                            )}
                        </View>
                        <View style={stylesGral.textInputs}>
                            <Input
                                placeholder="E-Mail"
                                placeholderTextColor="#D3D3D3"
                                value={Usuario}
                                onChangeText={(text) =>
                                    SetUsuario(text.replace(/\s/g, ""))
                                }
                            ></Input>
                            <Input
                                placeholder="Contraseña"
                                placeholderTextColor="#D3D3D3"
                                secureTextEntry={secure}
                                value={Password}
                                onChangeText={(text) =>
                                    SetPassword(text.replace(/\s/g, ""))
                                }
                                rightIcon={ViewPass()}
                            ></Input>
                        </View>
                        <View style={stylesGral.boxButtons}>
                            <TouchableOpacity
                                style={stylesGral.buttons}
                                onPress={() => {
                                    signIn({ Usuario, Password });
                                }}
                            >
                                <Text style={stylesGral.textButtons}>
                                    ¡ Vamos !
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <TouchableOpacity
                                style={stylesGral.buttons}
                                onPress={() => {
                                    signInWithGoogle();
                                }}
                            >
                                <Text style={stylesGral.textButtons}>
                                    Iniciar con{" "}
                                    <Image
                                        source={require("../../../../Asset/Images/Medico/gmaiLogo.png")}
                                        style={stylesGral.logoGoogle}
                                    />
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </InputScrollView>
            </View>
        </SafeAreaView>
    );
}

function LoginAlert(msg) {
    Alert.alert(
        "Ha ocurrido un error!",
        msg,
        [
            //{ text: "OK", onPress: () => console.log("Alerta cerrada") }
        ],
        { cancelable: true }
    );
}
