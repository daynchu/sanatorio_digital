import * as React from 'react'
import { Input, Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome'
import { TouchableOpacity, View, Text, Alert, ActivityIndicator, Picker, Platform, ActionSheetIOS, TouchableHighlight, BackHandler, ToastAndroid, AlertIOS, Image } from 'react-native'
import { styles, RG } from '../../../../Asset/estilos/Styles'
import InputScrollView from 'react-native-input-scroll-view';
import { SafeAreaView } from 'react-native-safe-area-context';
import { AuthContext } from '../../../../Component/context'
import { useStateIfMounted } from "use-state-if-mounted";
import { stylesGral } from '../../../../Asset/estilos/stylesGral'



// @author Andres

export default function registroA(props) {

    const { signUp } = React.useContext(AuthContext)
    const { signIn } = React.useContext(AuthContext);


    const [Dni, SetDni] = useStateIfMounted('')
    const [Sexo, SetSexo] = useStateIfMounted('Seleccionar Sexo')
    // const [Tel,SetTel] = useStateIfMounted('')
    const [Email, SetEmail] = useStateIfMounted('')
    const [Password, SetPassword] = useStateIfMounted('')
    const [Loading, SetLoading] = useStateIfMounted(false)
    const [selectedValue, setSelectedValue] = useStateIfMounted("");
    const [Validate, setValidate] = useStateIfMounted('')
    const [EmailValido, SetEmailValido] = useStateIfMounted(false)
    const [secure, SetSecure] = useStateIfMounted(true)
    const [msgErr, SetmsgErr] = useStateIfMounted("")


    React.useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    })
    function handleBackButtonClick() {
        props.navigation.goBack();
        return true;
    }


    const Ingresar = () => {
        signUp(
            { Email, Password }
        )
    }
    const ViewPass = () => {
        return (
            <TouchableOpacity onPress={() => { SetSecure(!secure) }}>
                {secure ? <Icon name='eye-slash' size={26} color='black' /> : <Icon name='eye' size={26} color='black' />}
            </TouchableOpacity>
        )
    }

    const crearCuenta = async () => {
        var data = {
            "dni": Dni,
            "sexo": Sexo,
            "email": Email,
            "password": Password,
        }
        fetch("https://app.excelenciadigital.ml/SanatorioDigitalApi/usuarios/registro/", {
            method: "POST",
            headers: { "Content-Type": "application/json", },
            body: JSON.stringify(data)
        })
            .then(result => {
                if (result.status === 201) {
                    //console.log("Registrado con éxito!!")
                    Ingresar();
                }
                else {
                    result.text().then(reason => {
                        if (reason == "Solicitud incorrecta") {
                            AlertMessageError(`Ingrese contraseña que contenga: 

-Letras mayúsculas
-Letras minúsculas 
-Caracteres numéricos
-Longitud de contraseña: como mínimo 6 caracteres`)
                        } else {
                            AlertMessageError(reason)
                        }
                    })
                }
            }).catch(reason => {
                SetLoading(false)
            })
    }

    const ValidarEmail = (text) => {
        var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if ((reg.test(text) === false) && (Email.length > 4)) {
            SetmsgErr("Ingrese Email Valido")
            SetEmail(text)
        }
        else {
            SetEmail(text)
            SetEmailValido(true)
            SetmsgErr(" ")
        }
    }



    return (
        Loading ?
            <View style={[RG.containerLoad, RG.horizontalLoad]}>
                <ActivityIndicator size="large" />
            </View>
            :
            <SafeAreaView
                style={{ flex: 1, justifyContent: 'space-between' }}
            >
                <View style={stylesGral.container}>
                    <Image source={require('../../../../Asset/Images/Medico/movilApp.png')} style={stylesGral.backgroundImage} />
                    <Image source={require('../../../../Asset/Images/Medico/todos.png')} style={stylesGral.backgroundPers} />
                    <InputScrollView useAnimatedScrollView topOffset={50}>
                        <View style={stylesGral.textContent}>
                            <Image source={require('../../../../Asset/Images/Medico/logo.png')} style={stylesGral.Logo} />
                            <Text style={stylesGral.textTitle}>Sanatorio Digital</Text>
                            <Text style={stylesGral.textSubtitle}>Cuidarte es nuestra prioridad</Text>
                        </View>
                        <View style={stylesGral.CardRegister}>
                            <Text style={stylesGral.textTitleCard}>REGISTRO</Text>
                            <View style={stylesGral.textInputs}>
                                <Input placeholder='DNI' style={{ borderWidth: 0, borderBottomWidth: 1 }} type="phone" placeholderTextColor='#D3D3D3' keyboardType="numeric" value={Dni} onChangeText={SetDni} ></Input>
                                {/* select */}
                                {(Platform.OS !== 'ios') ?
                                    <Picker
                                        mode='dialog'
                                        selectedValue={Sexo}
                                        onValueChange={(itemValue, itemIndex) => SetSexo(itemValue)}>
                                        <Picker.Item label="Seleccione Sexo" value="" color="#D3D3D3" />
                                        <Picker.Item label="Hombre" value="M" color="black" />
                                        <Picker.Item label="Mujer" value="F" color="black" />
                                    </Picker>
                                    :
                                    <TouchableHighlight
                                        style={{ marginBottom: 20, backgroundColor: '#D3D3D3' }}
                                        onPress={() => {
                                            ActionSheetIOS.showActionSheetWithOptions(
                                                {
                                                    options: ["Cerrar", "Hombre", "Mujer"],
                                                    cancelButtonIndex: 0
                                                },
                                                buttonIndex => {
                                                    if (buttonIndex === 0) {
                                                        // cancel action
                                                    } else if (buttonIndex === 1) {
                                                        SetSexo('M');
                                                    } else if (buttonIndex === 2) {
                                                        SetSexo('F');
                                                    }
                                                }
                                            )
                                        }}
                                    >
                                        <Text style={{ color: 'black', fontSize: 20, padding: 5 }}>{
                                            Sexo != 'Seleccionar Sexo' ?
                                                (Sexo == 'M' ? 'Hombre' : 'Mujer')
                                                : 'Seleccionar Sexo'
                                        }</Text>
                                    </TouchableHighlight>

                                }
                                <Input style={{ borderWidth: 0, borderBottomWidth: 1 }} placeholder='E-Mail' type="email" placeholderTextColor='#D3D3D3' value={Email} onChangeText={(text) => { ValidarEmail(text) }} errorStyle={{ color: 'red' }} renderErrorMessage errorMessage={msgErr} ></Input>
                                <Input style={{ borderWidth: 0, borderBottomWidth: 1 }} rightIcon={ViewPass()} placeholder='Contraseña' type="password" placeholderTextColor='#D3D3D3' value={Password} secureTextEntry={secure} onChangeText={SetPassword}  ></Input>

                                {/*  */}
                            </View>
                            <View style={stylesGral.boxButtons}>
                                <TouchableOpacity style={stylesGral.buttons} onPress={() => {
                                    if((Dni === '') || (Dni.length < 6))
                                    {
                                        AlertMessageError("Ingrese Dni Valido")
                                    }else if(Sexo === 'Seleccionar Sexo')
                                    {
                                        AlertMessageError("Seleccion Sexo por favor")
                                    }
                                    else{
                                        crearCuenta()
                                    }
                                }} >
                                    <Text style={stylesGral.textButtons}>Registrarme</Text>
                                </TouchableOpacity>
                            </View>

                        </View>

                    </InputScrollView>

                </View>
            </SafeAreaView>
    )
}

function AlertMessageError(msg) {
    Alert.alert(
        "Ha ocurrido un error",
        msg,
        [
            { text: "OK" }
        ],
        { cancelable: true }
    )
};
