import * as React from "react";
import { Input, Button } from "react-native-elements";
import Icon from "react-native-vector-icons/FontAwesome";
import {
  TouchableOpacity,
  View,
  Text,
  Alert,
  ActivityIndicator,
  Picker,
  Platform,
  ActionSheetIOS,
  TouchableHighlight,
  BackHandler,
  ToastAndroid,
  AlertIOS,
} from "react-native";
import { styles, RG } from "../../../../Asset/estilos/Styles";
import * as firebase from "firebase";
import InputScrollView from "react-native-input-scroll-view";
import { SafeAreaView } from "react-native-safe-area-context";
import { AuthContext } from "../../../../Component/context";
import { useStateIfMounted } from "use-state-if-mounted";

import InputText from "react-native-input-validator";
import { color } from "react-native-reanimated";

// @author Andres

export default function postgoogle(props) {
  const { signUp } = React.useContext(AuthContext);
  const { uidFire } = props.route.params;

  const [Dni, SetDni] = useStateIfMounted("");
  const [Sexo, SetSexo] = useStateIfMounted("Seleccionar Sexo");
  const [Email, SetEmail] = useStateIfMounted("");
  const [Password, SetPassword] = useStateIfMounted("");
  const [Loading, SetLoading] = useStateIfMounted(false);

  React.useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        "hardwareBackPress",
        handleBackButtonClick
      );
    };
  });
  function handleBackButtonClick() {
    props.navigation.goBack();
    return true;
  }

  const datosUsu = (uid) => {
    fetch(
      "https://app.excelenciadigital.ml/SanatorioDigitalApi/usuarios/uid/" +
        uid +
        ""
    )
      .then((response) => {
        return response.ok
          ? response.json()
          : Promise.reject("Error en la conexión");
      })
      .then((res) => {
        var respuesta = {};
        respuesta = { res };
        console.log(respuesta);
        signUp(respuesta);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const crearCuenta = async () => {
    var data = {
      uid: uidFire,
      dni: Dni,
      sexo: Sexo,
    };
    console.log(JSON.stringify(data));
    //console.log(data)
    fetch(
      "https://app.excelenciadigital.ml/SanatorioDigitalApi/usuarios/registro/",
      {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(data),
      }
    )
      .then((result) => {
        if (result.ok) {
          console.log("esta ok");
          datosUsu(uidFire);
        } else {
          result.text().then((reason) => {
            console.log(reason);
            if (reason == "Solicitud incorrecta") {
              AlertMessageError(`Tenemos problemas al comunicarnos.`);
            } else {
              AlertMessageError(`Tenemos problemas al comunicarnos.`);
            }
          });
        }
      })
      .catch((reason) => {
        //console.log(reason)
      });
  };

  return Loading ? (
    <View style={[RG.container, RG.horizontal]}>
      <ActivityIndicator size="large" />
    </View>
  ) : (
    <SafeAreaView style={{ flex: 1, justifyContent: "space-between" }}>
      <View style={RG.containerWelcomeG}>
        <InputScrollView useAnimatedScrollView topOffset={50}>
          <Text style={RG.textStyleGiN}>Ya casi estamos</Text>
          <Text
            style={{
              fontSize: 25,
              marginHorizontal: 30,
              marginTop: 30,
              color: "#F5FAFD",
            }}
          >
            Por favor, ingresa estos datos para continuar.
          </Text>
          <View style={styles.col8}>
            <View style={RG.mt1}>
              <View style={RG.container}>
                <View style={RG.inputLabel}>
                  <InputText
                    placeholder="DNI"
                    style={{
                      borderWidth: 0,
                      borderBottomWidth: 1,
                      borderBottomColor: "#F0F0F0",
                    }}
                    type="phone"
                    placeholderTextColor="#F5FAFD"
                    keyboardType="numeric"
                    value={Dni}
                    onChangeText={SetDni}
                  ></InputText>
                </View>
              </View>
              <View style={RG.container}>
                <View
                  style={
                    (RG.inputLabel,
                    {
                      paddingTop: 15,
                      borderWidth: 0,
                      borderBottomWidth: 1,
                      borderBottomColor: "#F0F0F0",
                    })
                  }
                >
                  {Platform.OS !== "ios" ? (
                    <Picker
                      mode="dialog"
                      selectedValue={Sexo}
                      style={{ height: 50, width: 275 }}
                      onValueChange={(itemValue, itemIndex) =>
                        SetSexo(itemValue)
                      }
                    >
                      {/* <Picker.Item label="" value="" /> */}
                      <Picker.Item
                        label="Seleccione Sexo"
                        value=""
                        color="#F5FAFD"
                      />
                      <Picker.Item label="Hombre" value="M" color="black" />
                      <Picker.Item label="Mujer" value="F" color="black" />
                    </Picker>
                  ) : (
                    <TouchableHighlight
                      style={{ marginBottom: 20, backgroundColor: "#F5FAFD" }}
                      onPress={() => {
                        ActionSheetIOS.showActionSheetWithOptions(
                          {
                            options: ["Cerrar", "Hombre", "Mujer"],
                            cancelButtonIndex: 0,
                          },
                          (buttonIndex) => {
                            if (buttonIndex === 0) {
                              // cancel action
                            } else if (buttonIndex === 1) {
                              SetSexo("M");
                            } else if (buttonIndex === 2) {
                              SetSexo("F");
                            }
                          }
                        );
                      }}
                    >
                      <Text
                        style={{ color: "black", fontSize: 20, padding: 5 }}
                      >
                        {Sexo != "Seleccionar Sexo"
                          ? Sexo == "M"
                            ? "Hombre"
                            : "Mujer"
                          : "Seleccionar Sexo"}
                      </Text>
                    </TouchableHighlight>
                  )}
                </View>
              </View>
            </View>
          </View>
          <View style={RG.containerButtonLoginG}>
            <View style={RG.buttonGoWrapper}>
              <TouchableOpacity
                style={RG.buttonGoTouchable}
                onPress={() => {
                  crearCuenta();
                }}
              >
                <Text
                  style={{
                    color: "#3FA9F5",
                    fontSize: 30,
                    textAlign: "center",
                    padding: 2,
                  }}
                >
                  ¡Listo!
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </InputScrollView>
      </View>
    </SafeAreaView>
  );
}

function AlertMessageError(msg) {
  Alert.alert("Ha ocurrido un error", msg, [{ text: "OK" }], {
    cancelable: true,
  });
}
function AlertMessageWorking(msg) {
  Alert.alert("Perfecto...", msg, [{ text: "OK" }], { cancelable: true });
}
