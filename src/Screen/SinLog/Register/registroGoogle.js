import * as React from 'react'
import { Input, Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome'
import { TouchableOpacity, View, Text, Alert, ActivityIndicator, Picker, Platform, ActionSheetIOS, TouchableHighlight, BackHandler, ToastAndroid, AlertIOS, Image } from 'react-native'
import { styles, RG } from '../../../../Asset/estilos/Styles'
import * as firebase from "firebase";
import InputScrollView from 'react-native-input-scroll-view';
import { SafeAreaView } from 'react-native-safe-area-context';
import { AuthContext } from '../../../../Component/context'
import { useStateIfMounted } from "use-state-if-mounted";
import * as Google from "expo-google-app-auth";
import { stylesGral } from '../../../../Asset/estilos/stylesGral';
import * as AppAuth from 'expo-app-auth'
import * as WebBrowser from 'expo-web-browser';

// @author Andres

const IOS_CLIENT_ID = "921371634398-ilqtn64r594h2ugh8mq9u9djfcs5q376.apps.googleusercontent.com";
const ANDROID_CLIENT_ID ="921371634398-dtv0nl6fjkk31lcartqvopfamm5l137i.apps.googleusercontent.com";
const ANDROID = "921371634398-p9up1obgqqalgphnc3f95ihfklou9nrm.apps.googleusercontent.com"

WebBrowser.maybeCompleteAuthSession();

export default function registro(props) {

    const { signUp } = React.useContext(AuthContext)
    const { signIn } = React.useContext(AuthContext);

    // const [Tel,SetTel] = useStateIfMounted('')
    const [Email, SetEmail] = useStateIfMounted('')
    const [Password, SetPassword] = useStateIfMounted('')
    const [Loading, SetLoading] = useStateIfMounted(false)





    React.useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    })
    function handleBackButtonClick() {
        props.navigation.goBack();
        return true;
    }



  const onSignIn = googleUser => {
    // //console.log('Google Auth Response', googleUser);
    // We need to register an Observer on Firebase Auth to make sure auth is initialized.
    var unsubscribe = firebase.auth().onAuthStateChanged(function (firebaseUser) {
      unsubscribe();
      // Check if we are already signed-in Firebase with the correct user.
      // Build Firebase credential with the Google ID token.
      var credential = firebase.auth.GoogleAuthProvider.credential(
        // googleUser.getAuthResponse().id_token);
        googleUser.idToken,
        googleUser.accessToken)
      // Sign in with credential from the Google user.
      firebase.auth()
        .signInWithCredential(credential)
        .then(res => {
          inicioSesion(firebase.auth().currentUser.uid)
          //console.log(firebase.auth().currentUser.uid)
        })
        .catch(function (error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          // The email of the user's account used.
          var email = error.email;
          // The firebase.auth.AuthCredential type that was used.
          var credential = error.credential;
          // ...
        });

    }.bind(this));
  }

  const signInWithGoogle = async () => {
    try {
      const result = await Google.logInAsync({
        iosClientId: IOS_CLIENT_ID,
        androidClientId: ANDROID_CLIENT_ID,
        androidStandaloneAppClientId: ANDROID,
        scopes: ["profile", "email"],
        redirectUrl: `com.excelencia.sanatorial:/oauthredirect`
        // redirectUrl: `${AppAuth.OAuthRedirect}:/oauthredirect`
      });
      SetLoading(true)
      if (result.type === "success") {
        onSignIn(result)
        // //console.log("LoginScreen.js.js 21 | ", result.user.givenName);
        // //console.log(result)
        // this.props.navigation.navigate("Profile", {
        //   username: result.user.givenName
        // }); //after Google login redirect to Profile
        return result.accessToken;
      } else {
        SetLoading(false)
        return { cancelled: true };
      }
    } catch (e) {
      //   //console.log('LoginScreen.js.js 30 | Error with login', e);
      return { error: true };
    }
  };


  const inicioSesion = async (uid) => {

    try {
      fetch("https://app.excelenciadigital.ml/SanatorioDigitalApi/usuarios/uid/" + uid + "")
        .then((response) => {
          return response.ok
            ? response.json()
            : Promise.reject("Error en la conexión");
        })
        .then((res) => {
          if (res == null) {
            SetLoading(false)
            props.navigation.navigate('postgoogle', { uidFire: uid })
          } else {
            SetLoading(false)
            signUp({ res })
          }
          // //console.log(res)
        })

    } catch (error) {
      //console.log(error)
    }

  }


  return (

  Loading ?
            <View style={[RG.container, RG.horizontal]}>
                <ActivityIndicator size="large" />
            </View>
            :
            <SafeAreaView
                style={{ flex: 1, justifyContent: 'space-between' }}
            >
                <Image source={require('../../../../Asset/Images/Sinlog/Landing/landing.png')} style={stylesGral.landing} />
                <View style={stylesGral.textContentIni}>
                    <Text style={stylesGral.textTitleIni}>Sanatorio Digital</Text>
                    <Text style={stylesGral.textSubtitleIni}>Cuidarte es nuestra prioridad</Text>
                </View>
                <View style={stylesGral.boxButtonsIni}>
                    <TouchableOpacity style={stylesGral.buttonsIni} onPress={() => {
                        signInWithGoogle()
                    }} >
                        <Text style={stylesGral.textButtonsIni}>Continuar con <Image style={stylesGral.logoGoogle} source={require('../../../../Asset/Images/Medico/gmaiLogo.png')} /></Text>
                    </TouchableOpacity>
                    <TouchableHighlight style={stylesGral.buttonsIni}>
                        <Text style={stylesGral.textButtonsIni} onPress={() => props.navigation.navigate("Registro")}>Otra Opcion</Text>
                    </TouchableHighlight>
                </View>
                
            </SafeAreaView>
    )
}


function AlertMessageError(msg) {
    Alert.alert(
        "Ha ocurrido un error",
        msg,
        [
            { text: "OK" }
        ],
        { cancelable: true }
    )
};
function AlertMessageWorking(msg) {
    Alert.alert(
        "Perfecto...",
        msg,
        [
            { text: "OK" }
        ],
        { cancelable: true }
    )
};
