import { TouchableOpacity,Text, View, Image } from 'react-native'
import { Button } from 'react-native-elements'
import * as React from 'react';
import { getTheme } from 'react-native-material-kit';
import { LG } from '../../../../Asset/estilos/Styles'
import { SafeAreaView } from 'react-native-safe-area-context';
import { stylesGral } from '../../../../Asset/estilos/stylesGral';
import { captureScreen } from "react-native-view-shot";
import * as Sharing from 'expo-sharing'; 


const theme = getTheme();

export default class Land extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <SafeAreaView
                style={{ flex: 1, justifyContent: 'space-between' }}
            >
                <Image source={require('../../../../Asset/Images/Sinlog/Landing/landing.png')} style={stylesGral.landing} />
                <View style={LG.conteiner}>
                    <Text style={LG.textStyle}> Hola :) </Text>
                    <View style={stylesGral.boxButtons}>
                        <TouchableOpacity style={stylesGral.buttonsLanding} onPress={() => this.props.navigation.navigate("Login")}>
                            <Text style={stylesGral.textButtons}>Soy usuario</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={stylesGral.boxButtons}>
                        <TouchableOpacity style={stylesGral.buttonsLanding} onPress={() => this.props.navigation.navigate("RegistroGo")}>
                            <Text style={stylesGral.textButtons}>Soy nuevo aquí</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}


