import * as React from 'react'
import { View, ActivityIndicator, Button, StyleSheet, TouchableOpacity, Alert, AsyncStorage, FlatList, ClippingRectangle } from 'react-native'
import { SwipeListView } from 'react-native-swipe-list-view';
import { Cl } from '../../../../Asset/estilos/Styles'
import { Text } from 'react-native-elements';
import { CreateAlert } from '../../Global/Alert';
import * as SecureStore from 'expo-secure-store';
import moment from 'moment';
import 'moment/locale/es'
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { isNull } from 'lodash';
import Icons from 'react-native-vector-icons/Ionicons'
import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'
import { useStateIfMounted } from 'use-state-if-mounted';
import { MenuGeneral } from "../../Global/Menu";

export default class MedicoTurno extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            cambio: false,
            doctorName: '',
            SSexo: '',
            FSeleccionada: '',
            respuestaTurno: {},
            listViewData: {},
            IdPrestador: '',
            ModalVisible: false,
            ModalVisible2: false,
            tema: "default",
            title: '',
            titleButton: '',
            subtitle: '',
            DatosPaciente: {},
            loading: false,
            titleBotonCancelar: ''
        }
    }
    SelectDia = async () => {
        const IdP = await AsyncStorage.getItem("IdPrestador")
        this.setState({ IdPrestador: IdP })
        var date = await SecureStore.getItemAsync('dia')
        this.setState({ FSeleccionada: date })
        this.BuscarPacientes()
    }
    getNameDoctor = async () => {
        const name = await AsyncStorage.getItem('UserName')
        const sexo = await AsyncStorage.getItem('UserSexo')

        let SaludoSexo = ''
        if (sexo === 'M') {
            SaludoSexo = 'Dr'
        }
        else {
            SaludoSexo = 'Dra'
        }
        this.setState({ doctorName: name })
        this.setState({ SSexo: SaludoSexo })

    }

    componentDidMount() {
        this.SelectDia()
        this.getNameDoctor()
    }

    componentDidUpdate(prevProps, prevState) {
        {
            if (prevState.listViewData !== this.state.listViewData) {
                //console.log("cambio")
            }
        }
    }




    atender = async () => {

        var nuevoEstado = 2;
        var estadoT = parseInt(this.state.DatosPaciente.item.idEstadoT)
        if (estadoT <= 2)
            nuevoEstado = 4;
        else if (estadoT == 4)
            nuevoEstado = 5;
        var datos = { idTurno: parseInt(this.state.DatosPaciente.item.key), estado: nuevoEstado };

        await fetch("https://app.excelenciadigital.ml/SanatorioDigitalApi/turnos/estado", {
            method: "POST",
            headers: { "Content-Type": "application/json", },
            body: JSON.stringify(datos)
        })
            .then(response => {
                if (response.status == 200) {
                    this.setState({ ModalVisible: false })
                    this.setState({ loading: true })
                    this.autorizar()
                }
            })
            .catch(reason => {
                CreateAlert('Algo salió mal', "Tuvimos un problema al validar la consulta")
                //console.log(reason);
            });
        this.setState({ ModalVisible: false })
        this.BuscarPacientes()
    }

    autorizar = async () => {
        var idTurno = parseInt(this.state.DatosPaciente.item.key)
        var datos = { idTurno: parseInt(this.state.DatosPaciente.item.key) }
        await fetch("https://app.excelenciadigital.ml/SanatorioDigitalApi/datos-afiliados/autorizarconsulta", {
            method: "POST",
            headers: { "Content-Type": "application/json", },
            body: JSON.stringify(datos)
        })
            .then((response) => {
                return response.ok
                    ? response.json()
                    : Promise.reject("Error en la conexión");

            }).then(res => {
                if (res.mensaje == "Particular validado") {
                    this.setState({ loading: false })
                    this.setState({ tema: "success" })
                    this.setState({ title: res.mensaje })
                    this.setState({ subtitle: "" })
                    this.setState({ ModalVisible2: true })
                } else {
                    this.setState({ loading: false })
                    this.setState({ tema: "warning" })
                    this.setState({ title: res.mensaje })
                    this.setState({ subtitle: "" })
                    this.setState({ ModalVisible2: true })
                }
            })
            .catch(e => {
                //console.log(e)
            })
    }
    BuscarPacientes() {
        //console.log(this.state.IdPrestador)
        fetch("https://app.excelenciadigital.ml/SanatorioDigitalApi/turnos/pacientespormedicoyfecha?IdInstitucion=38&IdPrestador=" + this.state.IdPrestador + "&Fecha=" + this.state.FSeleccionada + "")
            .then(response => {
                return response.ok ? response.json() : Promise.reject("algo va mal")
            })
            .then(turnos => {
                this.state.listViewData = this.setState({
                    listViewData: turnos.map(t => {
                        return {
                            key: `${t.idTurno}`,
                            idEstadoT: `${t.idEstadoTurno}`,
                            text: `${moment(t.inicio).format('HH:mm') + 'HS'}-${t.paciente}`
                        }
                    })
                })
                this.setState({ cambio: false })
            })
            .catch(reason => {
                //console.log(reason);
            });

    }

    handleBackButtonClick() {
        this.props.navigation.goBack();
        return true;
    }
    onRefresh() {
        this.BuscarPacientes()
    }
    handleClose() {
        this.setState({ ModalVisible: false })
    }
    handleClose2() {
        this.setState({ ModalVisible2: false })
        // this.setState({loading:false})
    }
    render() {
        moment.locale('es');
        return (

            <SafeAreaView
                style={{ flex: 1, justifyContent: 'space-between' }}
            >
                {this.state.loading ?
                    <View style={[styles.containerLoad, styles.horizontalLoad]}>
                        <ActivityIndicator size="large" color="#00ff00" />
                    </View>
                    :
                    <View>
                        <View style={styles.containerTop}>
                            <View style={styles.containerTopMedicalMenu}>
                                <Icons.Button style={styles.containerTopMedicalMenuIcon} name='ios-menu' size={30} backgroundColor='#9cd681' color='white' onPress={() => { this.props.navigation.openDrawer() }} />
                            </View>
                            <Text style={styles.containerText}>{this.state.SSexo}. {this.state.doctorName}</Text>
                        </View>
                        <View style={styles.containerTop}>
                            <Text style={styles.turnod}>Estos son tus turnos del día</Text>
                            <Text style={styles.turnod} >
                                {moment(this.state.FSeleccionada).format("dddd  D MMMM")}
                            </Text>
                        </View>
                        <View style={styles.containerTop1}>
                            <View style={styles.encabezado}>
                                {/* <Text style={styles.encabezadoItems}>Estado</Text> */}
                                <Text style={styles.encabezadoItems}>Hora</Text>
                                <Text style={styles.encabezadoItems}>Paciente</Text>
                            </View>
                        </View>
                        {this.state.listViewData !== isNull ? <FlatList
                            data={this.state.listViewData}
                            keyExtractor={(item) => item.key}
                            onRefresh={() => this.onRefresh()}
                            refreshing={this.state.cambio}
                            renderItem={(item) => {
                                return (
                                    <TouchableOpacity onPress={() => {
                                        //console.log(item)
                                        var hoy = new Date()
                                        var dd = String(hoy.getDate()).padStart(2, '0');
                                        var mm = String(hoy.getMonth() + 1).padStart(2, '0'); //January is 0!
                                        var yyyy = hoy.getFullYear();
                                        hoy = yyyy + '-' + mm + '-' + dd;
                                        if (hoy == this.state.FSeleccionada) {
                                            if (parseInt(item.item.idEstadoT) <= 2) {
                                                this.setState({ tema: "info" })
                                                this.setState({ titleButton: "SI" })
                                                this.setState({ titleBotonCancelar: "NO" })
                                                this.setState({ title: "Hey Doc..." })
                                                this.setState({ subtitle: "¿Deseas atender al paciente que seleccionaste?" })
                                                this.setState({ DatosPaciente: item })
                                                this.setState({ ModalVisible: true })
                                            } else if (parseInt(item.item.idEstadoT) == 4) {
                                                this.setState({ tema: "success" })
                                                this.setState({ titleButton: "Atendido" })
                                                this.setState({ titleBotonCancelar: "Volver" })
                                                this.setState({ subtitle: " " })
                                                this.setState({ title: " Todo Listo Doc? " })
                                                this.setState({ DatosPaciente: item })
                                                this.setState({ ModalVisible: true })
                                            }
                                        }
                                    }
                                    }>
                                        <View style={[styles.rowFront, styles.inlineWrapper]}>
                                            <Icon style={styles.inline15} name='checkbox-blank-circle' size={25} color={item.item.idEstadoT == 1 ? 'white' : '#0cd1e8'} />
                                            <Text style={[styles.text, styles.inline80]} >{item.item.text}</Text>
                                        </View>
                                    </TouchableOpacity>
                                );
                            }}
                        /> : null}
                        <SCLAlert
                            theme={this.state.tema}
                            show={this.state.ModalVisible}
                            title={this.state.title}
                            subtitle={this.state.subtitle}
                        >
                            <SCLAlertButton theme={this.state.tema} onPress={() => { this.atender() }}>
                                {this.state.titleButton}
                            </SCLAlertButton>
                            <SCLAlertButton theme="danger" onPress={() => { this.handleClose() }}>{this.state.titleBotonCancelar}</SCLAlertButton>
                        </SCLAlert>

                        <SCLAlert
                            theme={this.state.tema}
                            show={this.state.ModalVisible2}
                            title={this.state.title}
                            subtitle={this.state.subtitle}
                        >
                            <SCLAlertButton theme={this.state.tema} onPress={() => { this.handleClose2() }}>
                                Okay
                                        </SCLAlertButton>
                        </SCLAlert>

                    </View>
                }
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    containerText: {
        color: 'white',
        padding: 2,
        marginTop: -50,
        fontSize: 30,
    },
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    containerTop: {
        alignContent: 'center',
        alignItems: 'center',
        paddingTop: 0,
        backgroundColor: '#9cd681',
        padding: 3,
        height: 'auto',
        width: 'auto',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 1.62,

        elevation: 2,

    },
    containerTop1: {
        alignContent: 'center',
        alignItems: 'center',
        paddingTop: .5,
        backgroundColor: '#84d45fde',
        marginTop: 2.6,
        padding: 1,
        height: 'auto',
        width: 'auto',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.01,

        elevation: 4,

    },
    backTextWhite: {
        color: '#FFF',
    },
    rowFront: {
        alignItems: 'flex-start',
        backgroundColor: '#fff',
        borderBottomColor: '#FFF',
        borderBottomWidth: 1,
        justifyContent: 'center',
        height: 60,
        elevation: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
    },
    inlineWrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    rowBack: {
        alignItems: 'center',
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 15,
    },
    rowBackAutor: {
        backgroundColor: '#74F374',
        padding: 40,
    },
    rowBackAutorText: {
        color: '#fff'
    },
    rowBackDelet: {
        backgroundColor: '#F16060',
        padding: 40,
    },
    rowBackDeletText: {
        color: '#fff'
    },
    containerTopMedicalMenu: {
        backgroundColor: '#9CD681',
        height: 'auto',
        paddingLeft: 0,
        width: '100%',
        marginLeft: 2,
    },
    containerTopMedicalMenuIcon: {
        backgroundColor: '#9CD681',
    },
    text: {
        textAlign: 'left',
        fontSize: 18,
        marginTop: 15,
    },
    inline15: {
        width: '15%',
        /*height: 40*/
        marginTop: 20,
        marginLeft: '5%',
    },
    inline80: {
        width: '80%',
        /*height: 40*/
    },
    backRightBtn: {
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        width: 75,
    },
    backRightBtnLeft: {
        backgroundColor: 'blue',
        right: 150,
    },
    backRightBtnRight: {
        backgroundColor: 'red',
        right: 0,
    },
    turnod: {
        fontSize: 20,
        color: '#fff',
        fontWeight: 'bold',
    },
    encabezado: {
        flexDirection: 'row',
        backgroundColor: '#84d45fde',
    },
    encabezadoItems: {
        width: '30%',
        marginLeft: 0,
        marginRight: 0,
        marginTop: 3,
        fontSize: 18,
        fontWeight: 'bold',
        justifyContent: "center",
        color: '#fff',
        backgroundColor: '#84d45fde',
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },

    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    containerLoad: {
        flex: 1,
        justifyContent: "center"
    },
    horizontalLoad: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    }
});
