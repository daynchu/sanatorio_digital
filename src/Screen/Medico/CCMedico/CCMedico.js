import {
    Text,
    View,
    Image,
    AsyncStorage,
    Keyboard,
    BackHandler,
    TouchableOpacity,
} from "react-native";
import * as React from "react";
import { Input } from "react-native-elements";
import { ccMedico } from "../../../../Asset/estilos/Styles";
import DateTimePickerModalInit from "react-native-modal-datetime-picker";
import DateTimePickerModalFinal from "react-native-modal-datetime-picker";
import moment from "moment";
import { CreateAlert } from "../../Global/Alert";
import { useStateIfMounted } from "use-state-if-mounted";
import { stylesMedico } from "../../../../Asset/estilos/stylesMedico";
import { SafeAreaView } from "react-native-safe-area-context";
import { MenuGeneral } from "../../Global/Menu";

export default function CuentaCorriente(props) {
    const [
        isDatePickerVisibleInit,
        setDatePickerVisibilityInit,
    ] = useStateIfMounted(false);
    const [
        isDatePickerVisibleFinal,
        setDatePickerVisibilityFinal,
    ] = useStateIfMounted(false);
    const [getDatePickedInit, setDatePickedInit] = useStateIfMounted(moment);
    const [getDatePickedFinal, setDatePickedFinal] = useStateIfMounted(moment);
    const [SaludoPorSexo, setSaludoPorSexo] = useStateIfMounted("");
    const [SaludoPorNombre, setSaludoPorNombre] = useStateIfMounted("");
    const [getData, setDatos] = useStateIfMounted([
        { id: 0, title: "", data: [null] },
    ]);
    const [getTotal, setTotal] = useStateIfMounted(0);
    const [IdPrestador, setIdprestador] = useStateIfMounted("");
    const [getInitDOM, setInitDOM] = useStateIfMounted();
    const [getFirstTime, setFirstTime] = useStateIfMounted(true);

    React.useEffect(() => {
        BackHandler.addEventListener(
            "hardwareBackPress",
            handleBackButtonClick
        );
        return () => {
            BackHandler.removeEventListener(
                "hardwareBackPress",
                handleBackButtonClick
            );
        };
    });
    function handleBackButtonClick() {
        props.navigation.goBack();
        return true;
    }

    React.useEffect(() => {
        BackHandler.addEventListener(
            "hardwareBackPress",
            handleBackButtonClick
        );
        return () => {
            BackHandler.removeEventListener(
                "hardwareBackPress",
                handleBackButtonClick
            );
        };
    });
    function handleBackButtonClick() {
        props.navigation.goBack();
        return true;
    }
    //ESTE HOOK SE EJECUTA LUEGO DE INICIAR LA PANTALLA, Y OBTIENE LA DATA DEL PRESTADOR
    React.useEffect(() => {
        async function ObtenerDatosPrestador() {
            const name = await AsyncStorage.getItem("UserName");
            const sexo = await AsyncStorage.getItem("UserSexo");
            const IdP = await AsyncStorage.getItem("IdPrestador");
            let SaludoSexo = "";
            if (sexo === "M") {
                SaludoSexo = "Dr";
            } else {
                SaludoSexo = "Dra";
            }
            setSaludoPorSexo(SaludoSexo);
            setSaludoPorNombre(name);
            setIdprestador(IdP);
            setInitDOM(true);
        }
        if (getFirstTime === true) {
            ObtenerDatosPrestador();
            setFirstTime(false);
        }
    }, [IdPrestador]);

    //ESTE HOOK SE EJECUTA AL INICIAR LA PANTALLA
    React.useEffect(() => {
        if (getInitDOM === true) {
            getFacturacionDoctor();
        }
    }, [getInitDOM]);

    //ESTE HOOK ESTA VIENDO LA FECHA DE INICIO
    React.useEffect(() => {
        if (getInitDOM === true) {
            getFacturacionDoctor();
        }
    }, [getDatePickedInit]);

    //ESTE HOOK ESTA VIENDO LA FECHA DE FIN
    React.useEffect(() => {
        if (getInitDOM === true) {
            getFacturacionDoctor();
        }
    }, [getDatePickedFinal]);

    //FUNCIONES DE FECHAS

    //**FUNCION QUE TRAE FETCH**//
    const getFacturacionDoctor = () => {
        const fetchVar =
            "https://app.excelenciadigital.ml/SanatorioDigitalApi/prestadores/consultarcuentacorriente?idInstitucion=38&IdPrestador=" +
            IdPrestador +
            "&FechaDesde=" +
            moment(getDatePickedInit).format("YYYY-MM-DD") +
            "&FechaHasta=" +
            moment(getDatePickedFinal).format("YYYY-MM-DD") +
            "";
        let Total = 0;
        setTotal(0);
        fetch(fetchVar)
            .then((response) => {
                return response.ok
                    ? response.json()
                    : Promise.reject("Error en la conexión");
            })
            .then((listado) => {
                const listening = [];
                listado.map((l) => {
                    let contador = 0;

                    const datosPaciente = [];
                    l.detalle.map((det) => {
                        contador++;
                        datosPaciente.push({
                            os: l.cabecera.nomObraSocial,
                            date: det.fecha,
                            name:
                                det.nombre.split(" ")[0] +
                                " " +
                                det.apellido.split(" ")[0],
                            dni: det.documento,
                            descripcion: det.descripcion,
                            id: det.id,
                            valor: det.total,
                            codigo: det.codigo,
                        });
                    });
                    const DataList = {
                        id: l.cabecera.idObrasocial,
                        title: {
                            cantidad: contador,
                            nombre: l.cabecera.nomObraSocial,
                            total: l.cabecera.totalObraSocial,
                        },
                        data: datosPaciente,
                    };
                    listening.push(DataList);

                    Total = Total + l.cabecera.totalObraSocial;
                    setTotal(Total);
                });
                if (!listening.length) {
                    setDatos(listening);
                } else {
                    setDatos(listening);
                }
            });
    };
    //**FUNCION QUE TRAE FETCH**//

    const validateDates = (dateIni, DateFinal) => {
        if (dateIni > DateFinal) {
            CreateAlert(
                SaludoPorSexo + " " + SaludoPorNombre.split(" ")[0],
                "Revisa la fecha \nEl fin del periodo debe ser mayor que el inicio"
            );
            setDatePickedFinal(getDatePickedInit);
        } else {
            //SE ENVIA IGUAL EL FETCH AUNQUE VALIDE ERRONEAMENTE. SE ENVIA A TRAVES DEL HOOK USEEFFECT
            // getFacturacionDoctor()
        }
    };
    const showDatePickerInit = () => {
        Keyboard.dismiss();
        setDatePickerVisibilityInit(true);
    };
    const showDatePickerFinal = () => {
        Keyboard.dismiss();
        setDatePickerVisibilityFinal(true);
    };
    const hideDatePickerInit = () => {
        Keyboard.dismiss();
        setDatePickerVisibilityInit(false);
    };
    const hideDatePickerFinal = () => {
        Keyboard.dismiss();
        setDatePickerVisibilityFinal(false);
    };
    const DateFromRenderInit = (date) => {
        // const fecha = moment(date).format("DD [de] MMMM");
        setDatePickedInit(date);
        hideDatePickerInit();
        validateDates(date, getDatePickedFinal);
        Keyboard.dismiss();
    };
    const DateFromRenderFinal = (date) => {
        // const fecha = moment(date).format("DD [de] MMMM");
        setDatePickedFinal(date);
        hideDatePickerFinal();
        validateDates(getDatePickedInit, date);
        Keyboard.dismiss();
    };
    const goToOS = async (datos) => {
        props.navigation.navigate("CuentaCorrienteObraSocial", {
            data: datos.data,
            id: datos.id,
            cabecera: datos.cabecera,
        });
    };
    // REVISAR ESTA BUSQUEDA PARA LIVESEARCH

    //CABECERAS DE LA TABLA

    const renderizaListView = () => {
      if (getData.length) {

            return getData.map((DataObraSocial) => {
                return (
                    <View key={DataObraSocial.id}>
                        <TouchableOpacity
                            onPress={() =>
                                goToOS({
                                    data: DataObraSocial.data,
                                    id: DataObraSocial.id,
                                    cabecera: DataObraSocial.title,
                                })
                            }
                        >
                            <View
                                style={{
                                    alignContent: "center",
                                    alignItems: "center",
                                    flexDirection: "row",
                                    justifyContent: "space-between",
                                    backgroundColor: "#D6E5B2",
                                    height: "auto",
                                    width: "auto",
                                    padding: 12,
                                    paddingLeft: 15,
                                    paddingRight: 15,
                                    marginBottom: "6%",
                                    // marginTop: 6,
                                    borderBottomWidth: 0.2,
                                    borderColor: "#20232a",
                                    borderRadius: 12,
                                    shadowColor: "#000",
                                    shadowOffset: {
                                        width: 5,
                                        height: 5,
                                    },
                                }}
                                key={DataObraSocial.id}
                            >
                                <Text
                                    style={{
                                        color: "black",
                                        fontSize: 15,
                                        fontWeight: "bold",
                                    }}
                                    key={DataObraSocial.title.cantidad}
                                >
                                    {DataObraSocial.title.cantidad}
                                </Text>
                                <Text
                                    style={{
                                        color: "black",
                                        fontSize: 18,
                                    }}
                                    key={DataObraSocial.title.nombre}
                                >
                                    {DataObraSocial.title.nombre}
                                </Text>
                                <Text
                                    style={{
                                        color: "black",
                                        fontSize: 15,
                                    }}
                                    key={DataObraSocial.title.total}
                                >
                                    Sub Total $ {DataObraSocial.title.total}
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                );
            });
        }
    };
    return (
        <SafeAreaView style={{ flex: 1, justifyContent: "space-between" }}>
            <MenuGeneral
                cadenaTitulo={"Cuenta Corriente"}
                navigation={props.navigation}
                color="medico"
            ></MenuGeneral>
            <Image
                source={require("../../../../Asset/Images/Medico/Fondo/Trazado261.png")}
                style={stylesMedico.backgroundHeaderImage}
            />
            <View style={stylesMedico.container}>
                <View style={stylesMedico.cardContainerCCMedicoFilter}>
                    <View style={ccMedico.containerTotalMedicalFilter}>
                        {/* <Icon
                                        {...props}
                                        name="search-plus"
                                        size={30}
                                        color="white"
                                    /> */}
                        <Text
                            style={{
                                fontSize: 20,
                                color: "#000",
                                textAlign: "center",
                                marginBottom: '5%',
                                // fontWeight: "bold",
                            }}
                        >
                            Filtros
                        </Text>
                    </View>
                    <View style={ccMedico.containerDatesPickers}>
                        <View style={ccMedico.containerButtonsInLine}>
                            <Text> Hasta</Text>
                            <Input
                                placeholder="Hasta"
                                value={moment(getDatePickedFinal).format(
                                    "DD [de] MMM YYYY"
                                )}
                                onFocus={showDatePickerFinal}
                            />
                        </View>
                        <View style={ccMedico.containerButtonsInLine}>
                            <Text> Desde</Text>
                            <Input
                                placeholder="Desde"
                                value={moment(getDatePickedInit).format(
                                    "DD [de] MMM YYYY"
                                )}
                                onFocus={showDatePickerInit}
                            />
                        </View>
                    </View>

                    <View></View>
                </View>
                <View style={stylesMedico.cardNoMarginTop}>
                    <View style={ccMedico.containerTotalMedical}>
                        <Text style={ccMedico.containerTotalText}>
                            Monto Total $ {getTotal}
                        </Text>
                    </View>

                    {renderizaListView()}

                    <View style={ccMedico.containerTotalMedicalFilter}></View>
                </View>

                <DateTimePickerModalInit
                    isVisible={isDatePickerVisibleInit}
                    mode="date"
                    onConfirm={DateFromRenderInit}
                    onCancel={hideDatePickerInit}
                />
                <DateTimePickerModalFinal
                    isVisible={isDatePickerVisibleFinal}
                    mode="date"
                    onConfirm={DateFromRenderFinal}
                    onCancel={hideDatePickerFinal}
                />
            </View>
        </SafeAreaView>
    );
}
