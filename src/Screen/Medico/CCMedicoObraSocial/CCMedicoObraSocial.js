import {
    Text,
    View,
    Image,
    AsyncStorage,
    BackHandler,
    TouchableHighlight,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    StyleSheet,
} from "react-native";
import * as React from "react";
import Icons from "react-native-vector-icons/Ionicons";
import { stylesMedico } from "../../../../Asset/estilos/stylesMedico";
import { SafeAreaView } from "react-native-safe-area-context";
import { useStateIfMounted } from "use-state-if-mounted";
import { AppLoading } from "expo";
import moment from "moment";
import { MenuGeneral } from "../../Global/Menu";
import { Modal, FAB, Portal, Provider, TextInput } from "react-native-paper";
import Select2 from "react-native-select-two";
import { Input } from "react-native-elements";

export default function CuentaCorrienteObraSocial(props) {
    const [getCabecera, setCabecera] = useStateIfMounted({
        cantidad: 0,
        nombre: "",
        total: 0,
    });
    const [getFirstDOM, setFirstDOM] = useStateIfMounted(false);
    const [getData, setData] = useStateIfMounted(null);
    const [getParams, setParams] = useStateIfMounted(null);
    const [getIdOS, setIdOs] = useStateIfMounted(0);
    const [state, setState] = useStateIfMounted({ open: false });
    const [visible, setVisible] = useStateIfMounted(false);
    const [visibleSeach, setVisibleSearch] = useStateIfMounted(false);

    const [getCodigosCargados, setCodigosCargados] = useStateIfMounted();
    const [getFilterPractice, setFilterPractice] = useStateIfMounted([]);
    const [getPickedCodes, setPickedCodes] = useStateIfMounted([]);
    const [getText, setText] = useStateIfMounted(null);
    const showModal = () => setVisible(true);
    const hideModal = () => setVisible(false);
    const showModalSeach = () => setVisibleSearch(true);
    const hideModalSearch = () => setVisibleSearch(false);
    const onStateChange = ({ open }) => setState({ open });
    const { open } = state;
    const pattern = /^([^0-9]*)$/;
    React.useEffect(() => {
        BackHandler.addEventListener(
            "hardwareBackPress",
            handleBackButtonClick
        );
        return () => {
            BackHandler.removeEventListener(
                "hardwareBackPress",
                handleBackButtonClick
            );
        };
    });
    function handleBackButtonClick() {
        props.navigation.goBack();
        return true;
    }

    React.useEffect(() => {
        BackHandler.addEventListener(
            "hardwareBackPress",
            handleBackButtonClick
        );
        return () => {
            BackHandler.removeEventListener(
                "hardwareBackPress",
                handleBackButtonClick
            );
        };
    });
    function handleBackButtonClick() {
        props.navigation.goBack();
        return true;
    }
    //ESTE HOOK SE EJECUTA LUEGO DE INICIAR LA PANTALLA, Y OBTIENE LA DATA DEL TURNO
    React.useEffect(() => {
        const { data } = props.route.params;
        const { id } = props.route.params;
        const { cabecera } = props.route.params;
        const codigosCargados = [];
        const listeningFilters = [];
        setData(data);
        setParams(data);
        setIdOs(id);
        setCabecera(cabecera);

        data.map((OS) => {
            if (!OS.codigo.match(pattern)) {
                if (codigosCargados != null && codigosCargados.length > 0) {
                    if (!codigosCargados.includes(OS.codigo)) {
                        // if(OS.descripcion === 'Consulta'){
                        const codigoConPuntos =
                            OS.codigo.slice(0, -4) +
                            "." +
                            OS.codigo.slice(2, -2) +
                            "." +
                            OS.codigo.slice(4);
                        const DataListFilters = {
                            id: OS.codigo,
                            name: codigoConPuntos + " - " + OS.descripcion,
                        };
                        listeningFilters.push(DataListFilters);
                        codigosCargados.push(OS.codigo);
                        // }
                    }
                } else {
                    const codigoConPuntos =
                        OS.codigo.slice(0, -4) +
                        "." +
                        OS.codigo.slice(2, -2) +
                        "." +
                        OS.codigo.slice(4);
                    const DataListFilters = {
                        id: OS.codigo,
                        name: codigoConPuntos + " - " + OS.descripcion,
                    };
                    listeningFilters.push(DataListFilters);
                    codigosCargados.push(OS.codigo);
                }
            }
        });
        setCodigosCargados(codigosCargados);
        setFilterPractice(listeningFilters);
        setFirstDOM(true);
    }, []);

    React.useEffect(() => {
        if (getFirstDOM !== false) {
            const data = [];
            if (getPickedCodes.length) {
                getParams.map((OS) => {
                    getPickedCodes.map((codes) => {
                        if (OS.codigo === codes) {
                            data.push({
                                codigo: OS.codigo,
                                date: OS.date,
                                descripcion: OS.descripcion,
                                dni: OS.dni,
                                id: OS.id,
                                name: OS.name,
                                os: OS.os,
                                valor: OS.valor,
                            });
                        }
                    });
                });
                setData(data);
            } else {
                setData(getParams);
            }
        }
    }, [getPickedCodes]);

    const goToDetails = async (datos) => {
        var json = JSON.stringify(datos);
        await AsyncStorage.setItem("CCMedicoData", json);
        props.navigation.navigate("CuentaCorrienteDetalle");
    };

    const SearchFilterFunction = (text) => {
        setText(text);
        //passing the inserted text in textinput
        if (text !== "") {
            const newData = getParams.filter(function (item) {
                //applying filter for the inserted text in search bar
                const nameWithDni = item.name + " " + item.dni;
                const itemData = item
                    ? nameWithDni.toUpperCase()
                    : "".toUpperCase();
                const textData = text.toUpperCase();
                return itemData.indexOf(textData) > -1;
            });
            setData(newData);
        } else {
            setData(getParams);
        }
    };

    return getData === null ? (
        <View
            style={[
                { flex: 1, justifyContent: "center" },
                {
                    flexDirection: "row",
                    justifyContent: "space-around",
                    padding: 10,
                },
            ]}
        >
            <ActivityIndicator size="large" color="#00ff00" />
        </View>
    ) : (
        <SafeAreaView
            style={{
                justifyContent: "space-between",
                marginBottom: "15%",
            }}
        >
            <MenuGeneral
                cadenaTitulo={"Detalle obras sociales"}
                navigation={props.navigation}
                color="medico"
            ></MenuGeneral>
            <Image
                source={require("../../../../Asset/Images/Medico/Fondo/Trazado261.png")}
                style={stylesMedico.backgroundHeaderImage}
            />

            <View style={stylesMedico.container}>
                <View
                    style={{
                        alignContent: "center",
                        alignItems: "center",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        backgroundColor: "#fff",
                        height: "auto",
                        width: "auto",
                        padding: 12,
                        paddingLeft: 15,
                        paddingRight: 15,
                        marginTop: "3%",
                        marginBottom: "3%",
                        borderColor: "#20232a",
                        borderRadius: 12,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 5,
                            height: 5,
                        },
                    }}
                    key={getCabecera.id}
                >
                    <Text
                        style={{
                            color: "black",
                            fontSize: 15,
                            fontWeight: "bold",
                        }}
                        key={getCabecera.cantidad}
                    >
                        {getCabecera.cantidad}
                    </Text>
                    <Text
                        style={{
                            color: "black",
                            fontSize: 18,
                        }}
                        key={getCabecera.nombre}
                    >
                        {getCabecera.nombre}
                    </Text>
                    <Text
                        style={{
                            color: "black",
                            fontSize: 15,
                        }}
                        key={getCabecera.total}
                    >
                        Sub Total $ {getCabecera.total}
                    </Text>
                </View>
                <ScrollView style={{ marginBottom: "12%" }}>
                    <View
                        style={{
                            borderRadius: 6,
                            backgroundColor: "#fff",
                            marginTop: "3%",
                            borderRadius: 15,
                            paddingLeft: 10,
                            paddingRight: 10,
                            elevation: 10,
                            shadowColor: "#000",
                            shadowOffset: { width: 0, height: 3 },
                            shadowOpacity: 0.5,
                            shadowRadius: 5,
                        }}
                    >
                        {/*
                       
                                    {getFilterPractice ? (
                                        <Select2
                                            // isSelectSingle
                                            style={{ borderRadius: 5 }}
                                            colorTheme="blue"
                                            popupTitle="Selecciona una practica"
                                            title="Pulsa para buscar por practica"
                                            searchPlaceHolderText="Aqui se busca"
                                            listEmptyTitle="No encontramos ninguna practica"
                                            cancelButtonText="Volver"
                                            selectButtonText="Quiero estas"
                                            data={getFilterPractice}
                                            onSelect={(data) => {
                                                setDataFilters(data);
                                            }}
                                            onRemoveItem={(data) => {
                                                setDataFilters(data);
                                            }}
                                        />
                                    ) : null}
                                    {getFilterPractice ? <View></View> : null}
                        
                        */}
                        {getData.map((OS) => {
                            return (
                                <TouchableOpacity
                                    onPress={() =>
                                        goToDetails({
                                            id: OS.id,
                                            nombre: OS.name,
                                            descripcion: OS.descripcion,
                                            fecha: OS.date,
                                            dni: OS.dni,
                                            valor: OS.valor,
                                            os: OS.os,
                                        })
                                    }
                                    key={OS.id}
                                    style={{
                                        backgroundColor: "transparent",
                                    }}
                                >
                                    <View
                                        style={{
                                            backgroundColor: "white",
                                            borderBottomWidth: 0.5,
                                            borderColor: "black",
                                            marginBottom: "1%",
                                            marginTop: "1%",
                                        }}
                                        key={OS.id}
                                    >
                                        <View
                                            style={{
                                                flexDirection: "row",
                                                alignContent: "center",
                                                alignItems: "center",
                                                flex: 1,
                                            }}
                                        >
                                            <Text
                                                style={{
                                                    fontSize: 17,
                                                    width: "65%",
                                                    flex: 1,
                                                }}
                                                key={OS.date}
                                            >
                                                {moment(OS.date).format(
                                                    "DD [de] MMMM [de] YYYY"
                                                )}
                                            </Text>
                                            <Text>
                                                {OS.valor !== 0 ? (
                                                    <Text
                                                        style={{
                                                            textAlign: "left",
                                                            fontSize: 18,
                                                            color: "#92B913",
                                                            textAlignVertical:
                                                                "center",
                                                        }}
                                                        key={OS.valor}
                                                    >
                                                        Total ${OS.valor}
                                                    </Text>
                                                ) : (
                                                    <Text
                                                        style={{
                                                            textAlign: "left",
                                                            fontSize: 18,
                                                            color: "#FE3737",
                                                            textAlignVertical:
                                                                "center",
                                                        }}
                                                        key={OS.valor}
                                                    >
                                                        Autorización pendiente
                                                    </Text>
                                                )}
                                            </Text>
                                        </View>
                                        <View
                                            style={{
                                                flexDirection: "row",
                                                alignContent: "center",
                                                flex: 1,
                                            }}
                                        >
                                            <Text
                                                key={OS.name}
                                                style={{
                                                    padding: 5,
                                                    fontSize: 15,
                                                }}
                                            >
                                                {OS.name}
                                            </Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            );
                        })}
                    </View>
                </ScrollView>
                <Provider>
                    <Portal>
                        <Modal
                            visible={visibleSeach}
                            onDismiss={hideModalSearch}
                            contentContainerStyle={{
                                backgroundColor: "transparent",
                            }}
                            dismissable={true}
                        >
                            <View
                                style={{
                                    alignContent: "center",
                                    alignItems: "center",
                                    alignSelf: "center",
                                    backgroundColor: "#fff",
                                }}
                            >
                                <TextInput
                                    value={getText}
                                    onChangeText={(value) => {
                                        SearchFilterFunction(value);
                                    }}
                                    style={{ height: 100, width: 300 }}
                                    label="Escriba DNI/Nombre/Apellido"
                                    autoFocus
                                    onBlur={() => hideModalSearch()}
                                ></TextInput>
                            </View>
                        </Modal>
                        <Modal
                            visible={visible}
                            onDismiss={hideModal}
                            contentContainerStyle={{
                                backgroundColor: "transparent",
                            }}
                            dismissable={true}
                        >
                            <Select2
                                initialMode={
                                    getPickedCodes.length ? false : true
                                }
                                style={{ borderRadius: 5 }}
                                colorTheme="blue"
                                popupTitle="Selecciona una practica"
                                title="Pulsa para buscar por practica"
                                searchPlaceHolderText="Aqui se busca"
                                listEmptyTitle="No encontramos ninguna practica"
                                cancelButtonText="Volver"
                                selectButtonText="Quiero estas"
                                data={getFilterPractice}
                                onSelect={(data) => {
                                    setPickedCodes(data);
                                    setVisible(false);
                                }}
                                onRemoveItem={(data) => {
                                    setPickedCodes(data);
                                    if (!data.length) {
                                        setVisible(false);
                                    }
                                }}
                            />
                        </Modal>
                        <FAB.Group
                            style={{
                                paddingBottom: "20%",
                                height: "100%",
                                width: "100%",
                                position: "relative",
                                left: "0%",
                                top: "0%",
                                bottom: "12%",
                                // borderRadius: 40,
                            }}
                            // small
                            open={open}
                            icon={open ? "close" : "filter"}
                            actions={[
                                {
                                    icon: "format-list-bulleted",
                                    label: "Práctica",
                                    onPress: () => showModal(),
                                },
                                {
                                    icon: "account-search",
                                    label: "Nombre/Apellido/DNI",
                                    onPress: () => showModalSeach(),
                                },
                            ]}
                            onStateChange={onStateChange}
                            onPress={() => {
                                if (open) {
                                    console.log("is closed!!");
                                    // do something if the speed dial is open
                                }
                            }}
                        />
                    </Portal>
                </Provider>
            </View>

            {/* </View> */}
        </SafeAreaView>
    );
}
