import * as React from 'react'
import { LocaleConfig } from 'react-native-calendars';
import { View, Text, Platform, BackHandler, ScrollView, AsyncStorage, Image, StyleSheet, FlatList, ActivityIndicator } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context';
import Icons from 'react-native-vector-icons/FontAwesome';
import Iconss from 'react-native-vector-icons/MaterialCommunityIcons'
import { CreateAlert } from '../../Global/Alert'
import { stylesMedico } from '../../../../Asset/estilos/stylesMedico'
import { TouchableOpacity } from 'react-native-gesture-handler';
import DateTimePicker from '@react-native-community/datetimepicker'
import { isNull } from 'lodash';
import {
    SCLAlert,
    SCLAlertButton
} from 'react-native-scl-alert'
var moment = require('moment');
import { MenuGeneral } from "../../Global/Menu";

LocaleConfig.locales['es'] = {
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene.', 'Feb.', 'Mar', 'Abr', 'May', 'Jun', 'Jul.', 'Ago', 'Sept.', 'Oct.', 'Nov.', 'Dic.'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
    dayNamesShort: ['Dom.', 'Lun.', 'Mar.', 'Mier.', 'Jue.', 'Vier.', 'Sab.'],
    today: 'Hoy'
};

LocaleConfig.defaultLocale = 'es'
const iOS = Platform.OS === 'ios';

export default class MedicoCal extends React.Component {
    _isMounted = false;
    constructor(props) {
        super(props)
        this.state = {
            IdPrestador: '',
            // Selected:'',
            // Fecha:'',
            idInstitucion: 38,
            fechasActivadas: [],
            fechasActivadasSinTurno: [],
            FechasCalendario: [],
            listViewData: [],
            cambio: false,
            show: false,
            hoy: new Date(),
            ModalVisible: false,
            ModalVisible2: false,
            tema: "default",
            title: '',
            titleButton: '',
            subtitle: '',
            titleBotonCancelar: '',
            DatosPaciente: {},
            loading: false,
            cantAtendido: 0,
            cantEspera: 0
        }
    }


    async getIdprestador() {
        const idpre = await AsyncStorage.getItem('IdPrestador')
        this.setState({ IdPrestador: idpre })
    }

    componentDidMount() {
        this.getIdprestador()
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    componentDidUpdate(prevProps, prevState) {
        this._isMounted = true;
        if (this._isMounted) {
            if (prevState.IdPrestador !== this.state.IdPrestador) {
                this.BuscarPacientes()
            }
        }
    }

    

    handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
    }

    onChange = (event, selectedDate) => {

        const currentDate = selectedDate || this.state.hoy;
        this.setState({ show: Platform.OS === 'ios' })
        this.setState({ hoy: currentDate })
        this.BuscarPacientes()
    };

    BuscarPacientes = () => {
        this.setState({ cantEspera: 0 })
        this.setState({ cantAtendido: 0 })
        fetch("https://app.excelenciadigital.ml/SanatorioDigitalApi/turnos/pacientespormedicoyfecha?IdInstitucion=38&IdPrestador=" + this.state.IdPrestador + "&Fecha=" + moment(this.state.hoy).format('YYYY-MM-DD') + "")
            .then(response => {
                return response.ok ? response.json() : Promise.reject("algo va mal")
            })
            .then(turnos => {
                this.state.listViewData = this.setState({
                    listViewData: turnos.map(t => {
                        if (t.idEstadoTurno == 4) {
                            this.setState(prevState => ({ cantAtendido: prevState.cantAtendido + 1 }))
                        } else if ((t.idEstadoTurno == 2) || (t.idEstadoTurno == 1)) {
                            this.setState(prevState => ({ cantEspera: prevState.cantEspera + 1 }))
                            // cantEspera++
                            //    this.setState({cantEspera: cantEspera++})
                        }
                        return {
                            key: `${t.idTurno}`,
                            idEstadoT: `${t.idEstadoTurno}`,
                            text: `${moment(t.inicio).format('HH:mm') + 'HS'}`,
                            nomb: `${t.paciente}`
                        }
                    })
                })
                this.setState({ cambio: false })
            })
            .catch(reason => {
                //console.log(reason);
            });
    }

    cambio(id) {
        if (id == 1) {
            return 'green'
        } else if (id == 2) {
            return '#21E3F0'
        } else if (id == 4) {
            return '#21E3F0'
        } else {
            return '#BBBBBB'
        }
    }

    atender = async () => {

        var nuevoEstado = 2;
        var estadoT = parseInt(this.state.DatosPaciente.item.idEstadoT)
        if (estadoT <= 2)
            nuevoEstado = 4;
        else if (estadoT == 4)
            nuevoEstado = 5;
        var datos = { idTurno: parseInt(this.state.DatosPaciente.item.key), estado: nuevoEstado };

        await fetch("https://app.excelenciadigital.ml/SanatorioDigitalApi/turnos/estado", {
            method: "POST",
            headers: { "Content-Type": "application/json", },
            body: JSON.stringify(datos)
        })
            .then(response => {
                if (response.status == 200) {
                    this.setState({ ModalVisible: false })
                    this.setState({ loading: true })
                    this.autorizar()
                }
            })
            .catch(reason => {
                CreateAlert('Algo salió mal', "Tuvimos un problema al validar la consulta")
                //console.log(reason);
            });
        this.setState({ ModalVisible: false })
        this.BuscarPacientes()
    }

    autorizar = async () => {
        var idTurno = parseInt(this.state.DatosPaciente.item.key)
        var datos = { idTurno: parseInt(this.state.DatosPaciente.item.key) }
        await fetch("https://app.excelenciadigital.ml/SanatorioDigitalApi/datos-afiliados/autorizarconsulta", {
            method: "POST",
            headers: { "Content-Type": "application/json", },
            body: JSON.stringify(datos)
        })
            .then((response) => {
                return response.ok
                    ? response.json()
                    : Promise.reject("Error en la conexión");

            }).then(res => {
                if (res.mensaje == "Particular validado") {
                    this.setState({ loading: false })
                    this.setState({ tema: "success" })
                    this.setState({ title: res.mensaje })
                    this.setState({ subtitle: "" })
                    this.setState({ ModalVisible2: true })
                } else {
                    this.setState({ loading: false })
                    this.setState({ tema: "warning" })
                    this.setState({ title: res.mensaje })
                    this.setState({ subtitle: "" })
                    this.setState({ ModalVisible2: true })
                }
            })
            .catch(e => {
                //console.log(e)
            })
    }
    onRefresh = () => {
        this.BuscarPacientes()
    }

    handleClose() {
        this.setState({ ModalVisible: false })
    }
    handleClose2() {
        this.setState({ ModalVisible2: false })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, justifyContent: 'space-between' }}>
                <Image source={require('../../../../Asset/Images/Medico/Fondo/Trazado261.png')} style={stylesMedico.backgroundHeaderImage} />
                <MenuGeneral cadenaTitulo={'Agenda de turnos'} navigation={this.props.navigation} color="medico"></MenuGeneral>
                {this.state.loading ?
                    <View style={[stylesMedico.containerLoad, stylesMedico.horizontalLoad]}>
                        <ActivityIndicator size="large" color="#00ff00" />
                    </View>
                    :
                    <ScrollView>
                    <View style={stylesMedico.container}>
                        <View style={stylesMedico.ContainerButton}>
                            {/* card butom1 */}
                            <View style={stylesMedico.cardAgenda}>
                                <View style={stylesMedico.cardImageAgendaA}>
                                    <Image source={require('../../../../Asset/Images/Medico/user-Medic.png')} style={stylesMedico.cardImageCircleA} />
                                </View>
                                <View style={stylesMedico.cardTitleAgenda}>
                                    <Text style={stylesMedico.cardTitleAgenda}>   Paciente {"\n"} por Atender</Text>
                                </View>
                                <View style={stylesMedico.cardNumeroAgenda}>
                                    <Text style={stylesMedico.cardNumeroAgenda}>{this.state.cantEspera}</Text>
                                </View>
                            </View>
                            {/* card butom 2 */}
                            <View style={stylesMedico.cardAgenda}>
                                <View style={stylesMedico.cardImageAgendaA}>
                                    <Image source={require('../../../../Asset/Images/Medico/user-Medic.png')} style={stylesMedico.cardImageCircleA} />
                                </View>
                                <View style={stylesMedico.cardTitleAgenda}>
                                    <Text style={stylesMedico.cardTitleAgenda}>   Paciente {"\n"} Atendidos</Text>
                                </View>
                                <View style={stylesMedico.cardNumeroAgenda}>
                                    <Text style={stylesMedico.cardNumeroAgenda}>{this.state.cantAtendido}</Text>
                                </View>
                            </View>
                        </View>


                        <View style={stylesMedico.ContainerListado}>
                            <View style={stylesMedico.encabezado}>
                                <View style={stylesMedico.TitleListad}>
                                    <Text style={{ fontSize: 20 }}>Pacientes del día {moment(this.state.hoy).format('dddd DD MMMM')}</Text>
                                </View>
                                <View style={stylesMedico.icono}>
                                    <TouchableOpacity onPress={() => { this.setState({ show: true }) }}>
                                        <Icons name="calendar" backgroundColor='#9CD681' size={30} color='black' />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={stylesMedico.encabezado}>
                                <View style={{ width: '20%' }}>
                                    <Text style={{ fontWeight: 'bold' }} >Estado</Text>
                                </View>
                                <View style={{ width: '30%' }}>
                                    <Text style={{ fontWeight: 'bold' }}  >Hora</Text>
                                </View>
                                <View style={{ width: '50%' }}>
                                    <Text style={{ fontWeight: 'bold' }} >Paciente</Text>
                                </View>
                            </View>
                            <View>
                                {this.state.listViewData !== isNull ? <FlatList
                                    data={this.state.listViewData}
                                    keyExtractor={(item) => item.key}
                                    onRefresh={() => this.onRefresh()}
                                    refreshing={this.state.cambio}
                                    renderItem={(item) => {
                                        return (
                                            <TouchableOpacity onPress={() => {

                                                var FechaActual = moment(new Date()).format('YYYY-MM-DD')
                                                if (moment(FechaActual).isSame(moment(this.state.hoy).format('YYYY-MM-DD'))) {
                                                    if (parseInt(item.item.idEstadoT) <= 2) {
                                                        this.setState({ tema: "info" })
                                                        this.setState({ titleButton: "SI" })
                                                        this.setState({ titleBotonCancelar: "NO" })
                                                        this.setState({ title: "Hey Doc..." })
                                                        this.setState({ subtitle: "¿Deseas atender al paciente que seleccionaste?" })
                                                        this.setState({ DatosPaciente: item })
                                                        this.setState({ ModalVisible: true })
                                                    } else if (parseInt(item.item.idEstadoT) == 4) {
                                                        this.setState({ tema: "success" })
                                                        this.setState({ titleButton: "Atendido" })
                                                        this.setState({ titleBotonCancelar: "Volver" })
                                                        this.setState({ subtitle: " " })
                                                        this.setState({ title: " Todo Listo Doc? " })
                                                        this.setState({ DatosPaciente: item })
                                                        this.setState({ ModalVisible: true })
                                                    }
                                                }
                                            }
                                            }>
                                                <View style={stylesMedico.encabezado}>
                                                    <Iconss style={{ width: '20%' }} name='checkbox-blank-circle' size={25} color={this.cambio(item.item.idEstadoT)} />
                                                    <Text style={{ width: '30%', fontSize: 18 }} >{item.item.text}</Text>
                                                    <Text style={{ width: '50%', fontSize: 18 }} >{item.item.nomb}</Text>
                                                </View>
                                            </TouchableOpacity>
                                        );
                                    }}
                                /> : null}
                            </View>
                            {this.state.show && (
                                <DateTimePicker
                                    testID="dateTimePicker"
                                    value={this.state.hoy}
                                    mode={'date'}
                                    is24Hour={true}
                                    display="default"
                                    onChange={this.onChange}
                                    minimumDate={new Date()}
                                />
                            )}
                            <SCLAlert
                                theme={this.state.tema}
                                show={this.state.ModalVisible}
                                title={this.state.title}
                                subtitle={this.state.subtitle}
                            >
                                <SCLAlertButton theme={this.state.tema} onPress={() => { this.atender() }}>
                                    {this.state.titleButton}
                                </SCLAlertButton>
                                <SCLAlertButton theme="danger" onPress={() => { this.handleClose() }}>{this.state.titleBotonCancelar}</SCLAlertButton>
                            </SCLAlert>
                            <SCLAlert
                                theme={this.state.tema}
                                show={this.state.ModalVisible2}
                                title={this.state.title}
                                subtitle={this.state.subtitle}
                            >
                                <SCLAlertButton theme={this.state.tema} onPress={() => { this.handleClose2() }}>
                                    Okay
                                        </SCLAlertButton>
                            </SCLAlert>
                        </View>
                        <View style={stylesMedico.ContainerListado}>
                            <View style={stylesMedico.encabezado}>
                                <Iconss style={{ width: '20%' }} name='checkbox-blank-circle' size={25} color="green" />
                                <Text style={{ width: '80%', fontSize: 18 }} >En espera</Text>
                            </View>
                            <View style={stylesMedico.encabezado}>
                                <Iconss style={{ width: '20%' }} name='checkbox-blank-circle' size={25} color="#21E3F0" />
                                <Text style={{ width: '80%', fontSize: 18 }} >Atendido</Text>
                            </View>
                            <View style={stylesMedico.encabezado}>
                                <Iconss style={{ width: '20%' }} name='checkbox-blank-circle' size={25} color="#BBBBBB" />
                                <Text style={{ width: '80%', fontSize: 18 }} >Atencion Finalizada</Text>
                            </View>
                        </View>
                    </View>
                    </ScrollView>
                }
            </SafeAreaView>
        )
    }
}

