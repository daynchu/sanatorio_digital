import { StyleSheet, Image, Alert, Text, View, BackHandler } from 'react-native'
import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';
import { useStateIfMounted } from "use-state-if-mounted";
import { AuthContext } from '../../../../Component/context'
import { stylesMedico } from '../../../../Asset/estilos/stylesMedico'
import { SafeAreaView } from 'react-native-safe-area-context'
import { MenuGeneral } from "../../Global/Menu";


 const MedicoInicio = ({ navigation }) => {

    const [doctorName, SetDoctorName] = useStateIfMounted('')
    const [SSexo, SetSSexo] = useStateIfMounted('')
    const { signOut } = React.useContext(AuthContext);


    const [UserName, SetUsername] = useStateIfMounted('')

    React.useEffect(() => {
        NombreUsuario()
        getNameDoctor()
    })


    React.useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
          const backAction = () => {
            Alert.alert("Hey", "Estas seguro que quieres salir?", [
              {
                text: "NO",
                onPress: () => null,
                style: "cancel"
              },
              { text: "SI", onPress: () => signOut() }
            ]);
            return true;
          };
    
          BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
          );
          return () =>
          BackHandler.removeEventListener('hardwareBackPress',backAction);
        });
    
        // Return the function to unsubscribe from the event so it gets removed on unmount
        return unsubscribe;
      }, [navigation]);
      


    const NombreUsuario = async () => {
        SetUsername(await AsyncStorage.getItem('UserName'))
    }
    const getNameDoctor = async () => {
        const name = await AsyncStorage.getItem('UserName')
        const sexo = await AsyncStorage.getItem('UserSexo')
        let SaludoSexo = ''
        if (sexo === 'M') {
            SaludoSexo = 'Dr.'
        }
        else {
            SaludoSexo = 'Dra.'
        }
        SetDoctorName(name)
        SetSSexo(SaludoSexo)
    }
   
    return (
        <SafeAreaView style={{ flex: 1, justifyContent: 'space-between' }}>
          
            <MenuGeneral cadenaTitulo={'Hola :) '+SSexo+" "+doctorName} navigation={navigation} color="medico"></MenuGeneral>
            <Image source={require('../../../../Asset/Images/Medico/Fondo/Trazado261.png')} style={stylesMedico.backgroundHeaderImage} />       
           
            <View style={stylesMedico.container}>
               <Text style={[stylesMedico.titleIni,{fontWeight:'bold'}]}>¿En que podemos ayudarte hoy?</Text>
                <Text style={stylesMedico.pIni}>En este espacio tendrás disponible la informacion necesaria para gestionarte a ti y a tus pacientes de una mejor manera.</Text>
                <Text style={[stylesMedico.pIni,{fontWeight:'bold'}]}>Estamos para ayudarte</Text>
                <TouchableOpacity onPress={() => { navigation.navigate('MedicoCalendario') }}>
                <View style={stylesMedico.card}>
                    <View style={stylesMedico.cardImage}>
                        <Image source={require('../../../../Asset/Images/Medico/Grupo_458.png')} style={stylesMedico.cardImageCircle}/>
                    </View>
                    <View style={stylesMedico.cardText}>
                        <Text style={stylesMedico.cardTitle}>Agenda de Turnos</Text>
                    </View>
                </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { navigation.navigate('CCMedico') }}>
                <View style={stylesMedico.card}>
                    <View style={stylesMedico.cardImage}>
                        <Image source={require('../../../../Asset/Images/Medico/Grupo_460.png')} style={stylesMedico.cardImageCircle}/>
                    </View>
                    <View style={stylesMedico.cardText}>
                        <Text style={stylesMedico.cardTitle}>Cuenta Corriente</Text>
                    </View>
                </View>
                </TouchableOpacity>
            </View>
        </SafeAreaView>

    )
}
export default MedicoInicio