import {
    Text,
    View,
    Image,
    AsyncStorage,
    BackHandler,
    TouchableHighlight,
    ScrollView,
} from "react-native";
import * as React from "react";
import Icons from "react-native-vector-icons/Ionicons";
import { stylesMedico } from "../../../../Asset/estilos/stylesMedico";
import { stylesPaciente } from "../../../../Asset/estilos/stylesPaciente";
import { SafeAreaView } from "react-native-safe-area-context";
import { Button } from "react-native-paper";
import { useStateIfMounted } from "use-state-if-mounted";
import * as Print from "expo-print";
// import * as MediaLibrary from "expo-media-library";
// import * as Sharing from "expo-sharing";
import moment from "moment";
import { MenuGeneral } from "../../Global/Menu";
// import RNViewShot from "react-native-view-shot";

export default function CuentaCorrienteDetalle(props) {
    const [getDatos, setDatos] = useStateIfMounted({
        id: 0,
        nombre: "",
        descripcion: "",
        fecha: "",
        dni: "",
        valor: 0,
    });
    const [getUri, setUri] = useStateIfMounted(null);
    React.useEffect(() => {
        BackHandler.addEventListener(
            "hardwareBackPress",
            handleBackButtonClick
        );
        return () => {
            BackHandler.removeEventListener(
                "hardwareBackPress",
                handleBackButtonClick
            );
        };
    });
    function handleBackButtonClick() {
        props.navigation.goBack();
        return true;
    }

    React.useEffect(() => {
        BackHandler.addEventListener(
            "hardwareBackPress",
            handleBackButtonClick
        );
        return () => {
            BackHandler.removeEventListener(
                "hardwareBackPress",
                handleBackButtonClick
            );
        };
    });
    function handleBackButtonClick() {
        props.navigation.goBack();
        return true;
    }
    //ESTE HOOK SE EJECUTA LUEGO DE INICIAR LA PANTALLA, Y OBTIENE LA DATA DEL TURNO
    React.useEffect(() => {
        getDataFromCCMedico();
    }, []);

    const getDataFromCCMedico = async () => {
        const datos = await AsyncStorage.getItem("CCMedicoData");
        setDatos(JSON.parse(datos));
        // console.log(JSON.parse(datos));
    };
    const htmlContent = `
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Comprobante de cuenta corriente ${getDatos.nombre}</title>
        <style>
            body {
                font-size: 16px;
                color: black;
            }
            h1 {
                text-align: center;
                font-family: verdana;
            }
            p{
                font-size: 160%;
            }
        </style>
    </head>
    <body>
        <h1>COMPROBANTE DE CUENTA CORRIENTE</h1>
        </br>
        <p> Numero de comprobante: ${getDatos.id}</p>
        <p>Obra Social: ${getDatos.os}</p>
        <p> Nombre y Apellido: ${getDatos.nombre}</p>
        <p>Tipo de práctica: ${getDatos.descripcion}</p>
        <p> Fecha: ${moment(getDatos.fecha).format("LL ,HH:MM")} Horas</p>
        <p> Documento: ${getDatos.dni}</p>
        <p>  Valor: $${getDatos.valor}</p>
    </body>
    </html>
`;

    const createPDF = async (html) => {
        try {
            const { uri } = await Print.printToFileAsync({ html });
            setUri(uri);
            await Print.printAsync({ uri: uri });
        } catch (err) {
            console.error(err);
        }
    };

    return (
        <SafeAreaView style={{ flex: 1, justifyContent: "space-between" }}>
            <MenuGeneral
                cadenaTitulo={"Detalle comprobante"}
                navigation={props.navigation}
                color="medico"
            ></MenuGeneral>
            <Image
                source={require("../../../../Asset/Images/Medico/Fondo/Trazado261.png")}
                style={stylesMedico.backgroundHeaderImage}
            />
            <View style={stylesMedico.container}>
                <View
                    style={{
                        alignSelf: "center",
                        borderRadius: 15,
                        backgroundColor: "#fff",
                        marginTop: "1%",
                        width: "95%",
                        height: "auto",
                        padding: 7,
                        elevation: 10,
                        shadowColor: "#000",
                        shadowOffset: { width: 0, height: 3 },
                        shadowOpacity: 0.5,
                        shadowRadius: 5,
                    }}
                >
                    <Text
                        style={{
                            fontSize: 28,
                            textAlign: "center",
                            padding: 5,
                            fontWeight: "normal",
                        }}
                    >
                        Comprobante Nro. {getDatos.id}
                    </Text>
                </View>
                <View
                    style={{
                        alignSelf: "center",
                        borderRadius: 15,
                        backgroundColor: "#fff",
                        marginTop: "2%",
                        width: "95%",
                        height: "auto",
                        elevation: 10,
                        shadowColor: "#000",
                        shadowOffset: { width: 0, height: 3 },
                        shadowOpacity: 0.5,
                        shadowRadius: 5,
                        paddingTop: 5,
                        paddingBottom: 5,
                    }}
                >
                    <Text
                        style={{
                            fontSize: 25,
                            textAlign: "center",
                            padding: 4,
                        }}
                    >
                        Obra Social: {getDatos.os}
                    </Text>
                    <Text
                        style={{
                            fontSize: 25,
                            textAlign: "center",
                            padding: 4,
                        }}
                    >
                        NyA: {getDatos.nombre}
                    </Text>
                    <Text
                        style={{
                            fontSize: 25,
                            textAlign: "center",
                            padding: 4,
                        }}
                    >
                        Tipo de práctica: {getDatos.descripcion}
                    </Text>
                    <Text
                        style={{
                            fontSize: 25,
                            textAlign: "center",
                            padding: 4,
                        }}
                    >
                        Fecha: {moment(getDatos.fecha).format("LL , HH:mm")} HS
                    </Text>
                    <Text
                        style={{
                            fontSize: 25,
                            textAlign: "center",
                            padding: 4,
                        }}
                    >
                        Documento: {getDatos.dni}
                    </Text>
                    {getDatos.valor !== 0 ? (
                        <Text
                            style={{
                                fontSize: 25,
                                textAlign: "center",
                                padding: 4,
                            }}
                        >
                            Valor: ${getDatos.valor}
                        </Text>
                    ) : (
                        <View>
                            <Text
                                style={{
                                    fontSize: 25,
                                    textAlign: "center",
                                    padding: 4,
                                }}
                            >
                                Valor: ${getDatos.valor}
                            </Text>
                            <Text
                                style={{
                                    color: "#FE3737",
                                    fontSize: 25,
                                    textAlign: "center",
                                    padding: 4,
                                }}
                            >
                                Autorización Pendiente
                            </Text>
                        </View>
                    )}
                </View>

                <View style={stylesMedico.cardNoMarginTop}>
                    <Button
                        color="#92B913"
                        mode="text"
                        onPress={() => createPDF(htmlContent)}
                    >
                        GENERAR COMPROBANTE
                    </Button>
                </View>
            </View>
        </SafeAreaView>
    );
}
