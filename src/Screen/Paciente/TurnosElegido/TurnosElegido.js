import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  BackHandler,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import "moment/locale/es";
import moment from "moment";
import * as Speech from "expo-speech";
import { SafeAreaView } from "react-native-safe-area-context";
import { AsyncStorage } from "react-native";
import { getTheme, setTheme } from "react-native-material-kit";
import { stylesPaciente } from "../../../../Asset/estilos/stylesPaciente";
import { useStateIfMounted } from "use-state-if-mounted";
import { captureScreen } from "react-native-view-shot";
import { SCLAlert, SCLAlertButton } from "react-native-scl-alert";
import { MenuGeneral } from "../../Global/Menu";

const theme = getTheme();

export default function TurnoElegido(props) {
  const [IdInstitucion, SetIdinstitucion] = useStateIfMounted(38);
  const [IdPrestador, SetIdPrestador] = useStateIfMounted("");
  const [Turno_Fin, SetTurno_Fin] = useStateIfMounted("");
  const [NombrePaciente, SetNombrePaciente] = useStateIfMounted("");
  const [datos, Setdatos] = useStateIfMounted({});
  const [NMedico, SetNMedico] = useStateIfMounted("");
  const [Esp, SetEsp] = useStateIfMounted("");
  const [CostoPractica, SetCostoPractica] = useStateIfMounted("");
  const [ShowM, SetShow] = useStateIfMounted(false);
  const [Tema, SetTema] = useStateIfMounted("default");
  const [Title, SetTitle] = useStateIfMounted("");
  const [getInitDOM, SetgetInitDOM] = useStateIfMounted(false);
  const [turnoServicio, SetTurnoServicio] = useStateIfMounted(false);
  const [loading, setLoading] = useStateIfMounted(true);
  function handleBackButton() {
    return false;
  }

  const takepicture = () => {
    captureScreen({
      format: "jpg",
      quality: 0.8,
    }).then(
      (uri) => props.navigation.navigate("PostTurnoElegido", { uriImage: uri }),
      (error) => console.error("Oops, snapshot failed", error)
    );
  };

  const DesbloquepTem = () => {
    const data = {
      idTurnosBloqueoTemporal: datos.IdTurnosBloqueoTemporal,
    };
    fetch(
      "https://app.excelenciadigital.ml/SanatorioDigitalApi/turnos/desbloqueotemporal",
      {
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(data),
      }
    )
      .then(function (response) {})
      .catch((error) => {
        console.error("salio por catch: " + error.message);
        this.setState({ errorMessage: error.message });
      });
  };

  const desbloqueotemporal = () => {
    Speech.stop();
    const data = {
      idTurnosBloqueoTemporal: datos.IdTurnosBloqueoTemporal,
    };
    fetch(
      "https://app.excelenciadigital.ml/SanatorioDigitalApi/turnos/desbloqueotemporal",
      {
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(data),
      }
    )
      .then(function (response) {
        //console.log("llego")
        if (response.status == 200) {
          props.navigation.goBack();
        }
      })
      .catch((error) => {
        console.error("salio por catch: " + error.message);
        this.setState({ errorMessage: error.message });
      });
  };
  const registrarTurno = async () => {
    Speech.stop();
    //REGISTRANDO SOLO PARA OSP
    const datosguardados = datos;

    datosguardados["IdObraSocial"] =
      parseInt(await AsyncStorage.getItem("IdObraSocial")) != 0 ? 1 : 0;
    Setdatos(datosguardados);
    fetch("https://app.excelenciadigital.ml/SanatorioDigitalApi/turnos/crear", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(datos),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data.Existe == 1) {
          SetTema("warning");
          SetTitle(data.Mensaje);
          DesbloquepTem();
          SetShow(true);
        } else {
          SetTema("success");
          SetTitle(data.Mensaje);
          takepicture();
        }
        if (turnoServicio) {
          console.log("es un turno con servicio");
          const crear_pedido = {
            idTurno: data.IdTurno,
            idEstudio: datos.idEstudio,
          };

          fetch(
            "https://app.excelenciadigital.ml/SanatorioDigitalApiTest/servicios/crear-pedido",
            {
              method: "POST",
              headers: { "Content-Type": "application/json" },
              body: JSON.stringify(crear_pedido),
            }
          )
            .then((response) => response.json())
            .then((data) => {
              console.log(data);
            })
            .catch((err) => {
              console.log(err);
            });
        }
      })
      .catch((reason) => {
        console.error(reason);
      });
  };

  const TraerAs = async () => {
    Setdatos(JSON.parse(await AsyncStorage.getItem("Idi")));
    const NomMed = await AsyncStorage.getItem("NM");
    const nu = await AsyncStorage.getItem("UserName");
    console.log(JSON.parse(await AsyncStorage.getItem("Idi")));
    SetNombrePaciente(nu);
    var nmnuevo = NomMed.split("-");
    var NM = nmnuevo[0].slice(1);
    if (nmnuevo[1]) {
      var especialidad = nmnuevo[1].slice(0, -1);
    } else {
      var especialidad = "";
      SetTurnoServicio(true);
    }
    // var especialidad = nmnuevo[1].slice(0, -1);
    SetNMedico(NM);
    SetEsp(especialidad);
    SetgetInitDOM(true);
  };

  const Hablar = () => {
    setLoading(false);
    var thingToSay =
      "Estas por seleccionar el dia" +
      moment(datos.Turno_Inicio).format("dddd  D MMMM") +
      "a las" +
      moment(datos.Turno_Inicio).format("HH:mm") +
      "Horas";
    Speech.speak(thingToSay, {
      pitch: 0.9,
      rate: 1.2,
      language: "es-MX",
    });
    // setLoading(false);
  };

  const CalcularCostoPractica = async () => {
    var idOs = parseInt(await AsyncStorage.getItem("IdObraSocial"));
    var costoDePractica = await AsyncStorage.getItem("valorconsulta");

    if (idOs == -1) {
      SetCostoPractica(costoDePractica);
    } else {
      SetCostoPractica("");
    }
    setLoading(false);
  };

  React.useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButton);
    TraerAs();
  }, []);
  React.useEffect(() => {
    if (getInitDOM === true) {
      CalcularCostoPractica()
        .then(() => Hablar())
        .catch((err) => console.log(err));
    }
  }, [getInitDOM]);

  const handleClose = () => {
    SetShow(false);
    props.navigation.navigate("Turnos");
  };

  moment.locale("es");
  return (
    <SafeAreaView style={{ flex: 1, justifyContent: "space-between" }}>
      <Image
        source={require("../../../../Asset/Images/Paciente/DashPaciente/Trazado260.png")}
        style={stylesPaciente.backgroundImage}
      />
      <MenuGeneral
        cadenaTitulo={"Solicitar Turno"}
        navigation={props.navigation}
        color="paciente"
      ></MenuGeneral>
      <ScrollView>
        <View style={stylesPaciente.container}>
          <View>
            <View>
              <Text
                style={{
                  fontSize: 30,
                  fontWeight: "bold",
                  textAlign: "center",
                }}
              >
                {NombrePaciente}{" "}
              </Text>
              <Text style={{ fontSize: 20, textAlign: "center" }}>
                Aquí podrás revisar lo que seleccionaste{" "}
              </Text>
            </View>
            {loading == false ? (
              <View>
                <View style={{ marginTop: 15 }}>
                  <View
                    style={{
                      marginTop: 30,
                      marginLeft: "5%",
                      width: "90%",
                      backgroundColor: "white",
                      borderRadius: 15,
                      padding: 40,
                      elevation: 10,
                      shadowColor: "#000",
                      shadowOffset: { width: 0, height: 3 },
                      shadowOpacity: 0.5,
                      shadowRadius: 5,
                      //marginHorizontal: 10,
                    }}
                  >
                    <Text
                      style={{
                        color: "#7E7D7D",
                        marginBottom: 5,
                        fontSize: 25,
                        paddingTop: 10,
                      }}
                    >
                      Te atenderá
                    </Text>
                    <Text
                      style={{
                        color: "black",
                        fontSize: 28,
                        paddingTop: 10,
                        fontWeight: "bold",
                      }}
                    >
                      {turnoServicio ? "Servicio de" : "Dr. "} {NMedico}
                    </Text>
                    <Text
                      style={{ color: "black", fontSize: 20, paddingTop: 10 }}
                    >
                      {Esp}
                    </Text>
                    <Text
                      style={{
                        color: "#7E7D7D",
                        marginBottom: 5,
                        fontSize: 25,
                        paddingTop: 10,
                      }}
                    >
                      Asistirás el día
                    </Text>
                    <Text
                      style={{
                        color: "black",
                        fontSize: 28,
                        paddingTop: 10,
                        fontWeight: "bold",
                      }}
                    >
                      {moment(datos.Turno_Inicio).format("dddd  D MMMM")} a las{" "}
                      {moment(datos.Turno_Inicio).format("HH:mm")} hs.
                    </Text>

                    {CostoPractica != "" ? (
                      <View>
                        <Text
                          style={{
                            color: "#7E7D7D",
                            marginBottom: 5,
                            fontSize: 25,
                            paddingTop: 10,
                          }}
                        >
                          {turnoServicio
                            ? "Valor de la práctica"
                            : "Valor de la consulta particular"}
                        </Text>
                        <Text
                          style={{
                            color: "black",
                            fontSize: 28,
                            paddingTop: 10,
                            fontWeight: "bold",
                          }}
                        >
                          $ {CostoPractica}
                        </Text>
                      </View>
                    ) : null}
                  </View>
                </View>
                <View>
                  <View
                    style={{
                      marginTop: 25,

                      marginLeft: "5%",
                      width: "90%",
                      backgroundColor: "white",
                      borderRadius: 15,
                      padding: 10,
                      elevation: 10,
                      shadowColor: "#000",
                      shadowOffset: { width: 0, height: 3 },
                      shadowOpacity: 0.5,
                      shadowRadius: 5,

                      marginBottom: 20,
                    }}
                  >
                    <View
                      style={{
                        textAlign: "center",
                        marginTop: 10,
                        padding: 80,
                        paddingVertical: 8,
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 25,
                          fontWeight: "bold",
                          textAlign: "center",
                          color: "#A0A0A0",
                        }}
                      >
                        ¿Te Esperamos?{" "}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-around",
                        margin: 10,
                      }}
                    >
                      <View style={{ width: "40%" }}>
                        <TouchableOpacity
                          style={{
                            width: "100%",
                            marginTop: "4%",
                            alignSelf: "center",
                            backgroundColor: "#FF565B",
                            // sombra
                            elevation: 6,
                            borderRadius: 10,
                            paddingVertical: 10,
                            paddingHorizontal: 12,
                          }}
                          onPress={() => desbloqueotemporal()}
                        >
                          <Text
                            style={{
                              fontSize: 25,
                              color: "white",
                              textAlign: "center",
                            }}
                          >
                            NO
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <View style={{ width: "40%", fontSize: 40 }}>
                        <TouchableOpacity
                          style={{
                            width: "100%",
                            marginTop: "4%",
                            alignSelf: "center",
                            backgroundColor: "#42F259",
                            // sombra
                            elevation: 6,
                            borderRadius: 10,
                            paddingVertical: 10,
                            paddingHorizontal: 12,
                          }}
                          onPress={() => registrarTurno()}
                        >
                          <Text
                            style={{
                              fontSize: 25,
                              color: "white",
                              textAlign: "center",
                            }}
                          >
                            SI
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                    <SCLAlert
                      theme={Tema}
                      show={ShowM}
                      title={Title}
                      subtitle=" "
                    >
                      <SCLAlertButton
                        theme={Tema}
                        onPress={() => {
                          handleClose();
                        }}
                      >
                        Aceptar
                      </SCLAlertButton>
                    </SCLAlert>
                  </View>
                </View>
              </View>
            ) : (
              <View
                style={[
                  stylesPaciente.containerLoad,
                  stylesPaciente.horizontalLoad,
                ]}
              >
                <View style={{ marginTop: "60%" }}>
                  <ActivityIndicator size="large" color="green" />
                </View>
              </View>
            )}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
