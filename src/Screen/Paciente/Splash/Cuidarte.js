import { ImageBackground, Text, View, StyleSheet, Image } from 'react-native'
import React, { Component } from 'react';
import { styles } from '../../../../Asset/estilos/Styles'
import { stylesPaciente } from '../../../../Asset/estilos/stylesPaciente'
import { AsyncStorage } from 'react-native';
import { useStateIfMounted } from "use-state-if-mounted";
import { SafeAreaView } from 'react-native-safe-area-context'

const ImgBg = () => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.stretch}
        source={require('../../../../Asset/Images/Sinlog/Privacidad/doctores.png')}
      />
    </View>
  );
}

const verCuidarte = ({ navigation }) => {

  const [UserName, SetUsername] = useStateIfMounted('')
  const [Sexo, SetSexo] = useStateIfMounted('')
  React.useEffect(() => {
    NombreUsuario()
    setTimeout(() => {
      navigation.navigate("InicioPaciente")
    }, 3000);
  }, [])

  const NombreUsuario = async () => {
    SetUsername(await AsyncStorage.getItem('UserName'))
    SetSexo(await AsyncStorage.getItem('UserSexo'))
  }

  return (

    <SafeAreaView
    style={{ flex: 1, justifyContent: 'space-between' }}
  >
    <Image source={require('../../../../Asset/Images/Medico/Cuidarte/bienvenida.png')} style={stylesPaciente.backgroundImageSplash} />
    <View style={stylesPaciente.containerSplash}>
      {Sexo == 'F' ? <Text style={stylesPaciente.Title}> Bienvenida {UserName} ! </Text> :
        <Text style={stylesPaciente.Title}>Bienvenido {UserName} ! </Text>}
        <Text style={stylesPaciente.subTitle}>Gracias por elegirnos nuevamente</Text>
    </View>
  </SafeAreaView>

  )
}

export default verCuidarte