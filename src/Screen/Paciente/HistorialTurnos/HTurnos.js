import * as React from "react";
import {
  View,
  Text,
  Image,
  AsyncStorage,
  FlatList,
  BackHandler,
  ActivityIndicator,
} from "react-native";
import { useStateIfMounted } from "use-state-if-mounted";
import { SafeAreaView } from "react-native-safe-area-context";
import { stylesPaciente } from "../../../../Asset/estilos/stylesPaciente";
import Icons from "react-native-vector-icons/Ionicons";
import { Button } from "react-native-elements";
import { isNull } from "lodash";
import { MenuGeneral } from "../../Global/Menu";
var moment = require("moment");

export default class HTurnos extends React.Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      Nombre: "",
      IdPaciente: "",
      Data: [],
      loading: true,
    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount() {
    this.getNombre();
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  // componentWillMount() {
  //     this._isMounted = false;
  //     BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  //   }

  componentWillUnmount() {
    this._isMounted = false;
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  handleBackButtonClick() {
    this.props.navigation.goBack(null);
    return true;
  }

  componentDidUpdate(prevProps, prevState) {
    this._isMounted = true;
    if (this._isMounted) {
      if (prevState.IdPaciente !== this.state.IdPaciente) {
        // this.fetchData(this.state.userID);
        this.getTurnos();
        // this.getInfoMedico(this.state.hoy.getMonth() + 1, this.state.hoy.getFullYear())
        // console.log("Cambio id prestador",this.state.IdPrestador)
      }
    }
  }

  async getNombre() {
    this.state.IdPaciente = this.setState({
      IdPaciente: await AsyncStorage.getItem("IdPacIns"),
    });
    this.setState({ Nombre: await AsyncStorage.getItem("UserName") });
  }

  getTurnos() {
    try {
      console.log(this.state.IdPaciente);
      fetch(
        "https://app.excelenciadigital.ml/SanatorioDigitalApiTest/turnos/porpaciente?idInstitucion=38&IdPacientes_Instituciones=" +
          this.state.IdPaciente +
          "&pagina=1&cantidadPorPagina=20"
      )
        .then((response) => response.json())
        .then((datos) => {
          // console.log(datos)
          let arr = [];
          datos.detalle.map((t) => {
            if (moment(t.fecha).format() >= moment().format()) {
              arr.push({
                key: `${t.idTurno}`,
                idEstadoT: `${t.idEstadoTurno}`,
                IdTurno: `${t.idTurno}`,
                Especialidad: `${t.especialidad}`,
                Fecha: `${moment(t.fecha).format("LL")}`,
                Hora: `${moment(t.horaInicio, ["h:mm A"]).format("HH:mm")}`,
                NyA: `${t.nombrePrestador} ${t.apellidoPrestador}`,
                formatedDate: `${moment(t.fecha).format("YYYY-MM-DD")}`,
              });
            }
          });
          this.setState({ Data: arr });
          this.setState({ loading: false });
        });
    } catch (error) {
      console.log(error);
    }
  }

  _actionEnCamino = (idTurno) => {
    const datos = {
      Identificador: 30, //VOY EN CAMINO
      IdTurno: idTurno,
      Comentario: "Voy a camino, confirmación a través de app",
    };
    console.log(JSON.stringify(datos));
    fetch(
      "https://app.excelenciadigital.ml/SanatorioDigitalApiTest/estadosusuarios/crear",
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(datos),
      }
    )
      .then((response) =>
        response.status === 200 ? response.json() : response.text()
      )
      .then((response) => {
        console.log(response);
      })
      .catch((err) => {
        console.log("Tuvimos problemas. Motivo => ", err);
      });
  };
  _actionCancelarCita = (idTurno) => {};

  render() {
    return (
      <SafeAreaView style={{ flex: 1, justifyContent: "space-between" }}>
        <View style={{ height: "100%" }}>
          <Image
            source={require("../../../../Asset/Images/Paciente/DashPaciente/Trazado260.png")}
            style={stylesPaciente.backgroundImage}
          />
          <MenuGeneral
            cadenaTitulo={"Próximos Turnos"}
            navigation={this.props.navigation}
            color="paciente"
          ></MenuGeneral>
          <View>
            <Text
              style={{ fontWeight: "bold", fontSize: 30, textAlign: "center" }}
            >
              {this.state.Nombre}
            </Text>
            <Text style={{ fontSize: 20, textAlign: "center" }}>
              Aqui tienes tu historial de turnos
            </Text>
          </View>

          {this.state.loading == false ? (
            <FlatList
              data={this.state.Data}
              keyExtractor={(item) => item.key}
              renderItem={(item) => {
                return (
                  <View style={stylesPaciente.ContainerListado}>
                    <View>
                      <Text
                        style={{
                          fontSize: 20,
                          color: "#817777",
                          marginLeft: 10,
                        }}
                      >
                        Te atenderá
                      </Text>
                    </View>
                    <View>
                      <Text
                        style={{ fontSize: 25, color: "black", marginLeft: 10 }}
                      >
                        Dr. {item.item.NyA}
                      </Text>
                      <Text
                        style={{ fontSize: 20, color: "black", marginLeft: 10 }}
                      >
                        {item.item.Especialidad}
                      </Text>
                    </View>
                    <View style={{ marginTop: 15, marginBottom: 8 }}>
                      <Text
                        style={{
                          fontSize: 20,
                          color: "#817777",
                          marginLeft: 10,
                        }}
                      >
                        Asistiras el día
                      </Text>
                    </View>
                    <View>
                      <Text
                        style={{
                          fontSize: 18,
                          color: "black",
                          textAlign: "left",
                          marginLeft: 10,
                        }}
                      >
                        {item.item.Fecha} a las {item.item.Hora} Hs.
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-around",
                        marginTop: 15,
                      }}
                    >
                      {item.item.formatedDate ==
                      moment().format("YYYY-MM-DD") ? (
                        <View
                          style={{
                            width: "40%",
                            marginTop: 2,
                            paddingVertical: 0,
                            borderWidth: 2,
                            borderColor: "#42F259",
                            borderRadius: 6,
                          }}
                        >
                          <Button
                            type="outline"
                            title="Voy en camino"
                            onPress={
                              () =>
                                this._actionEnCamino(
                                  item.item.key
                                ) /*SEND item.item.key*/
                            }
                          ></Button>
                        </View>
                      ) : null}

                      <View
                        style={{
                          width: "40%",
                          marginTop: 2,
                          paddingVertical: 0,
                          borderWidth: 2,
                          borderColor: "#EC0000",
                          borderRadius: 6,
                        }}
                      >
                        <Button
                          type="outline"
                          title="Cancelar"
                          onPress={
                            () =>
                              console.log(
                                "pressed cancel date"
                              ) /*SEND item.item.key*/
                          }
                        ></Button>
                      </View>
                    </View>
                  </View>
                );
              }}
            />
          ) : (
            <View
              style={[
                stylesPaciente.containerLoad,
                stylesPaciente.horizontalLoad,
              ]}
            >
              <ActivityIndicator size="large" color="green" />
            </View>
          )}
        </View>
      </SafeAreaView>
    );
  }
}
