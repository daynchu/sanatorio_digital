import * as React from "react";
import {
  View,
  Text,
  Platform,
  StyleSheet,
  Button,
  Modal,
  TouchableHighlight,
  FlatList,
  BackHandler,
  Image,
} from "react-native";
import {
  Calendar,
  CalendarList,
  Agenda,
  LocaleConfig,
} from "react-native-calendars";
import * as firebase from "firebase";
import { useStateIfMounted } from "use-state-if-mounted";
import { CreateAlert } from "../../Global/Alert";
import moment from "moment";
import "moment/locale/es";
import { SafeAreaView } from "react-native-safe-area-context";
import { AsyncStorage } from "react-native";
import { stylesPaciente } from "../../../../Asset/estilos/stylesPaciente";
import { MenuGeneral } from "../../Global/Menu";
import { FULLSCREEN_UPDATE_PLAYER_DID_DISMISS } from "expo-av/build/Video";

LocaleConfig.locales["es"] = {
  monthNames: [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
  ],
  monthNamesShort: [
    "Ene.",
    "Feb.",
    "Mar",
    "Abr",
    "May",
    "Jun",
    "Jul.",
    "Ago",
    "Sept.",
    "Oct.",
    "Nov.",
    "Dic.",
  ],
  dayNames: [
    "Domingo",
    "Lunes",
    "Martes",
    "Miercoles",
    "Jueves",
    "Viernes",
    "Sabado",
  ],
  dayNamesShort: ["Dom.", "Lun.", "Mar.", "Mier.", "Jue.", "Vier.", "Sab."],
  today: "Hoy",
};

LocaleConfig.defaultLocale = "es";
const iOS = Platform.OS === "ios";

export default function Turnos(props) {
  const [idInstitucion, SetIdinstitucion] = useStateIfMounted(38);
  const [
    IdPacientes_Instituciones,
    SetIdPacientes_Instituciones,
  ] = useStateIfMounted("");
  const [horasInicio, SethorasInicio] = useStateIfMounted([]);
  const [practicasDisponibles, setPracticasDisponibles] = useStateIfMounted([]);
  const [horasFin, SethorasFin] = useStateIfMounted([]);
  const [horaSeleccionada, SethoraSeleccionada] = useStateIfMounted("");
  const [frec, Setfrec] = useStateIfMounted(30);
  const [selected, Setselected] = useStateIfMounted("");
  const [fecha, Setfecha] = useStateIfMounted("");
  const [modalVisible, SetmodalVisible] = useStateIfMounted(false);
  const [modalVisible_practica, SetmodalVisible_practica] = useStateIfMounted(
    false
  );

  const [NMedico, SetNMedico] = useStateIfMounted("");
  const [IdPrestador, SetIdPrestador] = useStateIfMounted("");
  const [NPaciente, SetNPaciente] = useStateIfMounted("");
  const [fechasActivadas, SetfechasActivadas] = useStateIfMounted([]);
  const [
    fechasActivadasSinTurno,
    SetfechasActivadasSinTurno,
  ] = useStateIfMounted([]);
  const [nomEsp, setEsp] = useStateIfMounted("");
  const [getInitDOM, setDom] = useStateIfMounted(false);
  const [tipoTurno, setTipoTurno] = useStateIfMounted();
  const [HoraPicked, SetHoraPicked] = useStateIfMounted();

  React.useEffect(() => {
    NombreMed();
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        "hardwareBackPress",
        handleBackButtonClick
      );
    };
  }, [handleBackButtonClick]);
  React.useEffect(() => {
    const hoy = new Date();
    if (getInitDOM === true) {
      getInfoMedico(hoy.getMonth() + 1, hoy.getFullYear());
    }
  }, [getInitDOM]);

  function handleBackButtonClick() {
    props.navigation.goBack();
    return true;
  }

  const registrarTurno = async (val) => {
    if (tipoTurno == 1 /*CONSULTA MEDICA*/) {
      var fecha = new Date();
      var TurnInit = moment(val).format("YYYY-MM-DD HH:mm:ss" + ".000");
      var TurnFinal = moment(val)
        .add(frec, "m")
        .format("YYYY-MM-DD HH:mm:ss" + ".000");
      fetch(
        "https://app.excelenciadigital.ml/SanatorioDigitalApi/turnos/validaexiste?IdInstitucion=" +
          idInstitucion +
          "&IdPrestador=" +
          IdPrestador +
          "&Turno_Inicio=" +
          TurnInit +
          "&Turno_Fin=" +
          TurnFinal
      )
        .then((response) => response.json())
        .then((data) => {
          if (data.Existe == 0) {
            SetmodalVisible(false);

            var datos = {
              IdInstitucion: idInstitucion,
              IdPrestador: IdPrestador,
              IdPacientes_Instituciones: IdPacientes_Instituciones,
              Turno_Inicio: TurnInit,
              Turno_Fin: TurnFinal,
              IdTurnosBloqueoTemporal: data.IdTurnosBloqueoTemporal,
            };
            // console.log(datos)
            AsyncStorage.setItem("Idi", JSON.stringify(datos));
            props.navigation.navigate("TurnosElegidos");
          } else {
            //console.log("existe")
            CreateAlert(
              "Ups",
              "Este turno ya ha sido registrado por otra persona, por favor, elige otro turno."
            );
          }
        })
        .catch((error) => {
          if (error.message == 'Unexpected identifier "Solicitud"')
            CreateAlert(
              "Ups",
              "No se puede establecer contácto con el servidor, intentelo de nuevo más"
            );
        });
    }
    if (tipoTurno == 2) {
      /* ESTUDIO MEDICO */
      var IdServicio = await AsyncStorage.getItem("IdServicio");
      getPracticasDisponibles(IdServicio);
      SetHoraPicked(val);
      SetmodalVisible(false);
      SetmodalVisible_practica(true);
    }
  };

  const generaTurnoServicio = (IdPractica) => {
    console.log(IdPractica);
    const val = HoraPicked;
    var fecha = new Date();
    var TurnInit = moment(val).format("YYYY-MM-DD HH:mm:ss" + ".000");
    var TurnFinal = moment(val)
      .add(frec, "m")
      .format("YYYY-MM-DD HH:mm:ss" + ".000");
    fetch(
      "https://app.excelenciadigital.ml/SanatorioDigitalApi/turnos/validaexiste?IdInstitucion=" +
        idInstitucion +
        "&IdPrestador=" +
        IdPrestador +
        "&Turno_Inicio=" +
        TurnInit +
        "&Turno_Fin=" +
        TurnFinal
    )
      .then((response) => response.json())
      .then((data) => {
        if (data.Existe == 0) {
          SetmodalVisible(false);
          SetmodalVisible_practica(false);
          var datos = {
            idEstudio: IdPractica,
            IdInstitucion: idInstitucion,
            IdPrestador: IdPrestador,
            IdPacientes_Instituciones: IdPacientes_Instituciones,
            Turno_Inicio: TurnInit,
            Turno_Fin: TurnFinal,
            IdTurnosBloqueoTemporal: data.IdTurnosBloqueoTemporal,
          };
          // console.log(datos)
          AsyncStorage.setItem("Idi", JSON.stringify(datos));
          props.navigation.navigate("TurnosElegidos");
        } else {
          //console.log("existe")
          CreateAlert(
            "Ups",
            "Este turno ya ha sido registrado por otra persona, por favor, elige otro turno."
          );
        }
      })
      .catch((error) => {
        if (error.message == 'Unexpected identifier "Solicitud"')
          CreateAlert(
            "Ups",
            "No se puede establecer contácto con el servidor, intentelo de nuevo más"
          );
      });
  };

  function getInfoMedico(mes, anio) {
    //console.log("idInstitucion="+idInstitucion+" IdPrestador="+IdPrestador+" Mes="+mes+" Anio="+anio)
    fetch(
      "https://app.excelenciadigital.ml/SanatorioDigitalApi/turnos/turnosdisponiblespormes?idInstitucion=" +
        idInstitucion +
        "&IdPrestador=" +
        IdPrestador +
        "&Mes=" +
        mes +
        "&Anio=" +
        anio
    )
      .then((response) => response.json())
      .then((data) => {
        let fechasActuales = fechasActivadas;
        let fechasSinTurno = fechasActivadasSinTurno;
        data.forEach((fecha) => {
          const fechaFormateada = fecha.Fecha.substr(0, 10);
          if (fecha.TurnosDisponibles > 1) fechasActuales.push(fechaFormateada);
          else fechasSinTurno.push(fechaFormateada);
        });
        SetfechasActivadas(fechasActuales);
        SetfechasActivadasSinTurno(fechasSinTurno);
      });
  }

  function getPracticasDisponibles(id) {
    fetch(
      "https://app.excelenciadigital.ml/SanatorioDigitalApiTest/servicios/practicas?idInstitucion=" +
        idInstitucion +
        "&idServicio=" +
        id
    )
      .then((response) => response.json())
      .then((data) => {
        buildPracticasSelect(data);
      });
  }

  function getHorasDisponibles(fecha) {
    fetch(
      "https://app.excelenciadigital.ml/SanatorioDigitalApi/turnos/turnosdisponibles?idInstitucion=" +
        idInstitucion +
        "&IdPrestador=" +
        IdPrestador +
        "&Fecha=" +
        fecha
    )
      .then((response) => response.json())
      .then((data) => {
        //console.log(data)
        buildHorasSelect(data);
      });
  }

  function RestarHoras(H) {
    var inicioMinutos = parseInt(H.HoraInicio.substr(3, 2));
    var inicioHoras = parseInt(H.HoraInicio.substr(0, 2));
    var finMinutos = parseInt(H.HoraFin.substr(3, 2));
    var finHoras = parseInt(H.HoraFin.substr(0, 2));
    var transcurridoMinutos = finMinutos - inicioMinutos;
    // var transcurridoHoras = finHoras - inicioHoras;
    Setfrec(transcurridoMinutos);
    //console.log("Frecuencia", transcurridoMinutos)
  }

  //cuidadooooo con los let
  function buildHorasSelect(horas) {
    let horasInicio = [];
    let horasFin = [];
    // //console.log(moment(horas).format("HH:mm"))
    RestarHoras(horas[0]);
    horas.forEach(function (hora) {
      var d = new Date();
      var n = d.getHours();
      let x = hora.HoraInicio;
      let y = hora.HoraFin;
      var arr = x.split(":");
      var arr2 = arr.splice(0, 2);
      let xModified = arr2.join(":");

      horasInicio.push({
        label: xModified,
        value: hora.Fecha + "T" + xModified,
      });
      horasFin.push({
        label: y,
        value: hora.Fecha + "T" + y,
      });
    });
    SethorasInicio(horasInicio);
    SethorasFin(horasFin);
  }
  function buildPracticasSelect(practicas) {
    // let expReg = RegExp(/\d/);
    let practicas_arr = [];
    practicas[0].map((data) => {
      practicas_arr.push({
        label: data.Nombre,
        value: data.IdEstudio.toString(),
      });
    });
    // //console.log(moment(horas).format("HH:mm"))
    // console.log(practicas);
    setPracticasDisponibles(practicas_arr);
  }

  const renderItem = ({ item }) => (
    <View style={ModalStile.listView}>
      <Text
        style={ModalStile.listItem}
        onPress={() => registrarTurno(item.value)}
      >
        {item.label}
      </Text>
    </View>
  );

  const renderItem_practicas = ({ item }) => (
    <View style={ModalStile.listView}>
      <Text
        style={ModalStile.listItem}
        onPress={() => generaTurnoServicio(item.value)}
      >
        {item.label}
      </Text>
    </View>
  );

  const NombreMed = async () => {
    try {
      const nu = await AsyncStorage.getItem("UserName");
      const idprestad = await AsyncStorage.getItem("Id");
      const NomMed = await AsyncStorage.getItem("NM");
      const tipoTurno = await AsyncStorage.getItem("tipo");
      if (nu !== null && idprestad !== null && NomMed !== null) {
        var nmnuevo = NomMed.split("-");
        var NM = nmnuevo[0].slice(1);
        if (nmnuevo[1]) {
          var Esp = nmnuevo[1].slice(0, -1);
        } else {
          var Esp = nmnuevo[0];
        }
        SetIdPacientes_Instituciones(await AsyncStorage.getItem("IdPacIns"));
        SetNMedico(NM);
        setEsp(Esp);
        setTipoTurno(tipoTurno);
        SetNPaciente(nu.split(" "));
        SetIdPrestador(idprestad);
        setDom(true);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const CalendarioValidado = {
    [selected]: {
      selected: true,
      selectedColor: "green",
      selectedTextColor: "white",
    },
  };
  fechasActivadas.forEach((fecha) => {
    //console.log(fecha)
    CalendarioValidado[fecha] = { disabled: false, disableTouchEvent: false };
  });
  fechasActivadasSinTurno.forEach((fecha) => {
    //console.log(fecha)
    CalendarioValidado[fecha] = {
      disabled: false,
      selected: true,
      selectedColor: "red",
      disableTouchEvent: true,
    };
  });
  //CalendarioValidado['2020-08-14'] = {disabled: false, disableTouchEvent: false}

  moment.locale("es");

  var dia = new Date();

  return (
    <SafeAreaView>
      <Image
        source={require("../../../../Asset/Images/Paciente/DashPaciente/Trazado260.png")}
        style={stylesPaciente.backgroundImage}
      />
      <MenuGeneral
        cadenaTitulo={"Solicitar Turno"}
        navigation={props.navigation}
        color="paciente"
      ></MenuGeneral>
      <View>
        <View style={stylesPaciente.container}>
          <View style={{ marginTop: "5%" }}>
            <Text style={stylesPaciente.medico}>{NMedico}</Text>
            <Text style={stylesPaciente.medicoEspecialidad}>{nomEsp}</Text>
          </View>
          <View style={stylesPaciente.conteinerText}>
            <Text style={stylesPaciente.text}>{NPaciente[0]},</Text>
            <Text style={stylesPaciente.text}>
              ¿Que día quieres venir a realizar tu consulta medica?
            </Text>
          </View>
          <View style={stylesPaciente.calendar}>
            <Calendar
              minDate={dia}
              displayLoadingIndicator
              theme={{
                selectedDayBackgroundColor: "blue",
                backgroundColor: "#3FA9F5",
              }}
              onDayPress={(day) => {
                Setselected(day.dateString);
                Setfecha(day.dateString);
                SetmodalVisible(true);
                getHorasDisponibles(day.dateString);
              }}
              disableAllTouchEventsForDisabledDays={true}
              disabledByDefault={true}
              markedDates={CalendarioValidado}
              onMonthChange={(fecha) => {
                getInfoMedico(fecha.month, fecha.year);
              }}
            />
          </View>

          <View style={stylesPaciente.containerOutBox}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                SetmodalVisible(false);
              }}
            >
              <View style={ModalStile.centeredView}>
                <View style={ModalStile.modalView}>
                  <Text style={ModalStile.listText}>Selecciona la hora</Text>
                  <FlatList
                    data={horasInicio}
                    renderItem={renderItem}
                    keyExtractor={(item) => item.value}
                    onPress={(item) => {
                      SethoraSeleccionada(item.value);
                    }}
                  />
                  <TouchableHighlight
                    style={{
                      ...ModalStile.openButton,
                      backgroundColor: "#3F70F5",
                    }}
                    onPress={() => {
                      SetmodalVisible(false);
                    }}
                  >
                    <Text style={ModalStile.textStyle}>Volver</Text>
                  </TouchableHighlight>
                </View>
              </View>
            </Modal>
            {/* MODAL DE SELECCIÓN DE PRACTICA  */}
            <Modal
              animationType="slide"
              transparent={false}
              visible={modalVisible_practica}
              onRequestClose={() => {
                SetmodalVisible_practica(false);
              }}
            >
              <View style={ModalStile.centeredView}>
                <View style={ModalStile.modalView}>
                  <Text style={ModalStile.listText}>¡Una cosa más!</Text>
                  <Text style={ModalStile.listText}>
                    Selecciona la práctica
                  </Text>

                  <FlatList
                    data={practicasDisponibles}
                    renderItem={renderItem_practicas}
                    keyExtractor={(item) => item.value}
                    onPress={(item) => {
                      SethoraSeleccionada(item.value);
                    }}
                  />
                  <TouchableHighlight
                    style={{
                      ...ModalStile.openButton,
                      backgroundColor: "#3F70F5",
                    }}
                    onPress={() => {
                      SetmodalVisible_practica(false);
                    }}
                  >
                    <Text style={ModalStile.textStyle}>Volver</Text>
                  </TouchableHighlight>
                </View>
              </View>
            </Modal>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const ModalStile = StyleSheet.create({
  listView: {
    marginBottom: 15,
    backgroundColor: "#73C0F7",
    borderRadius: 10,
    padding: 12,
    elevation: 2,
  },
  listItem: {
    fontSize: 24,
    fontWeight: "bold",
    color: "white",
  },
  listText: {
    fontSize: 26,
    color: "black",
    marginBottom: 25,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    marginTop: 15,
    backgroundColor: "#3F4BF5",
    borderRadius: 10,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 20,
    marginLeft: "25%",
    marginRight: "25%",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
