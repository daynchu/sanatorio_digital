import * as React from "react";
import { View, StyleSheet, AsyncStorage } from "react-native";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import {
  Text,
  Avatar,
  Title,
  Caption,
  Paragraph,
  Drawer,
  TouchableRipple,
  Switch,
  useTheme,
} from "react-native-paper";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { AuthContext } from "../../../../Component/context";
import { useStateIfMounted } from "use-state-if-mounted";
import { VersionActual } from "../../Global/Versionado";

export function Drawcontent(props) {
  const [NomUsu, SetNomUsu] = useStateIfMounted("");
  const [Sex, SetSex] = useStateIfMounted("");

  const GetNombre = async () => {
    SetNomUsu(await AsyncStorage.getItem("UserName"));
    SetSex(await AsyncStorage.getItem("UserSexo"));
  };

  React.useEffect(() => {
    GetNombre();
  }, []);
  // const paperTheme = useTheme();
  const { signOut } = React.useContext(AuthContext);
  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <View style={styles.userInfoSection}>
            {Sex == "F" ? (
              <View style={{ flexDirection: "row", marginTop: 15 }}>
                <Avatar.Image
                  source={require("../../../../Asset/Images/Paciente/Drower/AssetMujer.png")}
                  size={50}
                />
                <View style={{ marginLeft: 15, flexDirection: "column" }}>
                  <Title style={styles.title}>{NomUsu}</Title>
                  {/* <Caption style={styles.caption}>@j_doe</Caption> */}
                </View>
              </View>
            ) : (
              <View style={{ flexDirection: "row", marginTop: 15 }}>
                <Avatar.Image
                  source={require("../../../../Asset/Images/Paciente/Drower/AssetHombre.png")}
                  size={50}
                />
                <View style={{ marginLeft: 15, flexDirection: "column" }}>
                  <Title style={styles.title}>{NomUsu}</Title>
                  {/* <Caption style={styles.caption}>@j_doe</Caption> */}
                </View>
              </View>
            )}

            {/* <View style={{flexDirection:'row',marginTop: 15}}>
                          <Avatar.Image 
                                source={{
                                    uri: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABSlBMVEX///9Vlp2iQyMogIXt2bTcxaG8n4Llz6uwyswSen80IhRRmaHeyaVIkJejQiGfOhikQB2lPhi/iWnx37rIlHSdNRDp8fIAdXtYmJ+lPRedMwCmPBPZvpqdNA6xel2MtrvTso+lSSni7e60b1DPqof27OnAhGRqg4KvZEW3d1hmh4jG2tzLoX9xfnzFlnUuGw3r2tU7iY56q67hx8DIl4rQppqaKQCoUTLQ4eNro6lejpKcSi+EaV6iw8eHs7h0qK57cmqKYlOUUzx9cGgeAADZj3XFqouDal/ZurPBh3W2cV6xZU/EjoCsWT+7emfo082vmJCRWUaUVT+QPSBoMRs6LyGRf2ZiUj8/IhAuIROAbVbRvp0UAAByYUyejXMmDwBRPy7Te2XOVkfco4bWb128JiLUIBnXNyzXSTzou5vd28zXzLLf3ck9d3hgOZZQAAASdElEQVR4nO2d+1fbRhbHkbGQje2RQJFtjF1sXoGYJiEEDKTkHdI0odl0t8m2NE1f22xJ9v//dUfv152ZO7KMTY+/p6dtYiP00Z25986dh2ZmpppqqqmmmmqqqaaS0sbe1/t3X7x48Wr/672Ncd9M3trYu/vy4KDV6izpVFarddB6uX973HeVmzb2v2m1LFKIy+i0zvb/Fqb8+puljlEAZXSMu1edceOu0WLgubKW98d9j8No48WBxcOzRVovr6wZN151dBGfLZ28HvetZtP+gtB+vhk7r/b3964a5utBC8nnmNHqtFqtwd0rFD9eHSSDA8aWFo0f475znF6fdeT5XMhW7yow7vMDhIjxbOLb6rcyPRCQcTDZZtw4w7pQtlrfjpuCo9s9VAwUqPNy3BxMvW5l8KGA9LNxkzC0N2QXjCBOphX3OvlY0JY1iX1xL6cm6qp1d9w8Kb3OksdwdDBpmeptI1/AAulN1rBqYzBEIgNrabK64plcHCS9bbHJW3vjporohVyuTQbtAeZb48YKVP+HXCA0Bto1jM1bE5Kh1lduyOWi+pmmoRo1WZ4IZ3NSq92TcqP6VtHcwv2E9WrcdDMzJaVWebQgA2htF80dbLcl4zbi4VFNqVxflgLcNIsa+tudMWc2KzWFSqqNWtfMormJDi2ErIyRr1+zASvPZdqovkMB1yUc08KTSn9MfPX7jgGVGzK5jA1Y1HoSP0HuVWr36+MAPGy6gJU3EoTGTrFYNNeYbRT6YPm6UmseXj7gicsn5WYIWa9q1I8y26h+DbCu8aaiKLWTS+arH3mASuUx2s0Q0q2qmqkyf0DfNNeAD41j+ntqR5faUg8VX5Uvl9CAPQqoasUzFqFBEwEol1t4WHF+1yW21FItIMRHChdQLTIDBelpxWIb+JS8cQlrpcsCXKlFTIiNFHQwYQNWbzKTGaNN3WwRHHEcu7+tdkmh8U7Egsp3SBP6gF2mG+10TcfPAhdc+LLiId65BL7QxziOFNkLjTMHUG0PWE+kYwdKSrgDPALDa6aX4m/qzYgB0bFQ9wCra6zvW4sOIM0GoFBCwl/ZHDFiXYnpGBcL9S3VUXWb1UZpnHABiyZk5eXrlfCXro4ScDUOWHmC8jMB4E1WqNe3fUA4XhjPI4SjREwAKhVUqAgAoS7mfmMtAKTxAnC2NDdVLgOxngS8jjFhANhlPQ59rRgVlJYvx3/ziPpi3MnYhI8QfkbfqrqA7R6DMAFobgJXDeLFKN1N/UhJCtFIfQuqKitZs+KARbML9FbjqxihMpKg8aCW4MMEwwCwyhoxWdvFhKDRI3kcJ6w9yB/wfhIQ00j9JsqOE9ammSQ0t4Hr9hK/vHY/b8CTFCCikeprvgVZ6XbnWgqwaK5D8eJGEjHnAWMfABSG+xBwkWXBnTQgbaZQavqwkkTMtXqTDIROI30oiBUhIKOAT4wuBAhm38ajJGG+YTEZJxzCN/xGqq/5ffAmDGj02iAgWIoj36UJj/IDTHsZm5BvwtCCjFRGP9NgQKr0T5B76TvIz9tAnVBRbnC7YQgI+Y0CFCUiRgSCvnEMIObUFZPJmmdC7ug+bKLrYFsm1k2mAamAoL90Pd1M80rfHsCEvGgYBYQI9R7sYwKlh1BpZ2orl8BfAtsot4qob/tNtAsCWmvsLug207T3hZxpPsWpOgyoHLMr86EFQUDSAsJ8QmqqgZA3EKFSG76dwm2UN1uhb3MBdVaQiBkxEhKJbv8/5ExzaaewH/UcjQ6V/kJAsA9am4IW6hKGHphsbW7ZfzDgGxnan0Kx3iF8Qm24DkQCPqBRWEfw2QoveM00u/S/y0C4sNUcDhBKuF3CR4axDgRzfTMATDdjoq+1NRxg6GucCcd2hxEuhk3BWW6GEn5H/UmaMATcSQPqg52qiiQsBms1LDuwmJt6YpgfQRzG2dxhAdKhk9WtpjJOfZENSMii6szMII3oD7g6qv3HtgUHRFtDJG+rTBPSYGFVUzm1dZOZbJPOljMxgyYsqt4ldPcneslCRsSI2QcZrEhhExqDFKG+4wMmx4NEP1uvuh+iCb2hPhm4f1zTwZDvKPNsxiHbhHT4u5UgJCQEtBJ8tAN6n+EJvYk24tZSzW39MZOwlnVmkdMLletJQnsGGyxZEGtwU/X5ZAjdqO/V+ynhPSZhViNyeqE99Rtvpd78Z6roRPl2InwyhEXVrn7r3iBkyyBswow98T7HhJWHS6QaGdz6k0sUZitq18JatxrlSxOa2nmVZUS7MVjeKGRAGEmNo0zulB0LHcIFqx0S0vGuX9kOFtISo/DPf81//1ZVuYQ/zz99+u4cTnQ06muI6v6vDo6BAyNmiYnMdMYhfGJQz+mP3zvX/BbadpdCE13X1xb//cMP8/MfftQ4hObbp/Pz87duwWakiY3nSu1COJcwS2LDA1Qqzw1CE1BnLE4Kvo9Ruz3dMEhhsLa4rmrvbs3b+umUQ3j+k/OdW+8Z2erAm5ayG2yqZBpDlAdkDSo8wq8M0lPVtmXXtduRntbudttVW+q5Czh/68cqk9B8638JdkBm98Cb/KZNY4FLKD/E4ER7l9AO8FWdBIlaUqf+zb/jEL7zv3QO29Dcdtcv2I1lgZF6u5IeJ/JCheJWachWtUrOugxAHOF7AWFRc77vrChmpt6uEWUDBtfPeHUoasSdKgtQVd17n//wlkP48wfvWwxAv7HaHk1AKOtrWCPfKCEZqGw+tfqje/cf2rG/jnc47an3FBClNz6hbAWcl5IGhOFoENYvFPHW0595Ed+c+4m20w+/8E3oJvICQslmKmikfrU0GE3Aeks74VziGwmnaZ6/u/X9Wz6gN40hIpRrpny+sB5srPMQq5qmJT9PhQWTigvY9kYqAkJFkQEUeNJIxZvwrZiWRObtWdCvZ4kIpZopo84dIfzKr1Lo26oT4PMnpKal/4RDMSGhTP07veyCSVjQyeZ6d6ebN6HZ3VpvdzdJOFLhR3xFypvCs00xwueRSpOhW501rBXRhGu6blnRoaaQUGImip+TOoRPErU0HWtELKGammDj5qW2JIoZolgBTOIb20gjYivC6SUn3LGFQ4iPF/ysGyQs6G0xnQRh2oTc8aErdPYt7obADDDWiDhCaNWQmBC93E2UsingJgSDOcqQJzShJfu8Oo0rdEQURkMFWqdAtnIkBPdfilwpPiKuiAGV4/SqPQuV3mAIwb1thFMv9QmxrkbsaOyqfvoeernZELJgcnkiJKyrQQAqx8B6WB3jbBCEJriQEV6qkBAOkFsoDQQtTNS5Iw0soQnvOomvZoeFLJsiXKk9QwqtshiIg6KYUIN3nbDnDyOEuKwG40oZe0kQ7VRICLdRxPBQQdcUxTmbEhtcRCX2pyJC5h7hZVHSpqDDBW9GJiRkrC7V2wJEAaEJpGseIea2cJvbhINDh5CxcI8MeHh0qKzxixaMTmhP32FuCxcuBIVETzcYSxN19kix2v31t19//+O3Z5qpskzI3AQNLaFNC7e4BhUslGPGnbBLjOt//MfXMwahucjcBI0Kh8iAiCNkr9QPVmQkTfj7nx9tvI8fPzI6I++0DEywQE5B4QI+b/Ulq4paffbxT1v//Qu/vjsQa0nUCAk5K2hZVdSquv7s2e9/FRmA7B20Bczo0CHEJDXCWqlHyNuMwCwU276U5WS4gKzFl6Mk5O56YmeorHgIbuiKPDN4AW2KEDMGRhIqCnetPmEhspxMl7+JyniSHyEq8aaqcO+oYDDcDWM6m7FnIZC4WDoCQsE+bgue/gYJzWvCQ5VQjibfVirc5mxtQhOoIOGm6EQezAA/f0LhBsv4Kg0moaltCZpoeg/ppRAqinCfMymkK4zp+cM2EW8nxgwOHUJMtEBGfAVzJob+xamQ8PwLxKlYqKFT7oSMQXBUxhdzp20uoXY694XYhNhuiFwahSYUH75DCefmuKu+6OcIQmQ0zJuQPYCKE87NtRmEVedTBCGmguEKRYgbASuMehtEGG2qIWH1dA5JiExKFew0MKbk7RIKD/7wCSOMPqF66n8kJsTGCuxyaMy0hSvhWXQhYQDpEGrnkb8XEy5cR94Q8pglVDXRkfBsE30xikIhT7tt9fw09neq8KhWfCNFVhPFs/gBoShe6IvV9hxXpxp4IE1MmHq+R4irCGNTb0W029nbJXTKAWwzjtyJE6I9KXKKFJ/UKBXWiSxRQrXNYjzVighCdLjHr4VGA8aX1TAJKSMIqHojJ9HQEFVlc4Tdi4gOF+y6cIIQYDz1S6ZCQvH8fSDstgu8M1UU/rmQ0UXgsbZ6Gm4/EBHigyF+lhs1veZKcPxHYpm7C3najVW8RYQLuNG9Q4hdsI8fIVJxfQ2wkD89PuQTIotsLiF2PU3qyCuO+CExB0JkCcoRfnEib1teUsCykzwJcVNOnvD7u2RcDbfkNjwhbrrClcQSWomshp/XDE0o0wullkFLAHKNCMwlyhHih76K3NkDEjGfF/XJ0l4SEKgm7i0x3TFyWtSTzDZLiYjImWczjNszF0LCi5nbBuMCBFnpdiW1fU0i+aY6hm1ADPsdeJ8EhJ9n2O/gkchIFdl9pBIRkXWwPmk5L/mrCwid27oNvigKP/J1JLfxSSZeUBWAU0wM7y2GF7ydXUXP/YFWXL4hZUK544akEjdwxXAveE3jZw7hZ/9Lt9MPaQE9tHcJJTc7o5YNhYhJZxNY0NYnJuGn8Espd0MeS92C9KkDUt40tdzUWIq+O6XOJIw+9o2lGCIhMn40w5lYiPX6UcUP3DUSrxO7YBBexL610YsiYmebAknvyEet34sgPl9iAsbaaYTwc+JbUcQF/LjXlfyB9Piaoof42L89w0i/3uccIkw99Y2gLxoyQwpbWY43kQqJtrzycNqCM9GuqLEBQysakl4m2zmYkr5GqbjeZuEMfEHTRYrwAvraxmO7P5N7cl4m65lmkoRK5cYSMRbez8JjmE8Jwk/gt1Zn3y8YpHcs2UbldsgGksxrnLULb/43O7sLXq2eIIRd3+7s7P/eFKQBM55oJpd+UzWb9AZnZ8vwKOYiRgi20Zn7ZfsCu01pH5DxQLMVKcRmszbrqgz7tU8RwmSgcHVY9q5Qk2PMfCidTHIa8s2y2qkbMlxC+Au74SXkGDOfSYc2otc+fZXhHLEeEMJ3dKccvQi+rQ5xriCuJ9I7ifFRNWDnbQ+kNGYn7DcSV9lVcJAZTt8JhHCnseYZCr7eJ4cQDhQzZeAymMY63Dm0Yryk+bx2CucY9XObEG6jRxAhxpDDHe/JyU6bTDwHEX6wF22tCL+UsgQDOpDUkmzKYY9oZdQV+XiO4NTmQoMDxSr/YhzIYV8BBZW/bdciwKNGZDQeRidssk3oQ+6CkEMcfOkpHjGaTcBzMhBhHw53whMhoEvp3kAUMIeXQDRDOqVZAx0nrAb0dOsg4moyUHBUq0UphzxF2JHbThEdL30vECBoRYnn5ijolpnP9YyJtlNsy4yrnKoslCL/jv4GXBtNUNK7yutFJUeyjzhATDxhxSNMjOYOJdpoTLm9T2c1yyN2FE/BjxoeYSOeD2RpH44aub3VspT1IcdS8AdlLw0olcvRh38n6wNs5Pgyncw3Ue5HrxEQzkYQ+5mvneNbWOSdXSjfc9oPKSQMrVvPfGXGKDSjBEkVW/6DduoTEcKg1sFIuBHK+Q16mf2d613ceBAl9GJJ5i7OGIIOoezehrbTFfeHY4SzDYpYnwQv4ytTWLbVnDnxnk6c0L7LZsZrMup5Q+pBRsRy0NUShJFPpC85CkDMCEd0YwnCzNfJI98eCWJOhOVsNfzLQMyHcJSANHce6uZyISyP6j2rnoayYh6Eo7Wgrew5SC6Eo/KiUWXOwvMgZMwX5KyTrNnN8IQN+fUImZR9vDMkIaPMPAId7ma7x+EIy7u5DenFqmeLGkMRjjpKJLWSpTMOQ9gYSa7NU39W/j6zE5Zncx8OilWXD/6ZCcujeZm6UKWy5K1mJCxfng9NStaM2QjHZUBXJam4kYWwvDs2A7qqyxQ3MhCWV8ZpQFerTXTgkCZsNHMuGWZUv4a8ZUnCcr4viB9KfVyOI0VYViaHz1ZfaYjvG09YbkwYn63DIyEjlrDcOLrEJFtC9ZVZfg6AIqSXmAD/yVT/iMeIICyXjyavecZVP2H3SBEh7X0nE2y+UPVSswyakkdIf6JZuhJ4nvorSrpTsgjpF5WVSW+cgOr9laPdciPCmSYs0893j1b6V8l4CdUPS/ebu40GBaU0/loM5w+Nxm7zfunwCsNFVT/s90snXkvsr5yU+v2/C9pUU0011VRTTTXVZej/K6htT+WJd6sAAAAASUVORK5CYII='
                                }}
                                size={50}
                            />
                            <View style={{marginLeft:15, flexDirection:'column'}}>
                        <Title style={styles.title}>{NomUsu}</Title>
                                <Caption style={styles.caption}>@j_doe</Caption>
                            </View>
                        </View>  */}

            {/* <View style={styles.row}>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>80</Paragraph>
                                <Caption style={styles.caption}>Following</Caption>
                            </View>
                            <View style={styles.section}>
                                <Paragraph style={[styles.paragraph, styles.caption]}>100</Paragraph>
                                <Caption style={styles.caption}>Followers</Caption>
                            </View>
                        </View> */}
          </View>

          <Drawer.Section style={styles.drawerSection}>
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="account-outline" color={color} size={size} />
              )}
              label="Selecciona Profesional"
              onPress={() => {
                props.navigation.navigate("SelProfesionales");
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="bookmark-outline" color={color} size={size} />
              )}
              label="Historia de Turnos"
              onPress={() => {
                props.navigation.navigate("HistorialTurnos");
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="map" color={color} size={size} />
              )}
              label="Voy en camino"
              onPress={() => {
                props.navigation.navigate("EnCaminoNew");
              }}
            />
            {/* <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="settings-outline" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Settings"
                            onPress={() => {props.navigation.navigate('SettingsScreen')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="account-check-outline" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Support"
                            onPress={() => {props.navigation.navigate('SupportScreen')}}
                        /> */}
          </Drawer.Section>
          {/* <Drawer.Section title="Preferences">
                        <TouchableRipple onPress={() => {toggleTheme()}}>
                            <View style={styles.preference}>
                                <Text>Dark Theme</Text>
                                <View pointerEvents="none">
                                    <Switch value={paperTheme.dark}/>
                                </View>
                            </View>
                        </TouchableRipple>
                    </Drawer.Section> */}
        </View>
      </DrawerContentScrollView>
      <Drawer.Section style={styles.bottomDrawerSection}>
        <DrawerItem
          icon={({ color, size }) => (
            <Icon name="exit-to-app" color={color} size={size} />
          )}
          label="Salir"
          onPress={() => {
            signOut();
          }}
        />
      </Drawer.Section>
      <Drawer.Section>
        <Text style={styles.bottomDrawerSectionVersion}>
          Versión {VersionActual}
        </Text>
      </Drawer.Section>
    </View>
  );
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: "bold",
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  section: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: 15,
  },
  paragraph: {
    fontWeight: "bold",
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },
  bottomDrawerSection: {
    marginBottom: 0,
    borderTopColor: "#f4f4f4",
    borderTopWidth: 1,
  },
  bottomDrawerSectionVersion: {
    textAlign: "center",
    maxHeight: "auto",
    marginTop: 0,
    height: "auto",
    width: "auto",
    borderTopColor: "#f4f4f4",
  },
  preference: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});
