import * as React from "react";
import { Text, View, StyleSheet, Dimensions } from "react-native";
import * as Location from "expo-location";
import { SafeAreaView } from "react-native-safe-area-context";
import { useStateIfMounted } from "use-state-if-mounted";
import MapView from "react-native-maps";

var PROVIDER_GOOGLE = "AIzaSyAXPe00G2I6mAhBGIF0rHiX2bR8jSEVS3c";

export default function Geo() {
  const [location, setLocation] = useStateIfMounted(null);
  const [errorMsg, setErrorMsg] = useStateIfMounted(null);

  React.useEffect(() => {
    (async () => {
      let { status } = await Location.requestPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("El permiso a la ubicación fue denegado. :( ");
      }
      let location = await Location.getCurrentPositionAsync({});
      var region = {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
        latitudeDelta: 1.015,
        longitudeDelta: 1.0134,
      };
      setLocation(region);
    })();
  });

  let text = "Waiting..";
  if (errorMsg) {
    text = errorMsg;
  } else if (location) {
    text = JSON.stringify(location);
  }

  return (
    <SafeAreaView>
      <View style={styles.containerMaps}>
        <MapView
          style={styles.mapStyle}
          region={location}
          showsUserLocation={true}
          loadingEnabled
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  containerMaps: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  mapStyle: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
});
