import * as React from "react";
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    Linking,
    AppState,
    BackHandler,
} from "react-native";
import * as Location from "expo-location";
import { SafeAreaView } from "react-native-safe-area-context";
import { useStateIfMounted } from "use-state-if-mounted";
import MapView from "react-native-maps";
import { Button } from "react-native-paper";
import { getDistance, getPreciseDistance } from "geolib";
import * as TaskManager from "expo-task-manager";
import * as Permissions from "expo-permissions";
import { app } from "firebase";
import { useIsFocused } from "@react-navigation/native";
import { reject } from "lodash";

// const APIKEYYANDEX = "pdct.1.1.20200907T160136Z.ca66d5a7d01bb32f.532b5fa0fffaf40bb6f65d3061ec02dc91644797"
// YandexMapKit.setApiKey(APIKEYYANDEX);
//CLAVE DE TESTEO.

const LOCATION_TRACKING = "location-tracking";
var PROVIDER_GOOGLE = "AIzaSyDSFsbs_OR3TCev61qmD75D7hlXSUw7RqQ";
const CIMYNLINKMAPS =
    "https://www.google.com.ar/maps/dir//Cimyn,+Catamarca+Sur+417,+J5402GSI+San+Juan,+Argentina/@-31.5394441,-68.5650085,13z/data=!4m9!4m8!1m0!1m5!1m1!1s0x96814025fdf97d43:0xcd4fdb678e7e7e83!2m2!1d-68.529989!2d-31.539522!3e0";
const latitudCimyn = "-31.5395174";
const longitudCimyn = "-68.5321777";
var dataLongitude = null;
var dataLatitude = null;
var dataSpeed = null;
// const latitudCimyn = "-31.538017";
// const longitudCimyn = "-68.535009";
// -31.538017, -68.535009;

export default function VoyEnCamino(props) {
    const [location, setLocation] = useStateIfMounted();
    const [errorMsg, setErrorMsg] = useStateIfMounted("");
    const [FirstDOM, setFirstDOM] = useStateIfMounted(false);
    const [getDistancia, setDistancia] = useStateIfMounted(null);
    const [getActualLatitud, setActualLatitud] = useStateIfMounted("");
    const [getActualLongitud, setActualLongitud] = useStateIfMounted("");
    const [getTimer, setTimer] = useStateIfMounted();
    const [getCounter, setCounter] = useStateIfMounted("0");
    const [getTiempoLlegada, setTiempoLlegada] = useStateIfMounted("...");
    const [getStateTask, setStateTask] = useStateIfMounted(false);
    const [appState, setAppState] = useStateIfMounted();
    const [getInit, setInit] = useStateIfMounted(false);

    const isFocused = useIsFocused();

    const startLocationTracking = async () => {
        if (getStateTask === false) {
            await Location.startLocationUpdatesAsync(LOCATION_TRACKING, {
                accuracy: Location.Accuracy.BestForNavigation,
                timeInterval: 5000,
                distanceInterval: 0,
                showsBackgroundLocationIndicator: true,
                pausesUpdatesAutomatically: false,
                activityType: Location.ActivityType.AutomotiveNavigation,
                // showsBackgroundLocationIndicator: true,
                foregroundService: {
                    notificationTitle: "Sanatorio Digital",
                    notificationBody:
                        "Estas en camino al sanatorio. Te van a estar esperando",
                    // notificationColor: "#D9ECFF",
                },
            })
                .then(async () => {
                    // console.log("tracking started?", hasStarted);
                    setStateTask(true);
                    const hasStarted = await Location.hasStartedLocationUpdatesAsync(
                        LOCATION_TRACKING
                    );
                })
                .catch(function (error) {
                    // console.log("no podemos iniciar la navegacion");
                });
        } else {
            await Location.stopLocationUpdatesAsync(LOCATION_TRACKING)
                .then(async () => {
                    // console.log("tracking stoped", hasStoped);
                    setStateTask(false);
                    const hasStoped = await Location.hasStartedLocationUpdatesAsync(
                        LOCATION_TRACKING
                    );
                })
                .catch(function (error) {
                    // console.log("No podemos detener la navegacion");
                });
        }
        setInit(true);
    };

    var contador = 0;

    React.useEffect(() => {
        if (FirstDOM === false) {
            (async () => {
                let { status } = await Location.requestPermissionsAsync();
                if (status !== "granted") {
                    setErrorMsg(
                        "No se han dado permisos para el mapa. Vuelve a intentarlo más tarde"
                    );
                }
                let location = await Location.getCurrentPositionAsync({
                    accuracy: Location.Accuracy.High,
                });
            })();
            // setTimer;
            // let timer = setInterval(tick, 5000);
            // setTimer(timer);
            //setFirstDOM for avoid loop
            setFirstDOM(true);
        }
    }, [FirstDOM]);

    React.useEffect(() => {
        const backAction = () => {
            props.navigation.navigate("SelProfesionales");
            return true;
        };

        const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
        );
        return () => backHandler.remove();
    }, []);
    // React.useEffect(() => {
    //     AppState.addEventListener("change", handleAppStateChange);
    //     if (isFocused === false) {
    //         // stopIt();
    //     }
    // });

    React.useEffect(() => {
        const config = async () => {
            let res = await Permissions.askAsync(Permissions.LOCATION);
            if (res.status !== "granted") {
                console.log("El permiso fue denegado");
            } else {
                console.log("permiso concedido");
            }
        };

        config();

        (async () => {
            let { status } = await Location.requestPermissionsAsync();
            if (status !== "granted") {
                setErrorMsg(
                    "No se han dado permisos para el mapa. Vuelve a intentarlo más tarde"
                );
            }
            let location = await Location.getCurrentPositionAsync({
                accuracy: Location.Accuracy.High,
            });
            var region = {
                latitude: location.coords.latitude,
                longitude: location.coords.longitude,
                latitudeDelta: 1.015,
                longitudeDelta: 1.0134,
            };

            setLocation(region);
        })();
    }, []);

    // const handleAppStateChange = (nextAppState) => {
    //     if (nextAppState === "active") {
    //         try {
    //             // startLocationTracking();
    //         } catch (error) {
    //             // console.log("ya se inició la navegación");
    //         }
    //         // console.log("La app esta activa");
    //     }
    //     if (nextAppState === "inactive") {
    //         try {
    //             // stopIt();
    //         } catch (error) {
    //             // console.log("ya se detuvo la navegacion");
    //         }
    //         // console.log("la app esta inactiva");
    //     }
    //     if (nextAppState === "background") {
    //         try {
    //             // stopIt();
    //         } catch (error) {
    //             // console.log("ya se detuvo la navegacion");
    //         }
    //         // console.log("i am in background");
    //     }
    //     //     if (nextAppState === 'inactive') {
    //     //     console.log('the app is closed');
    //     //    }
    // };
    const tick = async () => {
        contador = contador + 1;
        if (dataSpeed != "") {
            setInit(true);
            var velocityASFlashMetersPerSecond = dataSpeed;
            if (velocityASFlashMetersPerSecond.toFixed(0) == 0) {
                velocityASFlashMetersPerSecond = 8.33;
            }
            var velocityASFlashKilometerPerHour =
                (velocityASFlashMetersPerSecond / 1000) * 3600;
            var dis = getDistance(
                {
                    latitude: latitudCimyn,
                    longitude: longitudCimyn,
                },
                {
                    latitude: dataLatitude,
                    longitude: dataLongitude,
                }
            );
            try {
                setDistancia((dis / 1000).toFixed(1));
            } catch (error) {
                console.log(
                    "No hay valores en las variables globales: " + error
                );
            }
            const TiempoDeLLegada =
                dis / 1000 / velocityASFlashKilometerPerHour;
            function secondsToString(seconds) {
                var hour = Math.floor(seconds / 3600);
                hour = hour < 10 ? "0" + hour : hour;
                var minute = Math.floor((seconds / 60) % 60);
                minute = minute < 10 ? "0" + minute : minute;
                var second = seconds % 60;
                second = second < 10 ? "0" + second : second;
                return hour + ":" + minute + ":" + second;
            }
            var horas = secondsToString(TiempoDeLLegada * 3600);
            horas = horas.split(".")[0];
            const hora = horas.split(":")[0];
            const minutos = horas.split(":")[1];
            const segundos = horas.split(":")[2];
            if (hora >= 3) {
                console.log("demasiado tiempo.");
                setTiempoLlegada("No se puede calcular");
            } else {
                setTiempoLlegada(
                    hora +
                        " horas, " +
                        minutos +
                        " minutos y " +
                        segundos +
                        " segundos"
                );
            }
        }
    };

    const stopIt = async () => {
        await Location.stopLocationUpdatesAsync(LOCATION_TRACKING)
            .then(async () => {
                // console.log("tracking stoped", hasStoped);
                setStateTask(false);
                const hasStoped = await Location.hasStartedLocationUpdatesAsync(
                    LOCATION_TRACKING
                );
            })
            .catch(function (error) {
                // console.log("no podemos detener la navegacion");
            });
    };

    return (
        <View style={styles.fullScreen}>
            <View style={styles.containerMaps}>
                {/* <MapboxGL.MapView /> */}

                <MapView
                    style={styles.mapStyle}
                    region={location}
                    showsUserLocation={true}
                    loadingEnabled
                />
            </View>
            {getInit == false ? (
                <View style={styles.containerViews}>
                    <Text style={styles.containerViewsTitle}>
                        Pulse iniciar para calcular su tiempo estimado
                    </Text>
                </View>
            ) : getTiempoLlegada === "..." ? (
                <View style={styles.containerViews}>
                    <Text style={styles.containerViewsTitle}>
                        Estamos calculando... Por favor espera...
                    </Text>
                </View>
            ) : (
                <View style={styles.containerViews}>
                    <Text style={styles.containerViewsTitle}>
                        Estas a {getDistancia}Kms de Cimyn
                    </Text>
                    {getTiempoLlegada != null ? (
                        //getTiempoLlegada < 2 ||
                        getTiempoLlegada != "Infinity" ? (
                            <Text style={styles.containerViewsTitle}>
                                Tiempo aproximado: {getTiempoLlegada} para
                                llegar a Cimyn
                            </Text>
                        ) : (
                            <Text style={styles.containerViewsTitle}>
                                No pudimos calcular el tiempo de estimado
                            </Text>
                        )
                    ) : null}
                </View>
            )}
            <Button
                style={styles.ButtonGO}
                onPress={() => Linking.openURL(CIMYNLINKMAPS)}
            >
                Ir a cimyn
            </Button>
            {getStateTask == false ? (
                <Button style={styles.ButtonGO} onPress={startLocationTracking}>
                    Empezar a navegar
                </Button>
            ) : (
                <Button style={styles.ButtonGO} onPress={startLocationTracking}>
                    Llegue al sanatorio
                </Button>
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    containerMaps: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },
    mapStyle: {
        // width: Dimensions.get('window').width,
        // height: Dimensions.get('window').height,
        width: "100%",
        height: "100%",
    },
    fullScreen: {
        // width: Dimensions.get('window').width,
        // height: Dimensions.get('window').height,
        width: "100%",
        height: "100%",
    },
    containerViews: {
        width: "100%",
        height: "35%",
    },
    containerViewsTitle: {
        fontSize: 30,
        textAlign: "center",
    },
    ButtonGO: {
        backgroundColor: "#A9A9A9",
        color: "white",
    },
});

TaskManager.defineTask(LOCATION_TRACKING, async ({ data, error }) => {
    if (error) {
        // console.log("LOCATION_TRACKING task ERROR:", error);
        return;
    }
    if (data) {
        // console.log(data);
        const { locations } = data;
        dataLongitude = locations[0].coords.longitude;
        dataLatitude = locations[0].coords.latitude;
        dataSpeed = locations[0].coords.speed;
        let lat = locations[0].coords.latitude;
        let long = locations[0].coords.longitude;
        fetch(
            "http://192.168.0.13:3000/testingLocation?location=" +
                lat +
                "&" +
                long,
            {
                method: "GET",
                headers: { "Content-Type": "application/json" },
            }
        )
            .then((response) => response.json)
            .then((result) => {
                console.log(result);
            })
            .catch((reason) => {
                console.log(reason);
            });
        console.log(`${new Date(Date.now()).toLocaleString()}: ${lat},${long}`);
    }
});

// export default function VoyEnCamino(props) {
//     function handleBackButtonClick() {
//         props.navigation.goBack();
//         return true;
//     }
//     return <EnCamino />;
// }
