import React from "react";
import format from "date-format";
import * as BackgroundFetch from "expo-background-fetch";
import * as TaskManager from "expo-task-manager";
import * as Location from "expo-location";

import {
  AppState,
  AsyncStorage,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  BackHandler,
} from "react-native";

const BACKGROUND_FETCH_TASK = "background-fetch";
const LAST_FETCH_DATE_KEY = "background-fetch-date";
const LOCATION_TRACKING = "location-tracking";

class BackgroundFetchScreen extends React.Component {
  state = {
    fetchDate: null,
    status: null,
    isRegistered: false,
  };

  componentDidMount() {
    AppState.addEventListener("change", this.handleAppStateChange);
    (async () => {
      let { status } = await Location.requestPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg(
          "No se han dado permisos para el mapa. Vuelve a intentarlo más tarde"
        );
      }
      let location = await Location.getCurrentPositionAsync({
        accuracy: Location.Accuracy.High,
      });
    })();
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this.handleAppStateChange);
  }

  handleAppStateChange = (nextAppState) => {
    if (nextAppState === "active") {
      this.refreshLastFetchDateAsync();
      this.checkStatusAsync();
    }
  };

  async refreshLastFetchDateAsync() {
    const lastFetchDateStr = await AsyncStorage.getItem(LAST_FETCH_DATE_KEY);

    if (lastFetchDateStr) {
      this.setState({ fetchDate: new Date(+lastFetchDateStr) });
    }
  }

  async checkStatusAsync() {
    const status = await BackgroundFetch.getStatusAsync();
    const isRegistered = await TaskManager.isTaskRegisteredAsync(
      BACKGROUND_FETCH_TASK
    );
    console.log({ status, isRegistered });
    this.setState({ status, isRegistered });
  }

  toggle = async () => {
    if (this.state.isRegistered) {
      await BackgroundFetch.unregisterTaskAsync(BACKGROUND_FETCH_TASK);
      await Location.stopLocationUpdatesAsync(LOCATION_TRACKING);
    } else {
      await BackgroundFetch.registerTaskAsync(BACKGROUND_FETCH_TASK, {
        minimumInterval: 60, // 1 minute
        stopOnTerminate: false,
        startOnBoot: true,
      });
      await Location.startLocationUpdatesAsync(LOCATION_TRACKING, {
        accuracy: Location.Accuracy.BestForNavigation,
        timeInterval: 5000,
        distanceInterval: 0,
        showsBackgroundLocationIndicator: true,
        pausesUpdatesAutomatically: false,
        activityType: Location.ActivityType.AutomotiveNavigation,
        // showsBackgroundLocationIndicator: true,
        foregroundService: {
          notificationTitle: "Sanatorio Digital",
          notificationBody:
            "Estas en camino al sanatorio. Te van a estar esperando",
          notificationColor: "#D9ECFF",
        },
      })
        .then(async () => {
          // console.log("tracking started?", hasStarted);
          setStateTask(true);
          const hasStarted = await Location.hasStartedLocationUpdatesAsync(
            LOCATION_TRACKING
          );
        })
        .catch(function (error) {
          console.log("WE CANNOT INIT NAVIGATION");
        });
    }
    console.log("register");
    this.setState({ isRegistered: !this.state.isRegistered });
  };

  //   renderText() {
  //     if (!this.state.fetchDate) {
  //       return <Text>There was no BackgroundFetch call yet.</Text>;
  //     }
  //     return (
  //       <View style={{ flexDirection: "column", alignItems: "center" }}>
  //         <Text>Last background fetch was invoked at:</Text>
  //         <Text style={styles.boldText}>
  //           {format("yyyy-MM-dd hh:mm:ss:SSS", this.state.fetchDate)}
  //         </Text>
  //       </View>
  //     );
  //   }

  render() {
    return (
      <View style={styles.screen}>
        <View style={styles.textContainer}>
          <Text>
            Vamos a avisar que estas en camino:{" "}
            <Text style={styles.boldText}>
              {this.state.status
                ? BackgroundFetch.Status[this.state.status]
                : null}
            </Text>
          </Text>
        </View>
        <View style={styles.textContainer}>{/*this.renderText()*/}</View>
        <TouchableHighlight
          style={styles.button}
          onPress={/*this.toggle*/ () => console.log("Pressed")}
        >
          <Text>
            {this.state.isRegistered
              ? "Cancelar localización"
              : "Registrar localización"}
          </Text>
        </TouchableHighlight>
      </View>
    );
  }
}

// TaskManager.defineTask(BACKGROUND_FETCH_TASK, async () => {
//   const now = Date.now();

//   console.log(
//     `Got background fetch call at date: ${new Date(now).toISOString()}`
//   );
//   await AsyncStorage.setItem(LAST_FETCH_DATE_KEY, now.toString());

//   return BackgroundFetch.Result.NewData;
// });

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    padding: 10,
    marginVertical: 5,
  },
  textContainer: {
    margin: 10,
  },
  boldText: {
    fontWeight: "bold",
  },
});

export default function VoyEnCaminoNew(props) {
  BackHandler.addEventListener("hardwareBackPress", handleBackButton);
  function handleBackButton() {
    props.navigation.goBack();
    return true;
  }
  return <BackgroundFetchScreen />;
}
