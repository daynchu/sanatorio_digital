import { stylesPaciente } from "../../../../Asset/estilos/stylesPaciente";
import { TouchableOpacity, Text, View, Image } from "react-native";
import * as React from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import { useStateIfMounted } from "use-state-if-mounted";
import * as Sharing from "expo-sharing";
import { AsyncStorage } from "react-native";
import { Audio } from "expo-av";
import * as Calendar from "expo-calendar";
import moment from "moment";
import "moment/locale/es";
import { MenuGeneral } from "../../Global/Menu";

export default function PostConfirmTurno(props) {
  const { uriImage } = props.route.params;
  const [datos, setDatos] = useStateIfMounted();
  const [getNameDoctor, setNameDoctor] = useStateIfMounted();
  const [nomP, SetNomP] = useStateIfMounted("");
  React.useEffect(() => {
    getDataFromAsync();
    ReproducirSonido();
  }, []);
  const ReproducirSonido = async () => {
    const {
      sound: soundObject,
      status,
    } = await Audio.Sound.createAsync(
      require("../../../../Asset/sounds/dingdong.mp3"),
      { shouldPlay: true }
    );
  };
  const getDataFromAsync = async () => {
    setDatos(JSON.parse(await AsyncStorage.getItem("Idi")));
    setNameDoctor(await AsyncStorage.getItem("NM"));
    SetNomP(await AsyncStorage.getItem("UserName"));
  };
  const guardarEnCalendario = async () => {
    const { status } = await Calendar.requestCalendarPermissionsAsync();
    if (status === "granted") {
      const calendars = await Calendar.getCalendarsAsync();
      //   console.log('Here are all your calendars:');
      //   console.log({ calendars });
      try {
        const defaultCalendarSource =
          Platform.OS === "ios"
            ? // const { statusios } = await Calendar.requestRemindersPermissionsAsync()
              await getDefaultCalendarSource()
            : { isLocalAccount: true, name: "Sanatorio Digital" };
        const newCalendarID = await Calendar.createCalendarAsync({
          title: "Sanatorio Digital",
          color: "blue",
          entityType: Calendar.EntityTypes.EVENT,
          sourceId: defaultCalendarSource.id,
          source: defaultCalendarSource,
          name: "internalCalendarName",
          ownerAccount: "personal",
          accessLevel: Calendar.CalendarAccessLevel.OWNER,
        });
        // console.log(datos)
        var InitDate = moment(datos.Turno_Inicio)
          .add(3, "h")
          .format("YYYY-MM-DD[T]HH:mm:ss[.000Z]")
          .split(" ")
          .join("T");
        // console.log(InitDate)
        var EndDate = moment(datos.Turno_Fin)
          .add(3, "h")
          .format("YYYY-MM-DD[T]HH:mm:ss[.000Z]")
          .split(" ")
          .join("T");
        // console.log(EndDate)
        // console.log(getNameDoctor)

        // console.log(`Tu nuevo codigo de Calendario es: ${newCalendarID}`);
        const eventId = await Calendar.createEventAsync(newCalendarID, {
          endDate: EndDate,
          startDate: InitDate,
          alarms: [{ relativeOffset: -1440 }], //EXPRESADO EN MINUTOS.- UN DIA ANTES DEL TURNO
          location: "CIMYN",
          notes: "TURNO CON DR/DRA " + getNameDoctor.split("-")[0],
          timeZone: "GMT-3",
          title: "TURNO CONSULTA CON " + getNameDoctor,
        });
        // console.log("Event created success :"+eventId)

        // console.log("Event Id", id);
      } catch (error) {
        console.log("Error", error);
      }
    }
  };

  const openShareDialogAsync = async () => {
    if (!(await Sharing.isAvailableAsync())) {
      alert(`Ups, la opcion de compartir no es disponible con tu dispositivo`);
      return;
    }

    await Sharing.shareAsync(uriImage);
  };
  return (
    <SafeAreaView style={{ flex: 1, justifyContent: "space-between" }}>
      <Image
        source={require("../../../../Asset/Images/Paciente/DashPaciente/Trazado260.png")}
        style={stylesPaciente.backgroundImage}
      />
      <MenuGeneral
        cadenaTitulo={"Solicitar Turno"}
        navigation={props.navigation}
        color="paciente"
      ></MenuGeneral>
      <View style={{ height: "100%" }}>
        <View>
          <Text
            style={[
              stylesPaciente.textTitlePConfirmTSelectP,
              { marginBottom: "10%" },
            ]}
          >
            ¡Felicidades {nomP}!,
          </Text>
          <Text
            style={[
              stylesPaciente.textTitlePConfirmTSubtitleSelectP,
              { marginBottom: "10%", marginHorizontal: "5%" },
            ]}
          >
            Aqui podes compartir, imprimir o guardar tu turno.
          </Text>
        </View>
        <View style={stylesPaciente.boxButtons}>
          <TouchableOpacity
            style={stylesPaciente.buttons}
            onPress={() => guardarEnCalendario()}
          >
            <Image
              source={require("../../../../Asset/Images/Paciente/DashPaciente/Icon-awesome-calendar-alt.png")}
              style={stylesPaciente.cardImageInline}
            />
            <Text style={stylesPaciente.textButtons}>Guardar</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={stylesPaciente.buttons}
            onPress={() => {
              openShareDialogAsync();
            }}
          >
            <Image
              source={require("../../../../Asset/Images/Paciente/DashPaciente/Icon-awesome-share-alt.png")}
              style={stylesPaciente.cardImageInline}
            />
            <Text style={stylesPaciente.textButtons}>Compartir</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={stylesPaciente.buttons}
            onPress={() => {
              props.navigation.navigate("SelProfesionales");
            }}
          >
            <Image
              source={require("../../../../Asset/Images/Paciente/DashPaciente/Icon-awesome-undo-alt.png")}
              style={stylesPaciente.cardImageInline}
            />
            <Text style={stylesPaciente.textButtons}>Volver</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
}

