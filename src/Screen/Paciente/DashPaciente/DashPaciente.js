import * as React from 'react'
import { View, Text, Image,TouchableOpacity,BackHandler,Alert,AsyncStorage } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context';
import { stylesPaciente } from '../../../../Asset/estilos/stylesPaciente'
import Icons from 'react-native-vector-icons/Ionicons'
import { useStateIfMounted } from "use-state-if-mounted";
import { MenuGeneral } from "../../Global/Menu";

export default function DashPaciente(props) {

    const [UserName, SetUsername] = useStateIfMounted('')

    React.useEffect(() => {
        GetNombre()
    },[])

    const GetNombre = async() => {
        SetUsername(await AsyncStorage.getItem('UserName'))
    }


    React.useEffect(() => {
        const backAction = () => {
          Alert.alert("Hey", "Estas seguro que quieres salir?", [
            {
              text: "NO",
              onPress: () => null,
              style: "cancel"
            },
            { text: "SI", onPress: () => signOut() }
          ]);
          return true;
        };
    
        const backHandler = BackHandler.addEventListener(
          "hardwareBackPress",
          backAction
        );
        return () =>
        BackHandler.removeEventListener('hardwareBackPress');
      }
      );

    return (
        <SafeAreaView
            style={{ flex: 1, justifyContent: 'space-between' }}
        >
            <Image source={require('../../../../Asset/Images/Paciente/DashPaciente/Trazado260.png')} style={stylesPaciente.backgroundImage} />
            <MenuGeneral cadenaTitulo={'Hola '+UserName} navigation={props.navigation} color="paciente"></MenuGeneral>
            <View style={{ height: '100%' }}>                
                <View >
                    <Text style={{fontSize:25,color:'black', marginTop:'10%',fontWeight: 'bold',textAlign:'center'}}>¿En que podemos ayudarte?</Text>
                    <Text style={{fontSize:20,color:'black', marginTop:'5%', textAlign:'left', marginHorizontal:'5%'}}>En este espacio tendras toda la informacíon necesaria para gestionarte</Text>
                    <Text style={{fontSize:20,color:'black', marginTop:'5%',textAlign:'left',fontWeight: 'bold', marginHorizontal:'5%'}}>Estamos para ayudarte</Text>
                </View>
                <View>
                <TouchableOpacity onPress={() => { props.navigation.navigate('SelProfesionales') }}>
                <View style={stylesPaciente.card}>
                    <View style={stylesPaciente.cardImage}>
                        <Image source={require('../../../../Asset/Images/Paciente/DashPaciente/Grupo_455.png')} style={stylesPaciente.cardImageCircle}/>
                    </View>
                    <View style={stylesPaciente.cardText}>
                        <Text style={stylesPaciente.cardTitle}>Solicitar Turnos</Text>
                    </View>
                </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { props.navigation.navigate('HistorialTurnos')}}>
                <View style={stylesPaciente.card}>
                    <View style={stylesPaciente.cardImage}>
                        <Image source={require('../../../../Asset/Images/Paciente/DashPaciente/Grupo_454.png')} style={stylesPaciente.cardImageCircle}/>
                    </View>
                    <View style={stylesPaciente.cardText}>
                        <Text style={stylesPaciente.cardTitle}>Historial de Turnos</Text>
                    </View>
                </View>
                </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>

    )
}