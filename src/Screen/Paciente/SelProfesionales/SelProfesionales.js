import * as React from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  BackHandler,
  Alert,
  AsyncStorage,
  ActivityIndicator,
} from "react-native";

import { SelProfesiona, styles } from "../../../../Asset/estilos/Styles";
import { stylesPaciente } from "../../../../Asset/estilos/stylesPaciente";
import Select2 from "../../../Libs_personalizadas/react-native-select-two";
import { SafeAreaView } from "react-native-safe-area-context";
import { useStateIfMounted } from "use-state-if-mounted";
import { AuthContext } from "../../../../Component/context";
import { MenuGeneral } from "../../Global/Menu";
import RNPickerSelect from "react-native-picker-select";

const style = SelProfesiona;

const Profesionales = ({ navigation }) => {
  const { signOut } = React.useContext(AuthContext);
  const [esMedico, setEsMedico] = useStateIfMounted(false);
  const [dataFetch, setDataFetch] = useStateIfMounted([]);
  const [dataFetchMedicos, setDatafechMedicos] = useStateIfMounted([]);
  const [dataFetchServicios, setDatafechServicios] = useStateIfMounted([]);

  const [search, setSearch] = useStateIfMounted("");
  const [data, setData] = useStateIfMounted("");
  const [nomP, SetNomP] = useStateIfMounted("");
  const [getTipoTurno, setTipoTurno] = useStateIfMounted("");
  const [visibleModal, setVisibleModal] = useStateIfMounted(false);
  const [loading, setLoading] = useStateIfMounted(true);
  const TraerDatos = async () => {
    SetNomP(await AsyncStorage.getItem("UserName"));
    fetch(
      "https://app.excelenciadigital.ml/SanatorioDigitalApi/prestadores/medicosEspecialidad/?IdInstitucion=38&NomTipoPrestador=Médico"
    )
      .then((response) => {
        return response.ok ? response.json() : Promise.reject("algo va mal");
      })
      .then((medicos) => {
        setDatafechMedicos((dataFetch) =>
          dataFetch.concat(
            medicos.map((m) => {
              //console.log(m)
              if (m.idprestador == data[0]) {
                //console.log(m.idprestador+" + "+ data[0])
              }
              return {
                id: m.idprestador,
                name: `${m.nombre} ${m.apellido} - ${m.NombreEspecialidad}`,
                valorconsulta: m.valorconsulta,
              };
            })
          )
        );
      })
      .catch((reason) => {
        console.log(reason);
      });
  };
  const TraerServicios = async () => {
    SetNomP(await AsyncStorage.getItem("UserName"));
    fetch(
      "https://app.excelenciadigital.ml/SanatorioDigitalApiTest/servicios/por-institucion?idInstitucion=38"
    )
      .then((response) => {
        return response.ok ? response.json() : Promise.reject("algo va mal");
      })
      .then((servicios) => {
        setDatafechServicios((dataFetch) =>
          dataFetch.concat(
            servicios[0].map((m) => {
              return {
                id: m.IdPrestador,
                idService: m.IdServicio,
                name: m.Nombre,
              };
            })
          )
        );
        setLoading(false);
      })
      .catch((reason) => {
        console.log(reason);
      });
  };

  const VeriMedico = async () => {
    fetch(
      "https://app.excelenciadigital.ml/SanatorioDigitalApi/usuarios/uid/" +
        (await AsyncStorage.getItem("uid"))
    )
      .then((response) => response.json())
      .then((data) => {
        if (data.hasOwnProperty("roles")) {
          if (data.roles[0] == "PRESTADOR") setEsMedico(true);
        }
      });
  };

  React.useEffect(() => {
    setLoading(true);
    // BackHandler.addEventListener('hardwareBackPress', handleBackButton);
    VeriMedico();
    TraerDatos();
    TraerServicios();
    // setLoading(false);
  }, []);

  React.useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        "hardwareBackPress",
        handleBackButtonClick
      );
    };
  });
  function handleBackButtonClick() {
    navigation.goBack();
    return true;
  }

  const verCalendario = async () => {
    for (var i = 0; i < dataFetch.length; i++) {
      if (getTipoTurno == 1 /*CONSULTA MEDICA*/) {
        if (dataFetch[i].checked === true) {
          AsyncStorage.setItem("NM", JSON.stringify(dataFetch[i].name));
          AsyncStorage.setItem("Id", JSON.stringify(dataFetch[i].id));
          AsyncStorage.setItem("tipo", JSON.stringify(getTipoTurno));
          navigation.navigate("Turnos");
        }
      }
      if (getTipoTurno == 2 /* ESTUDIO MEDICO */) {
        if (dataFetch[i].checked === true) {
          console.log(dataFetch[i]);
          AsyncStorage.setItem("NM", JSON.stringify(dataFetch[i].name));
          AsyncStorage.setItem("Id", JSON.stringify(dataFetch[i].id));
          AsyncStorage.setItem(
            "IdServicio",
            JSON.stringify(dataFetch[i].idService)
          );
          AsyncStorage.setItem("tipo", JSON.stringify(getTipoTurno));
          AsyncStorage.setItem(
            "preparacion",
            JSON.stringify(dataFetch[0].Preparacion)
          );
          navigation.navigate("Turnos");
        }
      }
    }
  };

  const UpdateSearch = (search) => {
    setSearch(search);
  };

  return (
    <SafeAreaView style={{ flex: 1, justifyContent: "space-between" }}>
      <Image
        source={require("../../../../Asset/Images/Paciente/DashPaciente/Trazado260.png")}
        style={stylesPaciente.backgroundImage}
      />
      {loading == true ? (
        <View
          style={[stylesPaciente.containerLoad, stylesPaciente.horizontalLoad]}
        >
          <ActivityIndicator size="large" color="green" />
        </View>
      ) : (
        <View>
          <MenuGeneral
            cadenaTitulo={"Solicitar Turno"}
            navigation={navigation}
            color="paciente"
          ></MenuGeneral>
          <View style={{ height: "100%" }}>
            <View>
              <Text
                style={[
                  stylesPaciente.textTitlePConfirmTSelectP,
                  { marginBottom: "4%" },
                ]}
              >
                Hola :) {nomP}
              </Text>
              <Text
                style={[
                  stylesPaciente.textTitlePConfirmTSubtitleSelectP,
                  { marginBottom: "0%" },
                ]}
              >
                Selecciona el tipo de consulta que realizarás
              </Text>
              <View>
                <TouchableOpacity
                  onPress={() => {
                    setDataFetch(dataFetchMedicos);
                    setVisibleModal(true);
                    setTipoTurno(1);
                  }}
                >
                  <View style={stylesPaciente.buttonTurno}>
                    <View style={stylesPaciente.buttonTurnoText}>
                      <Text style={stylesPaciente.buttonTurnoTitle}>
                        Consulta Médica
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    setDataFetch(dataFetchServicios);
                    setVisibleModal(true);
                    setTipoTurno(2);
                  }}
                >
                  <View style={stylesPaciente.buttonTurno}>
                    <View style={stylesPaciente.buttonTurnoText}>
                      <Text style={stylesPaciente.buttonTurnoTitleOrange}>
                        Estudio Médico
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={stylesPaciente.Search}>
                <Select2
                  // initialMode={getTipoTurno}
                  visibleModal={visibleModal}
                  isSelectSingle
                  colorTheme="blue"
                  popupTitle="Pulsa para iniciar la busqueda"
                  title="Pulsa para iniciar la busqueda"
                  cancelButtonText="Volver"
                  selectButtonText="Vamos"
                  data={dataFetch}
                  searchPlaceHolderText="Aquí coloca su nombre o especialidad o servicio"
                  onSelect={(data) => {
                    if (getTipoTurno == 1 /* CONSULTA MEDICA */) {
                      console.log("Consulta Medica", data);
                      if (data.length) {
                        var medico = dataFetch.filter((med) => {
                          return med.id === data[0];
                        });
                        AsyncStorage.setItem(
                          "valorconsulta",
                          JSON.stringify(medico[0].valorconsulta)
                        );
                        setData(data);
                        verCalendario();
                      }
                    }
                    if (getTipoTurno == 2 /* ESTUDIO MEDICO */) {
                      console.log("Estudio Medico", data);
                      if (data.length) {
                        var medico = dataFetch.filter((est) => {
                          return est.IdPrestador === data[0];
                        });
                        AsyncStorage.setItem(
                          "valorconsulta",
                          JSON.stringify(500)
                        );
                        setData(data);
                        verCalendario();
                      }
                    }
                  }}
                  onRemoveItem={(data) => {
                    setData(data);
                  }}
                  onCancelButton={(estado) => {
                    setVisibleModal(estado);
                  }}
                />
              </View>
            </View>
            <View style={stylesPaciente.consultas}>
              {/*<View style={stylesPaciente.consultasConsultorio}>
      <Text style={stylesPaciente.text}>
       Aún no tienes{" "}
       <Text style={stylesPaciente.textBlue}>consultas médicas</Text>
     </Text> */}
              {/* <RNPickerSelect
       onValueChange={(value) => console.log(value)}
       items={[
         { label: "Football", value: "football" },
         { label: "Baseball", value: "baseball" },
         { label: "Hockey", value: "hockey" },
       ]}
     />
    
   </View>
    */}
              {/* <View style={stylesPaciente.consultasEstudio}>
      <Text style={stylesPaciente.text}>
       Aún no tienes{" "}
       <Text style={stylesPaciente.textOrange}>estudios médicos</Text>
     </Text> 
   </View>
   */}
              <View style={stylesPaciente.consultasEstudio}>
                <Text style={stylesPaciente.text}>
                  Estamos construyendo para una mejor experiencia :D
                  <Text style={stylesPaciente.textOrange}></Text>
                </Text>
              </View>
            </View>
          </View>
        </View>
      )}
    </SafeAreaView>
  );
};

export default Profesionales;
