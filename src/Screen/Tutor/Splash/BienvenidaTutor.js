import { TouchableOpacity, Text, View, StyleSheet, Image } from 'react-native'
import * as React  from 'react';
import  stylesTutor  from '../../../../Asset/estilos/stylesTutor'
import { AsyncStorage, Picker } from 'react-native';
import { useStateIfMounted } from "use-state-if-mounted";
import { SafeAreaView } from 'react-native-safe-area-context';

// const ImgBg = () => {
//   return (
//     <View style={styles.container}>
//       <Image
//         style={styles.stretch}
//         source={require('../../../../Asset/Images/Sinlog/Privacidad/doctores.png')}
//       />
//     </View>
//   );
// }

const BienvenidaTutor = ({ navigation }) => {

  const [Institucion, SetInstitucion] = useStateIfMounted('')
  const [InstitucionSeleccionada, SetInstitucionSeleccionada] = useStateIfMounted(false)
  const [UserName, SetUsername] = useStateIfMounted('')
  const [Sexo, SetSexo] = useStateIfMounted('')
  React.useEffect(() => {
    NombreUsuario()
    setTimeout(() => {
      navigation.navigate("InicioTutor")
    }, 3000);
  }, [])

  const NombreUsuario = async () => {
    SetUsername(await AsyncStorage.getItem('UserName'))
    SetSexo(await AsyncStorage.getItem('UserSexo'))
  }

  return (

    <SafeAreaView>
    <Image source={require('../../../../Asset/Images/Medico/Cuidarte/bienvenida.png')} style={stylesTutor.backgroundImageSplash} />
    <View style={stylesTutor.containerSplash}>
      {Sexo == 'F' ? <Text style={stylesTutor.Title}> Bienvenida Tutora {UserName} ! </Text> :
        <Text style={stylesTutor.Title}>Bienvenido Tutor {UserName} ! </Text>}
        <Text style={stylesTutor.subTitle}>Gracias por elegirnos nuevamente</Text>
       
    </View>
  </SafeAreaView>

  )
}
const styles = StyleSheet.create({
  button: {
    width: '70%',
    marginTop: '4%',
    alignSelf: 'center',
    backgroundColor: "#fff",
    // sombra
    elevation: 2,
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
  },
  buttonWrapper: {
    marginTop: '4%',
  },
  buttonText:{
    color: '#3FA9F5',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  }
});
export default BienvenidaTutor