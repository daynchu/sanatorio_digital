import * as React from 'react'
import {View,Text,Alert, TouchableOpacity, FlatList, BackHandler, Image, AsyncStorage} from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context';
import Icons from 'react-native-vector-icons/Ionicons'
import { useStateIfMounted } from "use-state-if-mounted";
import { TutorDashboardStyles } from '../../../../Asset/estilos/TutorDashboardStyles'
var moment = require('moment');
import { MenuGeneral } from "../../Global/Menu";


const Pcamino = ({ navigation }) => {
  const [doctorName, SetDoctorName] = useStateIfMounted('')
  const [SSexo, SetSSexo] = useStateIfMounted('')
  const [UserName, SetUsername] = useStateIfMounted('')

  React.useEffect(() => {
    // BackHandler.addEventListener('hardwareBackPress', handleBackButton);
    NombreUsuario()
    getNameDoctor()
  },[])

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      const backAction = () => {
        Alert.alert("Hey", "Estas seguro que quieres salir?", [
          {
            text: "NO",
            onPress: () => null,
            style: "cancel"
          },
          { text: "SI", onPress: () => signOut() }
        ]);
        return true;
      };

      BackHandler.addEventListener(
        "hardwareBackPress",
        backAction
      );
      return () =>
      BackHandler.removeEventListener('hardwareBackPress',backAction);
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);
  
  
 

  const NombreUsuario = async () => {
    SetUsername(await AsyncStorage.getItem('UserName'))
  }
  const getNameDoctor = async () => {
    const name = await AsyncStorage.getItem('UserName')
    const sexo = await AsyncStorage.getItem('UserSexo')
    let SaludoSexo = ''
    if (sexo === 'M') {
        SaludoSexo = 'Tutor'
    }
    else {
        SaludoSexo = 'Tutora'
    }
    SetDoctorName(name)
    SetSSexo(SaludoSexo)
  }

  const [Notificaciones, SetNotificaciones] = useStateIfMounted({
      listViewData: {
        key: 1,
        titulo: "notificacion de prueba",
        contenido: "contenido de prueba"
      }
  })

  return(
    <SafeAreaView style={TutorDashboardStyles.flex1}>
      <Image source={require('../../../../Asset/Images/Tutor/Dashboards/TrazadoSuccess.png')} style={TutorDashboardStyles.backgroundHeaderImage} />      
      <MenuGeneral cadenaTitulo={'Hola :) '+SSexo+" "+doctorName} navigation={navigation} color="tutor"></MenuGeneral>
      <View>        
        <Text style={[TutorDashboardStyles.ScreenTitleText,{fontWeight:'bold'}]}>Trabajando juntos para una mejor atención.</Text>
      </View>
      <View>        
        <Text style={TutorDashboardStyles.ScreenTitleTextSub}>Gestiona de una manera ágil los pacientes.</Text>
      </View>
      <TouchableOpacity onPress={() => { navigation.navigate('MainTab') }}>
        <View style={TutorDashboardStyles.StyledButton}>
            <View style={TutorDashboardStyles.StyledButtonImage}>
                <Image source={require('../../../../Asset/Images/Tutor/Dashboards/UsersSuccess.png')} style={TutorDashboardStyles.StyledButtonImageCircle}/>
            </View>
            <View style={TutorDashboardStyles.StyledButtonText}>
                <Text style={TutorDashboardStyles.StyledButtonTitle}>Pacientes</Text>
            </View>
        </View>
      </TouchableOpacity>
    </SafeAreaView>
  )
}

export default Pcamino

 
