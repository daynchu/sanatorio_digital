import * as React from 'react'
import {View,Text,ScrollView, TouchableOpacity, FlatList, BackHandler, Image} from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context';
import Icons from 'react-native-vector-icons/Ionicons'
import { useStateIfMounted } from "use-state-if-mounted";
import { TutorDashboardStyles } from '../../../../Asset/estilos/TutorDashboardStyles'
var moment = require('moment');
import { MenuGeneral } from "../../Global/Menu";

//export default function PEspera (){
const PEspera = ({ navigation }) => {
  const [Notificaciones, SetNotificaciones] = useStateIfMounted({
      listViewData: {
        key: 1,
        titulo: "notificacion de prueba",
        contenido: "contenido de prueba"
      }
  })

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
    
function handleBackButtonClick() {
  navigation.goBack();
  return true;
}
    
      BackHandler.addEventListener(
        "hardwareBackPress",
        handleBackButtonClick
    );
    return () => {
        BackHandler.removeEventListener(
            "hardwareBackPress",
            handleBackButtonClick
        );
    };
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, [navigation]);



  const [Cambio, SetCambio] = useStateIfMounted(false)
  return(
    <SafeAreaView style={TutorDashboardStyles.flex1}>
      <Image source={require('../../../../Asset/Images/Tutor/Dashboards/TrazadoSuccess.png')} style={TutorDashboardStyles.backgroundHeaderImage} />       
      <MenuGeneral cadenaTitulo={'Pacientes en Espera'} navigation={navigation} color="tutor"></MenuGeneral>
      <View style={TutorDashboardStyles.BoxNotifs}>
        <View style={TutorDashboardStyles.flexDirectionRow}>
          <Text style={TutorDashboardStyles.HeaderNotifsFont}>Notificaciones</Text>
        </View>
        {/* {Notificaciones de muestra} */}
        <View style={TutorDashboardStyles.flexDirectionRow}>            
          <View style={TutorDashboardStyles.TitleNotifWidth}>
            <Text style={TutorDashboardStyles.TitleNotifText}>Notificación de ejemplo</Text>
          </View>
          <View style={TutorDashboardStyles.ContentNotifWidth}>
            <Text style={TutorDashboardStyles.ContentNotifText}>Contenido de notificación de ejemplo</Text>
          </View>
          <View style={TutorDashboardStyles.ContentIconWidth}>
            <TouchableOpacity>
              <Icons name='ios-close' size={50} color='black' onPress={() => { }} />
            </TouchableOpacity>
          </View>
        </View>
          {/* {Notificaciones de muestra} */}
          {
            Notificaciones !== [] ? <FlatList
              data={Notificaciones}
              keyExtractor={(item) => item.key}
              onRefresh={() => this.onRefresh()}
              refreshing={Cambio}
              renderItem={(item) => {
                {console.log(item)}
                <View>
                  {/* <Icon style={styles.inline15} name='checkbox-blank-circle' size={25} color={item.item.idEstadoT == 1 ? 'white' : '#0cd1e8'} /> */}
                  <View style={{width:'35%'}}>
                    <Text >{item.item.titulo}</Text>
                  </View>
                  <View style={{width:'65%'}}>
                  <Text >{item.item.contenido}</Text>
                  </View>
                </View>
              }}
            /> : null
          }
        </View>
        {/* {Data de muestra} */}
        <ScrollView>
          <View style={TutorDashboardStyles.boxData}>
            <View style={TutorDashboardStyles.boxDataTitle}>
              <Text style={TutorDashboardStyles.boxDataTitleAlignLeft}>Llegó</Text>
            </View>
            <View style={TutorDashboardStyles.flexDirectionRow}>
              <View style={TutorDashboardStyles.boxDataContentWidth}>
                <Text style={TutorDashboardStyles.bold}>Paciente Mario Gallego</Text>
              </View>
              <View style={TutorDashboardStyles.boxDataContentWidth}>
                <Text style={TutorDashboardStyles.text}>Turno a las 12:00hs</Text>
              </View>
            </View>
            <View style={TutorDashboardStyles.flexDirectionRow}>
              <View style={TutorDashboardStyles.boxDataContentWidth}>
                <Text style={TutorDashboardStyles.text}>Dr. Pablo Sanchez</Text>
              </View>
              <View style={TutorDashboardStyles.boxDataContentWidth}>
                <Text style={TutorDashboardStyles.bold}>Consultorio 1</Text>
              </View>
            </View>
            
          </View>
          <View style={TutorDashboardStyles.boxData}>
            <View style={TutorDashboardStyles.boxDataTitle}>
              <Text style={TutorDashboardStyles.boxDataTitleAlignLeft}>Llegó</Text>
            </View>
            <View style={TutorDashboardStyles.flexDirectionRow}>
              <View style={TutorDashboardStyles.boxDataContentWidth}>
                <Text style={TutorDashboardStyles.bold}>Paciente Mario Gallego</Text>
              </View>
              <View style={TutorDashboardStyles.boxDataContentWidth}>
                <Text style={TutorDashboardStyles.text}>Turno a las 12:00hs</Text>
              </View>
            </View>
            <View style={TutorDashboardStyles.flexDirectionRow}>
              <View style={TutorDashboardStyles.boxDataContentWidth}>
                <Text style={TutorDashboardStyles.text}>Dr. Pablo Sanchez</Text>
              </View>
              <View style={TutorDashboardStyles.boxDataContentWidth}>
                <Text style={TutorDashboardStyles.bold}>Consultorio 1</Text>
              </View>
            </View>
            
          </View>
        </ScrollView>
        {/* {FIN DE Data de muestra} */}
    </SafeAreaView>
  )
}

export default PEspera

 
