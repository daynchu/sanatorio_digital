import * as React from "react";
import { View, Text, Image } from "react-native";
import { useStateIfMounted } from "use-state-if-mounted";
import { SafeAreaView } from "react-native-safe-area-context";
import { StyleServices } from "../../../../Asset/estilos/StylesServices";
import { MenuGeneral } from "../../Global/Menu";
import RNPickerSelect from "react-native-picker-select";

const Tservicio = ({ navigation }) => {
  const [ListadoServicios, SetListadoS] = useStateIfMounted({});
  const [ListadoEstados, SetListadoE] = useStateIfMounted({});

  const CargaListado = async () => {
    try {
      await fetch(
        "https://app.excelenciadigital.ml/SanatorioDigitalApiTest/servicios/por-institucion?idInstitucion=38"
      )
        .then((response) => {
          return response.ok ? response.json() : Promise.reject("algo va mal");
        })
        .then((res) => {
          console.log(res);
        });
    } catch (e) {
      console.log(e);
    }
  };

  React.useEffect(() => {
    CargaListado();
  }, []);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Image
        source={require("../../../../Asset/Images/Tutor/Dashboards/TrazadoSuccess.png")}
        style={StyleServices.backgroundHeaderImage}
      />

      <View style={{ paddingBottom: 15 }}>
        <MenuGeneral
          cadenaTitulo={""}
          navigation={navigation}
          color="tutor"
        ></MenuGeneral>
      </View>
      <View style={{ paddingBottom: 10 }}>
        <Text style={{ textAlign: "center" }}>NOMBRE USUARIOS</Text>
      </View>

      <View style={{ paddingBottom: 10, paddingTop: 10 }}>
        <Text style={{ textAlign: "center" }}>
          Ingrese el servicio a listar
        </Text>
      </View>

      <View
        style={{
          backgroundColor: "white",
          marginHorizontal: 20,
          margin: 10,
          borderRadius: 15,
        }}
      >
        <RNPickerSelect
          onValueChange={(value) => console.log(value)}
          items={[
            { label: "Football", value: "football" },
            { label: "Baseball", value: "baseball" },
            { label: "Hockey", value: "hockey" },
          ]}
        />
      </View>
      {/* <View style={{backgroundColor:'white', marginHorizontal:20,margin:10,borderRadius:15}}> */}
      <RNPickerSelect
        onValueChange={(value) => console.log(value)}
        items={[
          { label: "Football", value: "football", color: "black" },
          { label: "Baseball", value: "baseball" },
          { label: "Hockey", value: "hockey" },
        ]}
      />
      {/* </View> */}
    </SafeAreaView>
  );
};

export default Tservicio;
