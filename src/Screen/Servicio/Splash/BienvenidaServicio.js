import { Text, View, StyleSheet, Image } from "react-native";
import * as React from "react";
import stylesTutor from "../../../../Asset/estilos/stylesTutor";
import { AsyncStorage } from "react-native";
import { useStateIfMounted } from "use-state-if-mounted";
import { SafeAreaView } from "react-native-safe-area-context";

const BienvenidaServicio = ({ navigation }) => {
  const [UserName, SetUsername] = useStateIfMounted("");
  React.useEffect(() => {
    NombreUsuario();
    setTimeout(() => {
      navigation.navigate("InicioServicio");
    }, 3000);
  }, []);

  const NombreUsuario = async () => {
    SetUsername(await AsyncStorage.getItem("UserName"));
    // SetSexo(await AsyncStorage.getItem("UserSexo"));
  };

  return (
    <SafeAreaView>
      <Image
        source={require("../../../../Asset/Images/Medico/Cuidarte/bienvenida.png")}
        style={stylesTutor.backgroundImageSplash}
      />
      <View style={stylesTutor.containerSplash}>
        <Text style={stylesTutor.Title}>
          {" "}
          Bienvenido Servicio {UserName} !{" "}
        </Text>
        <Text style={stylesTutor.subTitle}>
          Gracias por elegirnos nuevamente
        </Text>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  button: {
    width: "70%",
    marginTop: "4%",
    alignSelf: "center",
    backgroundColor: "#fff",
    // sombra
    elevation: 2,
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
  },
  buttonWrapper: {
    marginTop: "4%",
  },
  buttonText: {
    color: "#3FA9F5",
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
  },
});
export default BienvenidaServicio;
