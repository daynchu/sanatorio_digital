import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { useFocusEffect } from "@react-navigation/native";

import { Text } from "react-native";

//Import de Medico
import CuentaCorriente from "../Screen/Medico/CCMedico/CCMedico";
import MedicoCal from "../Screen/Medico/MedicoCalendario/MedicoCalendario";
import VerCuidateMedico from "../Screen/Medico/Splash/Cuidarte";
import MedicoInicio from "../Screen/Medico/MedicoInicio/MedicoInicio";
import CuentaCorrienteDetalle from "../Screen/Medico/CCMedicoDetalle/CCMedicoDetalle";
import CuentaCorrienteObraSocial from "../Screen/Medico/CCMedicoObraSocial/CCMedicoObraSocial";

//Import de Paciente
import verCuidarte from "../Screen/Paciente/Splash/Cuidarte";
import Profesionales from "../Screen/Paciente/SelProfesionales/SelProfesionales";
import Turnos from "../Screen/Paciente/Turnos/Turnos";
import TurnosElegidos from "../Screen/Paciente/TurnosElegido/TurnosElegido";
import PostTurnoElegido from "../Screen/Paciente/PostConfirmTurno/PostConfirmTurno";
import HTurnos from "../Screen/Paciente/HistorialTurnos/HTurnos";
import DashPaciente from "../Screen/Paciente/DashPaciente/DashPaciente";
import Geolocalizacion from "../Screen/Paciente/Geolocalizacion/EnCamino";
import GeolocalizacionNew from "../Screen/Paciente/Geolocalizacion/EnCaminoNew";

//Import de Tutor
import BienvenidoTutor from "../Screen/Tutor/Splash/BienvenidaTutor";
import InicioTutor from "../Screen/Tutor/InicioTutor/InicioTutor";

import Land from "../Screen/SinLog/Landing/Landing";
import Login from "../Screen/SinLog/Login/Login";
import Registro from "../Screen/SinLog/Register/registro";
import RegistroG from "../Screen/SinLog/Register/registroGoogle";
import verPrivacidad from "../Screen/SinLog/Privacidad/Privacidad";
import postgoogle from "../Screen/SinLog/Register/postgoogle";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import PAtendido from "../Screen/Tutor/PacAtendidos/PAtendidos";
import Pcamino from "../Screen/Tutor/PacEnCamino/Pcamino";
import PEspera from "../Screen/Tutor/PacEnEspera/PEspera";
import IconBadge from "react-native-icon-badge";

import BienvenidaServicio from "../Screen/Servicio/Splash/BienvenidaServicio";
import InicioServicio from "../Screen/Servicio/InicioServicio/InicioServicio";
import Tservicio from "../Screen/Servicio/TurnoServicio/Tservicio";

const PacienteStack = createStackNavigator();
const Stack = createStackNavigator();

const Tab = createBottomTabNavigator();

export const MainTab = ({ navigation, route }) => {
  async function estaEnStack() {
    var a = await AsyncStorage.getItem("enStack");
    return a;
  }

  async function traerDatos() {
    try {
      fetch(
        "https://app.excelenciadigital.ml/SanatorioDigitalApiTest/estadosUsuarios/pacientes-por-tutor?idInstitucion=38&uidTutor=811ed99f-f757-4de2-ab37-7d2f007d849b&fechaDesde=2020-09-30 10:00:00.000&fechaHasta=2020-10-02 19:20:00.000&tipo=2"
      )
        .then((response) => {
          return response.ok ? response.json() : Promise.reject("algo va mal");
        })
        .then((res) => {
          console.log(res);
        });
    } catch (e) {
      console.log(e);
    }
  }

  useFocusEffect(
    React.useCallback(() => {
      var numero = 0;
      var intervalo = setInterval(() => {
        traerDatos();
        numero += 1;
        console.log(numero);
      }, 2 * 60 * 1000);
      return () => clearInterval(intervalo);
    }, [navigation])
  );

  return (
    <Tab.Navigator
      initialRouteName="Encamino"
      backBehavior="none"
      tabBarOptions={{
        activeTintColor: "#e91e63",
      }}
    >
      <Tab.Screen
        name="Encamino"
        component={Pcamino}
        options={{
          tabBarLabel: ({ color, size }) => (
            <Text style={{ color: color, fontSize: 15 }}>En camino</Text>
          ),
          tabBarIcon: ({ color, size }) => (
            <IconBadge
              MainElement={
                <MaterialCommunityIcons
                  name="road"
                  size={size}
                  color={color}
                  style={{ width: 40, marginRight: 5 }}
                />
              }
              BadgeElement={<Text style={{ color: "white" }}>4</Text>}
            />
          ),
        }}
      />
      <Tab.Screen
        name="En Espera"
        component={PEspera}
        options={{
          tabBarLabel: ({ color, size }) => (
            <Text style={{ color: color, fontSize: 15 }}>En Espera</Text>
          ),
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="clock" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Atendido"
        component={PAtendido}
        options={{
          tabBarLabel: ({ color, size }) => (
            <Text style={{ color: color, fontSize: 15 }}>Atendido</Text>
          ),
          tabBarIcon: ({ color, size }) => (
            <IconBadge
              MainElement={
                <MaterialCommunityIcons
                  name="check"
                  size={size}
                  color={color}
                  style={{ width: 40, marginRight: 5 }}
                />
              }
              BadgeElement={<Text style={{ color: "white" }}>4</Text>}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export const SinLogin = ({ navigation }) => {
  return (
    <Stack.Navigator
      initialRouteName="Landing"
      screenOptions={{ headerShown: false }}
    >
      <Stack.Screen name="Landing" component={Land} />
      <Stack.Screen name="Registro" component={Registro} />
      <Stack.Screen name="Privacidad" component={verPrivacidad} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="RegistroGo" component={RegistroG} />
      <Stack.Screen name="postgoogle" component={postgoogle} />
    </Stack.Navigator>
  );
};

export const PacienteStackScreen = ({ navigation }) => (
  <PacienteStack.Navigator
    initialRouteName="Cuidarte"
    screenOptions={{ headerShown: false }}
  >
    <PacienteStack.Screen name="Cuidarte" component={verCuidarte} />
    <PacienteStack.Screen name="InicioPaciente" component={DashPaciente} />
    <PacienteStack.Screen name="SelProfesionales" component={Profesionales} />
    <PacienteStack.Screen name="Turnos" component={Turnos} />
    <PacienteStack.Screen name="TurnosElegidos" component={TurnosElegidos} />
    <PacienteStack.Screen name="EnCamino" component={Geolocalizacion} />
    <PacienteStack.Screen
      name="PostTurnoElegido"
      component={PostTurnoElegido}
    />
    <PacienteStack.Screen name="HistorialTurnos" component={HTurnos} />
    <PacienteStack.Screen name="EnCaminoNew" component={GeolocalizacionNew} />
  </PacienteStack.Navigator>
);

export const MedicoStackScreen = ({ navigation }) => (
  <Stack.Navigator
    initialRouteName="CuidarteMedico"
    screenOptions={{ headerShown: false }}
  >
    <Stack.Screen name="CuidarteMedico" component={VerCuidateMedico} />
    <Stack.Screen name="MedicoInicio" component={MedicoInicio} />
    <Stack.Screen name="MedicoCalendario" component={MedicoCal} />
    <Stack.Screen name="CCMedico" component={CuentaCorriente} />
    <Stack.Screen
      name="CuentaCorrienteDetalle"
      component={CuentaCorrienteDetalle}
    />
    <Stack.Screen
      name="CuentaCorrienteObraSocial"
      component={CuentaCorrienteObraSocial}
    />
    <Stack.Screen name="InicioPaciente" component={DashPaciente} />
    <Stack.Screen name="SelProfesionales" component={Profesionales} />
    <Stack.Screen name="HistorialTurnos" component={HTurnos} />
    <Stack.Screen name="Turnos" component={Turnos} />
    <Stack.Screen name="TurnosElegidos" component={TurnosElegidos} />
    <Stack.Screen name="EnCamino" component={Geolocalizacion} />
    <Stack.Screen name="PostTurnoElegido" component={PostTurnoElegido} />
  </Stack.Navigator>
);

export const TutorStackScreen = ({ navigation }) => (
  <Stack.Navigator
    initialRouteName="BienvenidaTutor"
    screenOptions={{ headerShown: false, headerTitle: "Titulo de ejemplo" }}
  >
    <Stack.Screen name="BienvenidaTutor" component={BienvenidoTutor} />
    <Stack.Screen name="InicioTutor" component={InicioTutor} />
  </Stack.Navigator>
);

export const ServicioStackScreen = ({ navigation }) => (
  <Stack.Navigator
    initialRouteName="BienvenidaServicio"
    screenOptions={{ headerShown: false, headerTitle: "Titulo de ejemplo" }}
  >
    <Stack.Screen name="BienvenidaServicio" component={BienvenidaServicio} />
    <Stack.Screen name="InicioServicio" component={InicioServicio} />
    <Stack.Screen name="Tservicio" component={Tservicio} />
  </Stack.Navigator>
);

// esto sirve para poder ocultar la barra se manda cual era la que se oculta
// function getTabBarVisible(route) {
//   const routeName = route.state
//     ?  route.state.routes[route.state.index].name
//     : route.params?.screen || 'Home';

//   if (routeName === 'Details') {
//     return false;
//   }
//   return true;
// }
